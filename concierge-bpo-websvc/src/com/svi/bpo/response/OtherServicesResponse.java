package com.svi.bpo.response;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;

public class OtherServicesResponse {
	
	@Expose
	private boolean successful;	
	@Expose
	private String operation;	
	@Expose
	private int errorCode;
	@Expose
	private String errorMessage;
	
	
	public OtherServicesResponse(){
		
	}
	
	public OtherServicesResponse (boolean successful, String operation, int errorCode,  String errorMessage){
			this.successful = successful;
			this.operation = operation;
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
	}
	public OtherServicesResponse (boolean successful, String operation){
		this.successful = successful;
		this.operation = operation;		
	}
	
	
	public boolean isSuccessful() {
		return successful;
	}
	public String getOperation() {
		return operation;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@Override
	public String toString() {
		Gson gson = new GsonBuilder()
				.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
				}.getType(), new AtomicStringTypeAdapter())
				.registerTypeAdapter(
						new TypeToken<AtomicReference<BigDecimal>>() {
						}.getType(), new AtomicBigDecimalTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicLong>() {
				}.getType(), new AtomicLongTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicInteger>() {
				}.getType(), new AtomicIntegerTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicDouble>() {
				}.getType(), new AtomicDoubleTypeAdapter())
				.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}
	


}
