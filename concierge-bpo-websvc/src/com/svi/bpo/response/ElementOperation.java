package com.svi.bpo.response;

public enum ElementOperation {
	CREATE_ELEM("CREATE_ELEMENT"),
	VIEW_ELEM("VIEW_ELEMENT"),
	GET_ELEM("GET_ELEMENT"),
	CHANGE_PRIORITY("CHANGE_PRIORITY"),
	CHANGE_QUEUE_INDEX("CHANGE_QUEUE_INDEX"),
	TRANSFER_ELEMENT("TRANSFER_ELEMENT"),
	UPDATE_ELEM("UPDATE_ELEMENT"),
	DELETE_ELEM("DELETE_ELEMENT"),
	CORRECT_ENTRY("CORRECT_ENTRY");
	
	
	private String val;
	
	ElementOperation(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
}
