package com.svi.bpo.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.Node;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;
import com.svi.object.ServiceObj;

public class NodeOperationResponse {
	@Expose
	private boolean successful;
	@Expose
	private NodeOperation operation;
	@Expose
	private String nodeId;
	@Expose
	private String params;
	@Expose
	private List<Element> elements;
	@Expose
	private List<Node> nodes;
	@Expose
	private List<WorkerQueueKV> workers;
	@Expose
	private int errorCode;

	public static class WorkerQueueKV {
		@Expose
		private String workerId;
		@Expose
		private Integer[] queueIndex;

		public WorkerQueueKV(String workerId, Integer[] integers) {
			this.workerId = workerId;
			this.queueIndex = integers;
		}

		public String getWorkerId() {
			return workerId;
		}

		public void setWorkerId(String workerId) {
			this.workerId = workerId;
		}

		public Integer[] getQueueIndex() {
			return queueIndex;
		}

		public void setQueueIndex(Integer[] queueIndex) {
			this.queueIndex = queueIndex;
		}

	}

	public NodeOperationResponse(){
	}
	public NodeOperationResponse(boolean successful, NodeOperation operation) {
		this.successful = successful;
		this.operation = operation;
	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.errorCode = errorCode;
	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			String nodeId) {
		this.successful = successful;
		this.operation = operation;
		this.nodeId = nodeId;
	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			String nodeId, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.nodeId = nodeId;
		this.errorCode = errorCode;
	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			String nodeId, List<WorkerQueueKV> workers, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.nodeId = nodeId;
		this.errorCode = errorCode;
		this.workers = workers;

	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			String nodeId, String params) {
		this.successful = successful;
		this.operation = operation;
		this.nodeId = nodeId;
		this.params = params;
	}

	public NodeOperationResponse(boolean successful, NodeOperation operation,
			String nodeId, String params, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.nodeId = nodeId;
		this.params = params;
		this.errorCode = errorCode;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public NodeOperation getOperation() {
		return operation;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getParams() {
		return params;
	}

	public int getErrorCode() {
		return errorCode;
	}

	@SuppressWarnings("serial")
	@Override
	public String toString() {
		// StringBuilder builder = new StringBuilder();
		// builder.append("{successful:\"");
		// builder.append(successful);
		// builder.append("\", ");
		// if (operation != null) {
		// builder.append("operation:\"");
		// builder.append(operation);
		// builder.append("\", ");
		// }
		// if (nodeId != null) {
		// builder.append("nodeId:\"");
		// builder.append(nodeId);
		// builder.append("\", ");
		// }
		// if (params != null) {
		// builder.append("params:\"");
		// builder.append(params);
		// builder.append("\", ");
		// }
		// if (elements != null) {
		// builder.append("elements:\"");
		// builder.append(elements);
		// builder.append("\", ");
		// }
		// if (nodes != null) {
		// builder.append("nodes:\"");
		// builder.append(nodes);
		// builder.append("\", ");
		// }
		// if(errorCode != 0){
		// builder.append("errorCode:\"");
		// builder.append(errorCode);
		// builder.append("\"");
		// }
		// builder.append("}");
		// return builder.toString();
/*		Gson gson = new GsonBuilder()
				.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
				}.getType(), new AtomicStringTypeAdapter())
				.registerTypeAdapter(
						new TypeToken<AtomicReference<BigDecimal>>() {
						}.getType(), new AtomicBigDecimalTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicLong>() {
				}.getType(), new AtomicLongTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicInteger>() {
				}.getType(), new AtomicIntegerTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicDouble>() {
				}.getType(), new AtomicDoubleTypeAdapter())
				.registerTypeAdapter(new TypeToken<List<ServiceObj>>() {
				}.getType(), new AtomicDoubleTypeAdapter())
				.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
				.create();
		return gson.toJson(this);*/
		ObjectMapper oMapper = new ObjectMapper();
		String jsonStr = "";
		try {
			jsonStr = oMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return jsonStr;
		
	}

	public List<Element> getElements() {
		return elements;
	}

	public void setElements(List<Element> elements) {
		this.elements = elements;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public void setWorkers(List<WorkerQueueKV> workers) {
		this.workers = workers;
	}
}
