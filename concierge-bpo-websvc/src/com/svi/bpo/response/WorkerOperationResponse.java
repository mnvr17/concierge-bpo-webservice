package com.svi.bpo.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.Worker;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;

public class WorkerOperationResponse {
	@Expose
	private boolean successful;
	@Expose
	private WorkerOperation operation;
	@Expose
	private String workerId;
	@Expose
	private String nodeId;
	@Expose
	private String nextNodeId;
	@Expose
	private String elementId;
	@Expose
	private Element element;
	@Expose
	private int errorCode;
	@Expose
	private List<Worker> workers;
	@Expose
	private List<Element> elements;

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String nodeId,
			Element element) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;
		this.nodeId = nodeId;
		this.element = element;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String nodeId,
			String elementId) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;
		this.nodeId = nodeId;
		this.elementId = elementId;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String nodeId,
			String elementId, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;
		this.nodeId = nodeId;
		this.elementId = elementId;
		this.errorCode = errorCode;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String nodeId,
			String nextNodeId, String elementId) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;
		this.nodeId = nodeId;
		this.nextNodeId = nextNodeId;
		this.elementId = elementId;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String nodeId,
			String nextNodeId, String elementId, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;
		this.nodeId = nodeId;
		this.nextNodeId = nextNodeId;
		this.elementId = elementId;
		this.errorCode = errorCode;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, String workerId, String elementId,
			int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.workerId = workerId;

		this.elementId = elementId;
		this.errorCode = errorCode;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, List<Element> elements) {
		this.successful = successful;
		this.operation = operation;
		this.elements = elements;
	}

	public WorkerOperationResponse(boolean successful,
			WorkerOperation operation, List<Element> elements, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.elements = elements;
		this.errorCode = errorCode;
	}

	public boolean isSuccessful() {
		return this.successful;
	}

	public WorkerOperation getOperation() {
		return this.operation;
	}

	public String getWorkerId() {
		return this.workerId;
	}

	public String getElementId() {
		return this.elementId;
	}

	public String getNodeId() {
		return this.nodeId;
	}

	public String getNextNodeId() {
		return this.nextNodeId;
	}

	public int getErrorCode() {
		return this.errorCode;
	}

	public List<Element> getElements() {
		return this.elements;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder()
				.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
				}.getType(), new AtomicStringTypeAdapter())
				.registerTypeAdapter(
						new TypeToken<AtomicReference<BigDecimal>>() {
						}.getType(), new AtomicBigDecimalTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicLong>() {
				}.getType(), new AtomicLongTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicInteger>() {
				}.getType(), new AtomicIntegerTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicDouble>() {
				}.getType(), new AtomicDoubleTypeAdapter())
				.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}
}
