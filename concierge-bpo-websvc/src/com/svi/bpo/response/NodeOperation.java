package com.svi.bpo.response;

public enum NodeOperation {
	CREATE("CREATE"),
	DELETE("DELETE"),
	DELETE_ELEMS("DELETE_ELEMS"),
	VIEW_NODE("VIEW_NODE"),
	VIEW_ELEM("VIEW_ELEM"),
	VIEW_WAIT_ELEM("VIEW_WAIT_ELEM"),
	VIEW_PROC_ELEM("VIEW_PROC_ELEM"),
	UPDATE_NODE("UPDATE_NODE"),
	INSERT_ELEM("INSERT_ELEM"),
	ASSIGN_WORKER("ASSIGN_WORKER"),
	UNASSIGN_WORKER("UNASSIGN_WORKER"),
	SHIFT_WORKER("SHIFT_WORKER"),
	GET_WORKERS("GET_WORKERS"),
	;
	
	private String val;
	
	NodeOperation(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
}
