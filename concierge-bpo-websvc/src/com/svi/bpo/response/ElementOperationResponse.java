package com.svi.bpo.response;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.svi.bpo.core.Element;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;

public class ElementOperationResponse {
	@Expose
	private boolean successful;
	@Expose
	private ElementOperation operation;
	@Expose
	private String params;
	@Expose
	private Element element;
	@Expose
	private int errorCode;

	public ElementOperationResponse(boolean successful,
			ElementOperation operation) {
		this.successful = successful;
		this.operation = operation;
	}

	public ElementOperationResponse(boolean successful,
			ElementOperation operation, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.errorCode = errorCode;
	}

	public ElementOperationResponse(boolean successful,
			ElementOperation operation, String params) {
		this.successful = successful;
		this.operation = operation;
		this.params = params;
	}

	public ElementOperationResponse(boolean successful,
			ElementOperation operation, String params, int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.params = params;
		this.errorCode = errorCode;
	}

	public ElementOperationResponse(boolean successful,
			ElementOperation operation, String params, Element element) {
		this.successful = successful;
		this.operation = operation;
		this.params = params;
		this.element = element;
	}

	public ElementOperationResponse(boolean successful,
			ElementOperation operation, String params, Element element,
			int errorCode) {
		this.successful = successful;
		this.operation = operation;
		this.params = params;
		this.element = element;
		this.errorCode = errorCode;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public ElementOperation getOperation() {
		return operation;
	}

	public String getParams() {
		return params;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	@SuppressWarnings("serial")
	@Override
	public String toString() {
		Gson gson = new GsonBuilder()
				.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
				}.getType(), new AtomicStringTypeAdapter())
				.registerTypeAdapter(
						new TypeToken<AtomicReference<BigDecimal>>() {
						}.getType(), new AtomicBigDecimalTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicLong>() {
				}.getType(), new AtomicLongTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicInteger>() {
				}.getType(), new AtomicIntegerTypeAdapter())
				.registerTypeAdapter(new TypeToken<AtomicDouble>() {
				}.getType(), new AtomicDoubleTypeAdapter())
				.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
				.create();
		return gson.toJson(this);
	}
}
