package com.svi.bpo.response;

public enum WorkerOperation {
	GET("GET"),
	GETRET("GET RETURNED"),
	RETURN("RETURN"),
	RETDROP("RETURN AND DROP"),
	COMPLETE("COMPLETE"),
	END("COMPLETE TO END"),
	VIEW_WORKLOAD("VIEW_WORKLOAD");
	
	private String val;
	
	WorkerOperation(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
}
