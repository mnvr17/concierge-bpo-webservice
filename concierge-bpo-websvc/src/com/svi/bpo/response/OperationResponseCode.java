package com.svi.bpo.response;

public enum OperationResponseCode {
	INVALID_PARAMS(460),
	NODE_DOES_NOT_EXIST(461),
	NEXT_NODE_DOES_NOT_EXIST(462),
	ELEMENT_DOES_NOT_EXIST(463),
	WORKER_DOES_NOT_EXIST(464),
	WORKER_NOT_ASSIGNED_TO_NODE(465),
	RESPONSE_OBJ_NULL(466),
	NODE_ALREADY_EXISTS(467),
	WORKER_ALREADY_EXISTS(468),
	ELEMENT_ALREADY_EXISTS(469),
	OPERATION_FAILED(470),
	NODE_IS_CLOSED(471),
	FILE_NOT_FOUND(472);
	
	private int code;
	
	OperationResponseCode(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
}
