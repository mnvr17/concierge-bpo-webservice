package com.svi.bpo.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;


public class ExportFileResponse {
	
	@Expose	private boolean successful;	
	@Expose	private String params;
	@Expose	private int errorCode;	
	
	public ExportFileResponse(boolean successful,String params,int errorCode ){
		this.successful = successful;
		this.params = params;
		this.errorCode = errorCode;		
	}
	
	public ExportFileResponse(boolean successful,String params){
		this.successful = successful;
		this.params = params;	
	}
	
	
	public boolean isSuccessful() {
		return successful;
	}

	public String getParams() {
		return params;
	}

	public int getErrorCode() {
		return errorCode;
	}
	
	@Override
	public String toString() {		
		Gson gson = new GsonBuilder()		
		.setPrettyPrinting()
		.create();
		return gson.toJson(this);
	}

}
