package com.svi.bpo.baggagetag;

import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.svi.bpo.core.ProductionOutputUnit;

public class BaggageTagBuffer {

	// --------------------------------- VARIABLES
	// ------------------------------------//
	private static ConcurrentLinkedQueue<BaggageTag> baggageTagBuffer = new ConcurrentLinkedQueue<BaggageTag>();
	private static int bufferLimit;
	private static long interval;
	private static String[] prodOutputUnitKeys;
	private static String[] extraDetailsKeys;
	private static Thread bufferThread = null;
	private static boolean threadIsRunning = true;
	private static Logger logger = null;
	private static final String DELIMITER = ",";

	// --------------------------------- INNER CLASS
	// ------------------------------------//

	static class BufferThread implements Runnable {

		@Override
		public void run() {

			while (threadIsRunning) {
				try {
					System.out.println("BPO Baggagetags Writer starting");
					final int baggageSize = baggageTagBuffer.size();
					System.out.println("BPO Baggagetags Writer found " + baggageSize + " baggage tags");
					for (int i = 0; i < baggageSize; i++) {
						writeBaggageTagsToFile();
					}
					System.out.println("BPO Baggagetags Writer hibernating for " + interval + "ms");
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					System.out.println("Waking up");
//					Thread.currentThread().interrupt();
				}
				if (!threadIsRunning) {
					return;
				}
			}
//			System.out.println("Stopping thread...");
		}
	}

	// --------------------------------- OTHER METHODS
	// ------------------------------------//
	/**
	 * Add a baggage tag in the baggage tag buffer.
	 * <p>
	 * 
	 * @param baggagetag
	 *            The new baggage tag to be added.
	 */
	public static void enqueueBaggageTag(BaggageTag baggageTag) {
		// adding new baggage tag
		baggageTagBuffer.offer(baggageTag);

		// checks if the baggage tag buffer reached the buffer limit
		// if YES then then wake the Thread
		if (baggageTagBuffer.size() >= bufferLimit) {
			// waking the Thread
//			System.out.println("Reached buffer limit, time to wake up thread");
			bufferThread.interrupt();
		}

	}

	private static void writeBaggageTagsToFile() {
		BaggageTag baggageTag = baggageTagBuffer.poll();
		if (baggageTag != null) {
			StringBuilder baggageDetails = new StringBuilder();
			String elemId = baggageTag.getElementId();
			String elemName = baggageTag.getElementName();
			String nodeId = baggageTag.getNodeId();
			String cluster = baggageTag.getCluster();
			String workerId = baggageTag.getWorkerId();
			String priority = baggageTag.getPriority();
			String elemType = baggageTag.getElemType();
			String status = baggageTag.getElemStatus();
			long startWaitTime = baggageTag.getStartWaitTime();
			long startProcTime = baggageTag.getStartProcTime();
			long endProcTime = baggageTag.getEndProcTime();
			long waitDuration = baggageTag.getWaitingDuration();
			long procDuration = baggageTag.getProcessingDuration();
			
			baggageDetails.append(Strings.isNullOrEmpty(elemId) ? ""
					: StringEscapeUtils.escapeCsv(elemId));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(elemName) ? ""
					: StringEscapeUtils.escapeCsv(elemName));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(nodeId) ? ""
					: StringEscapeUtils.escapeCsv(nodeId));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(cluster) ? ""
					: StringEscapeUtils.escapeCsv(cluster));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(workerId) ? ""
					: StringEscapeUtils.escapeCsv(workerId));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(priority) ? ""
					: StringEscapeUtils.escapeCsv(priority));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(elemType) ? ""
					: StringEscapeUtils.escapeCsv(elemType));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(Strings.isNullOrEmpty(status) ? ""
					: StringEscapeUtils.escapeCsv(status));
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(startWaitTime);
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(startProcTime);
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(endProcTime);
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(waitDuration);
			baggageDetails.append(DELIMITER);
			
			baggageDetails.append(procDuration);
			baggageDetails.append(DELIMITER);

			Map<String, ProductionOutputUnit> productionOutputUnits = baggageTag.getProductionOutputUnits();

			for (String key : prodOutputUnitKeys) {
				ProductionOutputUnit prodOutputUnit = productionOutputUnits.get(key);
				if (prodOutputUnit != null) {
					baggageDetails.append(prodOutputUnit.getMeasurementUnit());
					baggageDetails.append(DELIMITER);
					baggageDetails.append(prodOutputUnit.getCost());
					baggageDetails.append(DELIMITER);
					baggageDetails.append(prodOutputUnit.getOutputCount());
					baggageDetails.append(DELIMITER);
					baggageDetails.append(prodOutputUnit.getErrorCount());
					baggageDetails.append(DELIMITER);
				} else {
					baggageDetails.append(",,,,");
				}
			}

			// getting the details from the extra details
			// Map
			Map<String, String> extraDetails = baggageTag.getExtraDetails();
			for (int j = 0; j < extraDetailsKeys.length; j++) {
				if(j > 0){
					baggageDetails.append(DELIMITER);
				}
				
				String extra = extraDetails.get(extraDetailsKeys[j]);
				if (extra != null) {
					baggageDetails.append(StringEscapeUtils.escapeCsv(extra));
				}
			}
			logger.info(baggageDetails.toString());
		}
	}
	
	public static int getBaggageTagCount(){
		return baggageTagBuffer.size();
	}

	public static void startThread() {
		bufferLimit = Integer.parseInt(BaggageTagEnum.BAGGAGE_TAG_SCHED_DUR_WRITE_BUFFER_LIMIT.getVal());
		interval = 1L * (Integer.parseInt(BaggageTagEnum.BAGGAGE_TAG_SCHED_DUR_WRITE_SLEEP_SEC.getVal()) * 1000);
		prodOutputUnitKeys = BaggageTagEnum.PROD_OUTPUT_UNIT_KEYS.getVal().split("\\|");
		extraDetailsKeys = BaggageTagEnum.EXTRA_DETAIL_KEYS.getVal().split("\\|");
		logger = LoggerFactory.getLogger("baggagetag");
		// starting the thread
		bufferThread = new Thread(new BufferThread());
		bufferThread.start();
	}

	/**
	 * Terminates the running thread that writes the baggage tags in a file.
	 */
	public static void stopThread() {
		if (bufferThread != null && bufferThread.isAlive()) {
			threadIsRunning = false;
			bufferThread.interrupt();
		}
		
//		if(!baggageTagBuffer.isEmpty()){
//			System.out.println(baggageTagBuffer.size()+" baggagetags were left. Writing baggagetags before exiting...");
//		}
		
		while(!baggageTagBuffer.isEmpty()){
			writeBaggageTagsToFile();
		}
		
	}
}
