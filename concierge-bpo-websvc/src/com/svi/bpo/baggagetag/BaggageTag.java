package com.svi.bpo.baggagetag;

import java.util.HashMap;
import java.util.Map;

import com.svi.bpo.core.ProductionOutputUnit;

public class BaggageTag {

// --------------------------------- VARIABLES ------------------------------------//
	private String elementId = new String();
	private String elementName = new String();
	private String nodeId = new String();	
	private String cluster = new String(); 
	private String workerId = new String();
	private String priority;
	private String elemType = new String(); 
	private String elemStatus  = new String();
	private long startWaitTime;
	private long startProcTime;
	private long endProcTime;
	private long waitingDuration;
	private long processingDuration;
	private Map<String, ProductionOutputUnit> productionOutputUnits  = new HashMap<String, ProductionOutputUnit>();;
	private Map<String,String> extraDetails = new HashMap<String,String>();
	
// --------------------------------- SETTERS AND GETTERS ------------------------------------//
	/**
	 * Returns the ID of the element.
	 * <p>
	 * @return The element ID as a String.
	 */
	public String getElementId() {
		return elementId;
	}
	
	/**
	 * Sets the ID of the element.
	 * <p>
	 * @param elementId  The element ID as a String.
	 */
	public void setElementId(String elementId) {
		this.elementId = elementId;
	}
	
	/**
	 * Returns the name of the element.
	 * <p>
	 * @return String elementName
	 */
	public String getElementName() {
		return elementName;
	}
	
	/**
	 * Sets the name of the element.
	 * <p>
	 *  @param elementName
	 */
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	
	/**
	 * Returns the ID of the node.
	 * <p>
	 * @return String nodeId
	 */
	public String getNodeId() {
		return nodeId;
	}
	
	/**
	 * Sets the ID of the node.
	 * <p>
	 * @param String nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	/**
	 * Returns the cluster of the node.
	 * <p>
	 * @return String cluster
	 */
	public String getCluster() {
		return cluster;
	}
	
	/**
	 * Sets the cluster of the node.
	 * <p>
	 *  @param cluster
	 */
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	
	/**
	 * Returns the ID of the worker.
	 * <p>
	 * @return String workerId
	 */
	public String getWorkerId() {
		return workerId;
	}
	
	/**
	 * Sets the ID of the worker.
	 * <p>
	 * @param String workerId
	 */
	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}
	
	/**
	 * Returns the level of priority of the element ranging from 0 to 9 (9 as the highest).
	 * <p>
	 * @return int priority
	 */
	public String getPriority() {
		return priority;
	}
	
	/**
	 *Sets the level of priority of the element ranging from 0 to 9 (9 as the highest).
	 * <p>
	 * @param priority
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	/**
	 * Returns the standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * <p>
	 * @return String elemType
	 */
	public String getElemType() {
		return elemType;
	}
	
	/**
	 * Sets the standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * <p>
	 *  @param elemType
	 */
	public void setElemType(String elemType) {
		this.elemType = elemType;
	}
	
	/**
	 * Returns the status of the element: either WAITING, PROCESSING or COMPLETE.
	 * <p>
	 * @return String status
	 */
	public String getElemStatus() {
		return elemStatus;
	}
	
	/**
	 * Sets the status of the element: either WAITING, PROCESSING or COMPLETE.
	 * <p>
	 * @param status
	 */
	public void setElemStatus(String elemStatus) {
		this.elemStatus = elemStatus;
	}
	
	/**
	 * Returns the timestamp when the element was enqueued in the node.
	 * <p>
	 * @return long startWaitTime
	 */
	public long getStartWaitTime() {
		return startWaitTime;
	}
	
	/**
	 * Sets the timestamp when the element was enqueued in the node.
	 * <p>
	 * @param startWaitTime
	 */
	public void setStartWaitTime(long startWaitTime) {
		this.startWaitTime = startWaitTime;
	}
	
	/**
	 * Returns the timestamp when work in the element was started.
	 * <p>
	 * @return long startProcTime
	 */	
	public long getStartProcTime() {
		return startProcTime;
	}
	
	/**
	 *Sets the timestamp when work in the element was started.
	 * <p>
	 * @param startProcTime
	 */	
	public void setStartProcTime(long startProcTime) {
		this.startProcTime = startProcTime;
	}
	
	/**
	 * Returns the timestamp when work in the element was completed
	 * <p>
	 * @return long endProcTime
	 */	
	public long getEndProcTime() {
		return endProcTime;
	}
	
	/**
	 * Sets the timestamp when work in the element was completed
	 * <p>
	 * @param endProcTime
	 */
	public void setEndProcTime(long endProcTime) {
		this.endProcTime = endProcTime;
	}
	
	/**
	 * Returns the duration of time (milliseconds) the element was waiting/enqueued until being processed.
	 * <p>
	 * @return long waitingDuration
	 */	
	public long getWaitingDuration() {
		return waitingDuration;
	}
	
	/**
	 * Sets the duration of time (milliseconds) the element was waiting/enqueued until being processed.
	 * <p>
	 * @param waitingDuration
	 */	
	public void setWaitingDuration(long waitingDuration) {
		this.waitingDuration = waitingDuration;
	}
	
	/**
	 * Returns the duration of time (milliseconds) the element was in-process.
	 * <p>
	 * @return long processingDuration
	 */	
	public long getProcessingDuration() {
		return processingDuration;
	}
	
	/**
	 * Sets the duration of time (milliseconds) the element was in-process.
	 * <p>
	 * @param processingDuration
	 */	
	public void setProcessingDuration(long processingDuration) {
		this.processingDuration = processingDuration;
	}
	
	/**
	 * Returns the production output and error count mapping for each unit of measurement.
	 * <p>
	 * @return Map productionOutputUnits
	 */	
	public Map<String, ProductionOutputUnit> getProductionOutputUnits() {
		return productionOutputUnits;
	}
	
	/**
	 * Sets the production output and error count mapping for each unit of measurement.
	 * <p>
	 * @param Map productionOutputUnits
	 */	
	public void setProductionOutputUnits(
			Map<String, ProductionOutputUnit> productionOutputUnits) {
		this.productionOutputUnits = productionOutputUnits;
	}
	
	/**
	 * Returns the other details specific to this element.
	 * <p>
	 * @return Map extraDetails
	 */	
	public Map<String, String> getExtraDetails() {
		return extraDetails;
	}
	
	/**
	 * sets the other details specific to this element.
	 * <p>
	 * @param Map extraDetails
	 */	
	public void setExtraDetails(Map<String, String> extraDetails) {
		this.extraDetails = extraDetails;
	}

}
