package com.svi.bpo.baggagetag;

import org.ini4j.Profile.Section;

public enum BaggageTagEnum {
	BAGGAGE_TAG_DIR("BAGGAGE_TAG_DIR"),
	BAGGAGE_TAG_SCHED_DUR_WRITE_SLEEP_SEC("BAGGAGE_TAG_SCHED_DUR_WRITE_SLEEP_SEC"),
	BAGGAGE_TAG_SCHED_DUR_WRITE_BUFFER_LIMIT("BAGGAGE_TAG_SCHED_DUR_WRITE_BUFFER_LIMIT"),
	PROD_OUTPUT_UNIT_KEYS("PROD_OUTPUT_UNIT_KEYS"),
	EXTRA_DETAIL_KEYS("EXTRA_DETAIL_KEYS");
	
	private String val = "";
	
	BaggageTagEnum(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
	
	public static void setContext(Section workers){
		for(BaggageTagEnum each : values()){
			each.setVal(workers.get(each.getVal()));
		}
	}
}
