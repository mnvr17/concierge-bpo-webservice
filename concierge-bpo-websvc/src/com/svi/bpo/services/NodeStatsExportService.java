package com.svi.bpo.services;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("exportNodeStats")
public class NodeStatsExportService {
	
	private NodeStatsService nodeStatsService;

	@GET
	@Path("/yearly/{year}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagYearly(@PathParam("year") String year)
			throws IOException {
		
		nodeStatsService = new NodeStatsService();
		return nodeStatsService.getBaggagetagYearly(year);		
	}

	@GET
	@Path("/monthly/{year}/{month}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagMonthly(@PathParam("year") String year,
			@PathParam("month") String month) throws IOException {
		
		nodeStatsService = new NodeStatsService();
		return nodeStatsService.getBaggagetagMonthly(year, month);		
	}

	@GET
	@Path("/daily/{year}/{month}/{day}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagDaily(@PathParam("year") String year,
			@PathParam("month") String month, @PathParam("day") String day)
			throws IOException {
		
		nodeStatsService = new NodeStatsService();
		return nodeStatsService.getBaggagetagDaily(year, month, day);
	}

	@GET
	@Path("/status/{filesLocation}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkFilePathStatus(
			@PathParam("filesLocation") String filesLocation)
			throws IOException {
		
		nodeStatsService = new NodeStatsService();
		return nodeStatsService.checkFilePathStatus(filesLocation);		
	}

}
