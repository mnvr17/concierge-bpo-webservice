package com.svi.bpo.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.bpo.core.Worker;
import com.svi.bpo.core.WorkerRepository;
import com.svi.bpo.response.OperationResponseCode;
import com.svi.bpo.response.WorkerOperation;
import com.svi.bpo.response.WorkerOperationResponse;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;
import com.svi.bpo.util.ElementComparator;
import com.svi.bpo.util.GeneralUtil;

@Path("workers")
@SuppressWarnings("serial")
public class WorkerService{


	private Gson gson = new GsonBuilder()
			.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
			}.getType(), new AtomicStringTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicReference<BigDecimal>>() {
			}.getType(), new AtomicBigDecimalTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicLong>() {
			}.getType(), new AtomicLongTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicInteger>() {
			}.getType(), new AtomicIntegerTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicDouble>() {
			}.getType(), new AtomicDoubleTypeAdapter())
			.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
			.create();

	// old paths
	
	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement0(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId) {
		
		return getElement(workerId, nodeId);		
	}

	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/get/{queueIndex}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement0(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam("queueIndex") int queueIndex) {
		
		return getElement(workerId, nodeId, queueIndex);		
	}
	
	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/get/{queueIndex}/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement0(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam("queueIndex") int queueIndex,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		
		return getElement(workerId, nodeId, queueIndex, elementId);		
	}

	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/getret/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnedElement0(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		
		return getReturnedElement(workerId, nodeId, elementId);		
	}

	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/return/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response returnElement0(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		
		return returnElement(workerId, nodeId, elementId);		
	}

	@POST
	@Path("/list/{workerId}/nodes/{nodeId}/drop/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response returnElementAndDrop0(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId,
			String json) {
		
		return returnElementAndDrop(workerId, nodeId, elementId, json);		
	}

	@POST
	@Path("/list/{workerId}/nodes/{nodeId}/complete/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response completeElementToNextNode0(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json) {	
		
		JsonObject values = GeneralUtil.parseToJson(json);
		String nextNodeId = values.get(BPOConstants.NEXT_NODE_ID).getAsString().trim();		
		return completeElementToNextNode(workerId, nodeId, elementId, json, nextNodeId);
	}

	@POST
	@Path("/list/{workerId}/nodes/{nodeId}/end/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response completeElementToEnd0(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json) {
		
		return completeElementToEnd(workerId, nodeId, elementId, json);		
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllWorkers0() {
		
		return getAllWorkers();		
	}
	
	@GET
	@Path("/list/{workerId}/workload")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkload0(@PathParam(BPOConstants.WORKER_ID) String workerId){
		
		return getWorkerCurrentWorkloadSorted(workerId,"id");		
	}
	  
	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/workload")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadPerNode0(@PathParam(BPOConstants.WORKER_ID) String workerId, @PathParam(BPOConstants.NODE_ID) String nodeId){
	   
		return getWorkerCurrentWorkloadPerNodeSorted(workerId, nodeId, "id");		 
	}
	  
	@GET
	@Path("/list/{workerId}/workload/bypriority")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadByPriority0(@PathParam(BPOConstants.WORKER_ID) String workerId){
		  
		  return getWorkerCurrentWorkloadSorted(workerId, BPOConstants.PRIORITY);		
	}
	  
	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/workload/bypriority")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadByPriorityPerNode0(@PathParam(BPOConstants.WORKER_ID) String workerId, @PathParam(BPOConstants.NODE_ID) String nodeId){
		  
		 return getWorkerCurrentWorkloadPerNodeSorted(workerId, nodeId, BPOConstants.PRIORITY);
	}
	  
	@GET
	@Path("/list/{workerId}/workload/bystartproc")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadByStartProcTime0(@PathParam(BPOConstants.WORKER_ID) String workerId){
		  
		return getWorkerCurrentWorkloadSorted(workerId, "startproctime");
	}
	  
	@GET
	@Path("/list/{workerId}/nodes/{nodeId}/workload/bystartproc")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadByStartProcTimePerNode0(@PathParam(BPOConstants.WORKER_ID) String workerId, @PathParam(BPOConstants.NODE_ID) String nodeId){
		  
		return getWorkerCurrentWorkloadPerNodeSorted(workerId, nodeId, "startproctime");
	}

	// new paths

	@GET
	@Path("/{workerId}/nodes/{nodeId}/elements")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId) {
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId)) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.getElement(nodeId);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode())
							.entity(new WorkerOperationResponse(false,
									WorkerOperation.GET, workerId, nodeId, "",
									OperationResponseCode.RESPONSE_OBJ_NULL
											.getCode()).toString()).build();
				}
			} else {
				return Response
						.status(OperationResponseCode.NODE_DOES_NOT_EXIST
								.getCode())
						.entity(new WorkerOperationResponse(false,
								WorkerOperation.GET, workerId, nodeId, "",
								OperationResponseCode.NODE_DOES_NOT_EXIST
										.getCode()).toString()).build();
			}
		} else {

			return Response
					.status(OperationResponseCode.WORKER_DOES_NOT_EXIST
							.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.WORKER_DOES_NOT_EXIST
									.getCode()).toString()).build();
		}
	}

	@GET
	@Path("/{workerId}/nodes/{nodeId}/elements/{queueIndex}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam("queueIndex") int queueIndex) {
		
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId) || queueIndex < 0
				|| queueIndex > 99) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.getElement(nodeId,
						queueIndex);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode())
							.entity(new WorkerOperationResponse(false,
									WorkerOperation.GET, workerId, nodeId, "",
									OperationResponseCode.RESPONSE_OBJ_NULL
											.getCode()).toString()).build();
				}
			} else {
				return Response
						.status(OperationResponseCode.NODE_DOES_NOT_EXIST
								.getCode())
						.entity(new WorkerOperationResponse(false,
								WorkerOperation.GET, workerId, nodeId, "",
								OperationResponseCode.NODE_DOES_NOT_EXIST
										.getCode()).toString()).build();
			}
		} else {

			return Response
					.status(OperationResponseCode.WORKER_DOES_NOT_EXIST
							.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.WORKER_DOES_NOT_EXIST
									.getCode()).toString()).build();
		}
	}

	@GET
	@Path("/{workerId}/nodes/{nodeId}/elements/{queueIndex}/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam("queueIndex") int queueIndex,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, elementId) || queueIndex < 0
				|| queueIndex > 99) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.getElement(nodeId,
						queueIndex, elementId);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode())
							.entity(new WorkerOperationResponse(false,
									WorkerOperation.GET, workerId, nodeId, "",
									OperationResponseCode.RESPONSE_OBJ_NULL
											.getCode()).toString()).build();
				}
			} else {
				return Response
						.status(OperationResponseCode.NODE_DOES_NOT_EXIST
								.getCode())
						.entity(new WorkerOperationResponse(false,
								WorkerOperation.GET, workerId, nodeId, "",
								OperationResponseCode.NODE_DOES_NOT_EXIST
										.getCode()).toString()).build();
			}
		} else {

			return Response
					.status(OperationResponseCode.WORKER_DOES_NOT_EXIST
							.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.WORKER_DOES_NOT_EXIST
									.getCode()).toString()).build();
		}
	}
	
	@GET
	@Path("/{workerId}/nodes/{nodeId}/returned-elements/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReturnedElement(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, elementId)) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GETRET, workerId, nodeId, elementId,
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.getReturnedElement(
						nodeId, elementId);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode())
							.entity(new WorkerOperationResponse(false,
									WorkerOperation.GETRET, workerId, nodeId,
									elementId, OperationResponseCode.RESPONSE_OBJ_NULL
											.getCode()).toString()).build();
				}
			} else {
				return Response
						.status(OperationResponseCode.NODE_DOES_NOT_EXIST
								.getCode())
						.entity(new WorkerOperationResponse(false,
								WorkerOperation.GETRET, workerId, nodeId, elementId,
								OperationResponseCode.NODE_DOES_NOT_EXIST
										.getCode()).toString()).build();
			}
		} else {
			return Response
					.status(OperationResponseCode.WORKER_DOES_NOT_EXIST
							.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.GETRET, workerId, nodeId, elementId,
							OperationResponseCode.WORKER_DOES_NOT_EXIST
									.getCode()).toString()).build();
		}
	}

	@POST
	@Path("/{workerId}/nodes/{nodeId}/{elementId}/return")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response returnElement(@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId) {
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, elementId)) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETURN, workerId, nodeId, elementId,
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.returnElement(nodeId,
						elementId);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode())
							.entity(new WorkerOperationResponse(false,
									WorkerOperation.RETURN, workerId, nodeId,
									elementId, OperationResponseCode.RESPONSE_OBJ_NULL
											.getCode()).toString()).build();
				}
			} else {
				return Response
						.status(OperationResponseCode.NODE_DOES_NOT_EXIST
								.getCode())
						.entity(new WorkerOperationResponse(false,
								WorkerOperation.RETURN, workerId, nodeId, elementId,
								OperationResponseCode.NODE_DOES_NOT_EXIST
										.getCode()).toString()).build();
			}
		} else {
			return Response
					.status(OperationResponseCode.WORKER_DOES_NOT_EXIST
							.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETURN, workerId, nodeId, elementId,
							OperationResponseCode.WORKER_DOES_NOT_EXIST
									.getCode()).toString()).build();
		}
	}

	@POST
	@Path("/{workerId}/nodes/{nodeId}/{elementId}/return-and-drop")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response returnElementAndDrop(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId,
			String json) {
		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, elementId, json)) {
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.INVALID_PARAMS.getCode())
							.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.returnElementAndDrop(
						nodeId, elementId, json);
				if (response != null) {
					return GeneralUtil.buildResponse(response);	
				} else {
					return Response.status(
							OperationResponseCode.RESPONSE_OBJ_NULL.getCode())
							.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.RESPONSE_OBJ_NULL.getCode())
							.toString()).build();
				}
			} else {
				return Response.status(
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
						.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
							.toString()).build();
			}
		} else {
			return Response.status(
					OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
							.toString()).build();
		}
	}

	@POST
	@Path("/{workerId}/nodes/{nodeId}/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response completeElementToNextNode(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json,
			@QueryParam(BPOConstants.NEXT_NODE_ID) String nextNodeId) {
		
		JsonObject values = GeneralUtil.parseToJson(json);
		
		if (!values.has(BPOConstants.PROD_OUTPUT_UNITS)) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.COMPLETE, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		}

		/*************************/

		Map<String, ProductionOutputUnit> prods;

		try {
			prods = gson.fromJson(values.get(BPOConstants.PROD_OUTPUT_UNITS),
					new TypeToken<Map<String, ProductionOutputUnit>>() {
					}.getType());
		} catch (JsonParseException | IllegalStateException
				| ClassCastException | NullPointerException e) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.COMPLETE, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		}

		Map<String, ProductionOutputUnit> productionOutputUnits = new HashMap<>();
		if (prods.isEmpty()) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.COMPLETE, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		} else {
			for (Entry<String, ProductionOutputUnit> each : prods.entrySet()) {
				String key = each.getKey();
				ProductionOutputUnit prod = each.getValue();
				if (Strings.isNullOrEmpty(key) || prod == null) {
					WorkerOperationResponse res = new WorkerOperationResponse(
							false, WorkerOperation.COMPLETE, workerId,
							elementId,
							OperationResponseCode.INVALID_PARAMS.getCode());
					return Response
							.status(OperationResponseCode.INVALID_PARAMS
									.getCode()).entity(res.toString()).build();
				}
				productionOutputUnits.put(
						key,
						new ProductionOutputUnit(prod.getOutputCount(), prod
								.getErrorCount()));
			}
		}

		Map<String, String> extraDetails = new HashMap<>();
		if (values.has(BPOConstants.EXTRA_DETAILS)) {
			try {
				Map<String, String> extraDetailsProds = gson.fromJson(
						values.get(BPOConstants.EXTRA_DETAILS),
						new TypeToken<Map<String, String>>() {
						}.getType());
				for (Entry<String, String> each : extraDetailsProds.entrySet()) {
					String key = each.getKey();
					String extra = each.getValue();
					if (Strings.isNullOrEmpty(key) || extra == null) {
						WorkerOperationResponse res = new WorkerOperationResponse(
								false, WorkerOperation.COMPLETE, workerId,
								elementId,
								OperationResponseCode.INVALID_PARAMS.getCode());
						return Response
								.status(OperationResponseCode.INVALID_PARAMS
										.getCode()).entity(res.toString())
								.build();
					}
					extraDetails.put(key, extra);
				}
			} catch (JsonParseException | IllegalStateException
					| ClassCastException | NullPointerException e) {

				WorkerOperationResponse res = new WorkerOperationResponse(
						false, WorkerOperation.COMPLETE, workerId, elementId,
						OperationResponseCode.INVALID_PARAMS.getCode());
				return Response
						.status(OperationResponseCode.INVALID_PARAMS.getCode())
						.entity(res.toString()).build();
			}
		}

		/*************************/

		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, nextNodeId, elementId)) {
			return Response.status(
					OperationResponseCode.INVALID_PARAMS.getCode()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker
						.completeElementToNextNode(nodeId, nextNodeId,
								elementId, productionOutputUnits, extraDetails);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					return Response.status(
							OperationResponseCode.RESPONSE_OBJ_NULL.getCode())
							.entity(new WorkerOperationResponse(false,
							WorkerOperation.COMPLETE, workerId, nodeId, elementId,
							OperationResponseCode.RESPONSE_OBJ_NULL.getCode())
							.toString()).build();
				}
			} else {
				return Response.status(
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
						.entity(new WorkerOperationResponse(false,
							WorkerOperation.COMPLETE, workerId, nodeId, elementId,
							OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
							.toString()).build();
			}
		} else {
			return Response.status(
					OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.COMPLETE, workerId, nodeId, elementId,
							OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
							.toString()).build();
		}
	}
			  
	@GET
	@Path("/{workerId}/workload")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadSorted(@PathParam(BPOConstants.WORKER_ID) String workerId,
			 @QueryParam("sort") String sortedBy){
		  
		 return getWorkerCurrentWorkload(workerId, sortedBy);
	}
	  	  
	@GET
	@Path("/{workerId}/nodes/{nodeId}/workload")
	@Produces({"application/json"})
	public Response getWorkerCurrentWorkloadPerNodeSorted(@PathParam(BPOConstants.WORKER_ID) String workerId, @PathParam(BPOConstants.NODE_ID) String nodeId,
			  @QueryParam("sort") String sortedBy){
		  
		 return getWorkerCurrentWorkloadPerNode(workerId, nodeId, sortedBy);		
	}
	  
	@GET	
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllWorkers() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"successful\": true, \"operation\": \"VIEW_WORKERS\", \"workers\": [");
		boolean first = true;
		for (Worker worker : WorkerRepository.getAllWorkers()) {
			if (!first) {
				sb.append(",");
			} else {
				first = false;
			}
			sb.append("{\"workerId\": \"" + worker.getWorkerId());
			sb.append("\",\"workerName\": \"" + worker.getWorkerName());
			sb.append("\"}");
		}
		sb.append("]}");
		return Response.ok(sb.toString()).build();
	}
	  
	@POST
	@Path("/{workerId}/nodes/{nodeId}/{elementId}/complete-to-end")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response completeElementToEnd(
			@PathParam(BPOConstants.WORKER_ID) String workerId,
			@PathParam(BPOConstants.NODE_ID) String nodeId,
			@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json) {
		
		
		JsonObject values = GeneralUtil.parseToJson(json);
		
		/*************************/
		Map<String, ProductionOutputUnit> prods;

		try {
			prods = gson.fromJson(values.get(BPOConstants.PROD_OUTPUT_UNITS),
					new TypeToken<Map<String, ProductionOutputUnit>>() {
					}.getType());
		} catch (JsonParseException | IllegalStateException
				| ClassCastException | NullPointerException e) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.END, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		}

		Map<String, ProductionOutputUnit> productionOutputUnits = new HashMap<>();
		if (prods.isEmpty()) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.END, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		} else {
			for (Entry<String, ProductionOutputUnit> each : prods.entrySet()) {
				String key = each.getKey();
				ProductionOutputUnit prod = each.getValue();
				if (Strings.isNullOrEmpty(key) || prod == null) {
					WorkerOperationResponse res = new WorkerOperationResponse(
							false, WorkerOperation.END, workerId, elementId,
							OperationResponseCode.INVALID_PARAMS.getCode());
					return Response
							.status(OperationResponseCode.INVALID_PARAMS
									.getCode()).entity(res.toString()).build();
				}
				productionOutputUnits.put(
						key,
						new ProductionOutputUnit(prod.getOutputCount(), prod
								.getErrorCount()));
			}
		}

		Map<String, String> extraDetails = new HashMap<>();
		if (values.has(BPOConstants.EXTRA_DETAILS)) {
			try {
				Map<String, String> extraDetailsProds = gson.fromJson(
						values.get(BPOConstants.EXTRA_DETAILS),
						new TypeToken<Map<String, String>>() {
						}.getType());
				for (Entry<String, String> each : extraDetailsProds.entrySet()) {
					String key = each.getKey();
					String extra = each.getValue();
					if (Strings.isNullOrEmpty(key) || extra == null) {
						WorkerOperationResponse res = new WorkerOperationResponse(
								false, WorkerOperation.END, workerId,
								elementId,
								OperationResponseCode.INVALID_PARAMS.getCode());
						return Response
								.status(OperationResponseCode.INVALID_PARAMS
										.getCode()).entity(res.toString())
								.build();
					}
					extraDetails.put(key, extra);
				}
			} catch (JsonParseException | IllegalStateException
					| ClassCastException | NullPointerException e) {
				WorkerOperationResponse res = new WorkerOperationResponse(
						false, WorkerOperation.END, workerId, elementId,
						OperationResponseCode.INVALID_PARAMS.getCode());
				return Response
						.status(OperationResponseCode.INVALID_PARAMS.getCode())
						.entity(res.toString()).build();
			}
		}

		/*************************/

		if (GeneralUtil.isParamsNullOrEmpty(workerId, nodeId, elementId)) {
			WorkerOperationResponse res = new WorkerOperationResponse(false,
					WorkerOperation.END, workerId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return Response
					.status(OperationResponseCode.INVALID_PARAMS.getCode())
					.entity(res.toString()).build();
		}

		// test if worker exists in the worker repository
		if (WorkerRepository.contains(workerId)) {
			// get the worker from the repository
			Worker worker = WorkerRepository.getWorker(workerId);
			if (NodeRepository.contains(nodeId)) {
				WorkerOperationResponse response = worker.completeElementToEnd(
						nodeId, elementId, productionOutputUnits, extraDetails);
				if (response != null) {
					return GeneralUtil.buildResponse(response);						
				} else {
					WorkerOperationResponse res = new WorkerOperationResponse(
							false, WorkerOperation.END, workerId, elementId,
							OperationResponseCode.RESPONSE_OBJ_NULL.getCode());
					return Response
							.status(OperationResponseCode.RESPONSE_OBJ_NULL
									.getCode()).entity(res.toString()).build();
				}
			} else {
				return Response.status(
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
						.entity(new WorkerOperationResponse(false,
							WorkerOperation.END, workerId, nodeId, elementId,
							OperationResponseCode.NODE_DOES_NOT_EXIST.getCode())
							.toString()).build();
			}
		} else {
			return Response.status(
					OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
					.entity(new WorkerOperationResponse(false,
							WorkerOperation.END, workerId, nodeId, elementId,
							OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode())
							.toString()).build();
		}
	}
  	  
	public Response getWorkerCurrentWorkload(String workerId, String sortedBy){
	    if (Strings.isNullOrEmpty(workerId)){
	      WorkerOperationResponse res = new WorkerOperationResponse(false, 
	        WorkerOperation.VIEW_WORKLOAD, null, 
	        OperationResponseCode.INVALID_PARAMS.getCode());
	      return 
	        Response.status(res.getErrorCode())
	        .entity(res.toString()).build();
	    }
	    if (WorkerRepository.contains(workerId))
	    {
	      Worker worker = WorkerRepository.getWorker(workerId);	    
	      
	      Set<Element> elements = null;
	      if(BPOConstants.PRIORITY.equalsIgnoreCase(sortedBy)){
	    	  elements = new TreeSet<>(ElementComparator.byPriorityThenByStartWaitTime);
	      }else if("startproctime".equalsIgnoreCase(sortedBy)){
	    	  elements = new TreeSet<>(ElementComparator.byStartProcTimeThenByID);
	      }else{
	    	  elements = new TreeSet<>(ElementComparator.byID);
	      }
	      
	      elements.addAll(worker.getWorkload());
	      
	      WorkerOperationResponse res = new WorkerOperationResponse(true, 
	        WorkerOperation.VIEW_WORKLOAD, new ArrayList<>(elements));
	      return 
	        Response.ok()
	        .entity(res.toString()).build();
	    }
	    WorkerOperationResponse res = new WorkerOperationResponse(false, 
	      WorkerOperation.VIEW_WORKLOAD, null, 
	      OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode());
	    return 
	      Response.status(res.getErrorCode())
	      .entity(res.toString()).build();
	}
	  
	public Response getWorkerCurrentWorkloadPerNode(String workerId, String nodeId, String sortedBy){
	    if ((Strings.isNullOrEmpty(workerId)) || 
	      (Strings.isNullOrEmpty(nodeId)))
	    {
	      WorkerOperationResponse res = new WorkerOperationResponse(false, 
	        WorkerOperation.VIEW_WORKLOAD, null, 
	        OperationResponseCode.INVALID_PARAMS.getCode());
	      return 
	        Response.status(res.getErrorCode())
	        .entity(res.toString()).build();
	    }
	    if (!NodeRepository.contains(nodeId))
	    {
	      WorkerOperationResponse res = new WorkerOperationResponse(false, 
	        WorkerOperation.VIEW_WORKLOAD, null, 
	        OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
	      return 
	        Response.status(res.getErrorCode())
	        .entity(res.toString()).build();
	    }
	    if (WorkerRepository.contains(workerId))
	    {
	      Worker worker = WorkerRepository.getWorker(workerId);    
	      
	      Set<Element> elements = null;
	      if(BPOConstants.PRIORITY.equalsIgnoreCase(sortedBy)){
	    	  elements = new TreeSet<>(ElementComparator.byPriorityThenByStartWaitTime);
	      }else if("startproctime".equalsIgnoreCase(sortedBy)){
	    	  elements = new TreeSet<>(ElementComparator.byStartProcTimeThenByID);
	      }else{
	    	  elements = new TreeSet<>(ElementComparator.byID);
	      }
	      
	      for (Element e : worker.getWorkload()) {
	        if (e.getNodeId().equalsIgnoreCase(nodeId)) {
	          elements.add(e);
	        }
	      }
	      WorkerOperationResponse res = new WorkerOperationResponse(true, 
	        WorkerOperation.VIEW_WORKLOAD, new ArrayList<>(elements));
	      return Response.ok().entity(res.toString()).build();
	    }
	    WorkerOperationResponse res = new WorkerOperationResponse(false, 
	      WorkerOperation.VIEW_WORKLOAD, null, 
	      OperationResponseCode.WORKER_DOES_NOT_EXIST.getCode());
	    return 
	      Response.status(res.getErrorCode())
	      .entity(res.toString()).build();
	}
	
	
}
