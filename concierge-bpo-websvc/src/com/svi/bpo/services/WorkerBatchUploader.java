package com.svi.bpo.services;

import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;


@Path("workersupload")
public class WorkerBatchUploader {
	
	private NodeService  nodeService;	
	
	@POST
	@Path("/assign")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response assignWorkerBatchUpload(@FormDataParam("bpo-batch-assign-worker") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-assign-worker") FormDataContentDisposition fileDetail){
		
		nodeService = new NodeService();
		return nodeService.assignWorkerBatchUpload(uploadedInputStream, fileDetail);
	}	
	
	@POST
	@Path("/unassign")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response unassignWorkerBatchUpload(@FormDataParam("bpo-batch-unassign-worker") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-unassign-worker") FormDataContentDisposition fileDetail){	
		
		nodeService = new NodeService();
		return nodeService.unassignWorkerBatchUpload(uploadedInputStream, fileDetail);
	}
	
	

	
	
	

}
