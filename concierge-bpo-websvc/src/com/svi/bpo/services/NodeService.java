package com.svi.bpo.services;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.core.Node;
import com.svi.bpo.response.ElementOperationResponse;
import com.svi.bpo.response.NodeOperation;
import com.svi.bpo.response.NodeOperationResponse;
import com.svi.bpo.response.OperationResponseCode;
import com.svi.bpo.util.ElementUtil;
import com.svi.bpo.util.GeneralUtil;
import com.svi.bpo.util.NodeUtil;

@Path("nodes")
public class NodeService{	
	
	private static final String STATUS = "status";
	private static final String NEW_NODE_ID = "newNodeId";	
	
	
	// old versions
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNode0(String jsonData){			
		
		JsonParser parser = new JsonParser();
		JsonObject values = parser.parse(jsonData).getAsJsonObject();
		String nodeId = values.get(BPOConstants.NODE_ID).getAsString();
		
		return createNode(jsonData, nodeId);		
	}
	
	@POST
	@Path("/delete/{nodeId}")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteNode0(@PathParam(BPOConstants.NODE_ID) String nodeId){	
		return deleteNode(nodeId);
	}
		
	@PUT
	@Path("/update/{nodeId}")	
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateNode0(@PathParam(BPOConstants.NODE_ID) String nodeId, String json){	
		return updateNode(nodeId, json);
	}
	
	@GET
	@Path("/list/{nodeId}/waiting")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWaitingElements0(@PathParam(BPOConstants.NODE_ID) String nodeId){	
		return getElements(nodeId, "waiting");
	}
	
	@GET
	@Path("/list/{nodeId}/processing")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProcessingElements0(@PathParam(BPOConstants.NODE_ID) String nodeId){	
		return getElements(nodeId, "processing");
	}
		
	@GET
	@Path("/list/{nodeId}/elements")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response getAllElements0(@PathParam(BPOConstants.NODE_ID) String nodeId){			
		return getAllElements(nodeId, "");
	}
		
	@GET
	@Path("/list/{nodeId}/elements/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement0(@PathParam(BPOConstants.NODE_ID) String nodeId, @PathParam(BPOConstants.ELEMENT_ID) String elementId){	
		return getElement(nodeId, elementId);
	}
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllNodes0(){	
		return getAllNodes();
	}
		
	@GET
	@Path("/list/{nodeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNode0(@PathParam(BPOConstants.NODE_ID) String nodeId){			
		return getNode(nodeId);
	}
		
	@POST
	@Path("/list/{nodeId}/insert-element")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertElement0(@PathParam(BPOConstants.NODE_ID) String nodeId, String json){			
		JsonObject values = GeneralUtil.parseToJson(json);
		String elementId = values.get(BPOConstants.ELEMENT_ID).getAsString();
		return insertElement(nodeId, elementId, json);
	}
	
	@POST
	@Path("/list/{nodeId}/workers/assign")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response assignWorker0(@PathParam(BPOConstants.NODE_ID) String nodeId,String json){		
		JsonObject values = GeneralUtil.parseToJson(json);
		String workerId = values.get(BPOConstants.WORKER_ID).getAsString().trim();		
		return assignWorker(nodeId, workerId, json);
	}
		
	@POST
	@Path("/list/{nodeId}/workers/unassign")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response unassignWorker0(@PathParam(BPOConstants.NODE_ID) String nodeId,String json){		
		JsonParser parser = new JsonParser();
		JsonObject values = parser.parse(json).getAsJsonObject();
		String workerId = values.get(BPOConstants.WORKER_ID).getAsString();		
		return unassignWorker(nodeId, json, workerId);
	}
	
	@POST
	@Path("/list/{nodeId}/workers/shift")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response shiftWorkers0(@PathParam(BPOConstants.NODE_ID) String nodeId,String json){		
		JsonObject values = GeneralUtil.parseToJson(json);
		String workerId = values.get(BPOConstants.WORKER_ID).getAsString();
		String newNodeId = values.get(BPOConstants.NODE_ID).getAsString();
		return shiftWorkers(nodeId, workerId, newNodeId, json);
	}
	
	@GET
	@Path("/list/{nodeId}/workers/list")
	public Response getWorkerList0(@PathParam(BPOConstants.NODE_ID) String nodeId){
		return getWorkerList(nodeId);
	}
	
	@POST
	@Path("/list/{nodeId}/elements/{elementId}/change-priority")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response changePriority0(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,String json){
		
		return updateElement(nodeId, elementId, json);
	}
	
	@POST
	@Path("/list/{nodeId}/elements/{elementId}/update")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateElement0(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,String json){
		
		return updateElement(nodeId, elementId, json);
	}
	
	@POST
	@Path("/list/{nodeId}/elements/{elementId}/transfer")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response transferElement0(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,String json){
		JsonObject values = GeneralUtil.parseToJson(json);
		String nextNodeId = values.get(BPOConstants.NEXT_NODE_ID).getAsString();
		return transferElement(nodeId, elementId, nextNodeId, json);
	}
	
	@POST
	@Path("/list/{nodeId}/elements/{elementId}/correcting-entry")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response correctingEntry0(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,String json){
		return correctingEntry(nodeId, elementId, json);
	}
	
	@POST
	@Path("/list/{nodeId}/elements/{elementId}/delete")
	public Response deleteElement0(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId){
		return deleteElement(nodeId, elementId);
	}
	
	
	// new versions	
	
	@PUT
	@Path("/{nodeId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createNode(String jsonData, @PathParam(BPOConstants.NODE_ID) String nodeId){			
		try{
			NodeOperationResponse response = NodeUtil.createNode(jsonData, nodeId);
			return GeneralUtil.buildResponse(response);			
				
		} catch(ClassCastException
				|IllegalStateException
				|IllegalArgumentException
				|UnsupportedOperationException
				|NullPointerException e) {
			NodeOperationResponse response = new NodeOperationResponse(false,
					NodeOperation.CREATE, OperationResponseCode.INVALID_PARAMS.getCode());
			
			return Response.status(response.getErrorCode())
					.entity(response.toString())
						   .build();
		}
	}
	
	@DELETE
	@Path("/{nodeId}")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteNode(@PathParam(BPOConstants.NODE_ID) String nodeId){	
		NodeOperationResponse response = NodeUtil.deleteNode(nodeId);
		return GeneralUtil.buildResponse(response);			
	}
	
	@PATCH
	@Path("/{nodeId}")	
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateNode(@PathParam(BPOConstants.NODE_ID) String nodeId, String json){	
		try{
			NodeOperationResponse response = NodeUtil.updateNode(nodeId, json);
			return GeneralUtil.buildResponse(response);						
		} catch(ClassCastException
				|IllegalStateException
				|IllegalArgumentException
				|NullPointerException e) {
			NodeOperationResponse response = new NodeOperationResponse(false,
					NodeOperation.UPDATE_NODE, OperationResponseCode.INVALID_PARAMS.getCode());
			
			return Response.status(response.getErrorCode())
					.entity(response.toString())
						   .build();
		} 
	}
			
	@GET
	@Path("/{nodeId}/elements/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement(@PathParam(BPOConstants.NODE_ID) String nodeId, @PathParam(BPOConstants.ELEMENT_ID) String elementId){	
		NodeOperationResponse response = NodeUtil.getElements(nodeId, elementId);
		return GeneralUtil.buildResponse(response);			
	}
		
	@GET
	@Path("/{nodeId}/elements")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response getAllElements(@PathParam(BPOConstants.NODE_ID) String nodeId, @QueryParam(STATUS) String elemStatus){		
		return getElements(nodeId, elemStatus);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllNodes(){	
		try {
			NodeOperationResponse response = NodeUtil.getNodes();
			Response buildResponse = GeneralUtil.buildResponse(response);
			return buildResponse;				
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}
	
	@GET
	@Path("/{nodeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNode(@PathParam(BPOConstants.NODE_ID) String nodeId){			
		try {
			NodeOperationResponse response = NodeUtil.getNodes(nodeId);
			return GeneralUtil.buildResponse(response);				
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}
	
	@PUT
	@Path("/{nodeId}/elements/{elementId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertElement(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json){	
		ElementOperationResponse response = ElementUtil.insertElement(nodeId,elementId,json);
		return GeneralUtil.buildResponse(response);			
	}
	
	@PUT
	@Path("/{nodeId}/workers/{workerId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response assignWorker(@PathParam(BPOConstants.NODE_ID) String nodeId, @PathParam(BPOConstants.WORKER_ID) String workerId, String json){
		NodeOperationResponse response = NodeUtil.assignWorker(nodeId, workerId, json);		
		return GeneralUtil.buildResponse(response);
	}
	
	@PATCH
	@Path("/{nodeId}/workers/{workerId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response unassignWorker(@PathParam(BPOConstants.NODE_ID) String nodeId,String json, @PathParam(BPOConstants.WORKER_ID) String workerId){
		NodeOperationResponse response = NodeUtil.unassignWorker(nodeId, json, workerId);
		return GeneralUtil.buildResponse(response);			
	}
	
	@POST
	@Path("/{nodeId}/workers/{workerId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response shiftWorkers(@PathParam(BPOConstants.NODE_ID) String nodeId, @PathParam(BPOConstants.WORKER_ID) String workerId,
			@QueryParam(NEW_NODE_ID) String newNodeId, String json){
		NodeOperationResponse response = NodeUtil.shiftWorker(nodeId, workerId, newNodeId, json);
		return GeneralUtil.buildResponse(response);	
	}

	@GET
	@Path("/{nodeId}/workers")
	public Response getWorkerList(@PathParam(BPOConstants.NODE_ID) String nodeId){
		NodeOperationResponse response = NodeUtil.getWorkers(nodeId);
		return GeneralUtil.buildResponse(response);			
	}
	
	@POST
	@Path("/{nodeId}/elements/{elementId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response transferElement(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,
			@QueryParam(NEW_NODE_ID) String newNodeId, String json){		
		ElementOperationResponse response = ElementUtil.transferElement(nodeId, elementId, newNodeId, json);
		return GeneralUtil.buildResponse(response);			
	}
	
	@POST
	@Path("/{nodeId}/elements/{elementId}/correcting-entry")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response correctingEntry(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId,String json){
		ElementOperationResponse response = ElementUtil.correctEntry(nodeId, elementId, json);
		return GeneralUtil.buildResponse(response);		
	}
	
	@DELETE
	@Path("/{nodeId}/elements/{elementId}")
	public Response deleteElement(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId){
		ElementOperationResponse response = ElementUtil.deleteElement(nodeId, elementId);
		return GeneralUtil.buildResponse(response);			
	}
		
	@POST		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response nodeBatchUpload(@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail){	
		
		NodeUtil util = new NodeUtil();
		return util.batchUpload(uploadedInputStream, fileDetail);		
	}
	
	@PATCH	
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response nodeBatchUpdate(@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail){	
		
		NodeUtil util = new NodeUtil();
		return util.batchUpdate(uploadedInputStream, fileDetail);		
	}
		
	@POST
	@Path("/workers")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response assignWorkerBatchUpload(@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail){	
		
		NodeUtil util = new NodeUtil();
		return util.batchAssignWorker(uploadedInputStream, fileDetail);			
	}
		
	@PATCH
	@Path("/workers")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response unassignWorkerBatchUpload(@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail){	
		
		NodeUtil util = new NodeUtil();
		return util.batchUnassignWorker(uploadedInputStream, fileDetail);		
	}	
	
	@PATCH
	@Path("/{nodeId}/elements/{elementId}")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateElement(@PathParam(BPOConstants.NODE_ID) String nodeId,@PathParam(BPOConstants.ELEMENT_ID) String elementId, String json){
		
		ElementOperationResponse response = null;		
		response = ElementUtil.updateElement2(nodeId, elementId, json);				
		return GeneralUtil.buildResponse(response);	
	}

	@DELETE
	@Path("/{nodeId}/elements")	
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteNodeElements(@PathParam("nodeId") String nodeId){	
		NodeOperationResponse response = NodeUtil.deleteNodeElements(nodeId);
		if(response.isSuccessful()){
			return Response.ok().entity(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString())
					   .build();
		}
	}
	
	public Response getElements(@PathParam(BPOConstants.NODE_ID) String nodeId, @QueryParam(STATUS) String elemStatus){	
		NodeOperationResponse response = null;
		if(elemStatus == null || elemStatus.trim().isEmpty()){
			response = NodeUtil.getElements(nodeId);
		} else if("processing".equalsIgnoreCase(elemStatus.trim())){
			response = NodeUtil.getProcessingElements(nodeId);
		}else if("waiting".equalsIgnoreCase(elemStatus.trim())){
			response = NodeUtil.getWaitingElements(nodeId);			
		}else{
			response = new NodeOperationResponse(false, NodeOperation.VIEW_ELEM, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
		}	
		
		return GeneralUtil.buildResponse(response);			
	}
	
	// new versions	
	
	@GET
	@Path("/element/{elementId}")
	public Response getElementAcrossAllNodes(@PathParam(BPOConstants.ELEMENT_ID) String elementId){			
		NodeOperationResponse response = NodeUtil.getNodes();
		NodeOperationResponse nodeResponse = new NodeOperationResponse(false,NodeOperation.VIEW_ELEM,OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
		for (Node node : response.getNodes()) {
			Set<String> elementIds = node.getElements().stream().map(k -> k.getElementId()).collect(Collectors.toSet());
			if(elementIds.contains(elementId)){
				nodeResponse = new NodeOperationResponse(true, NodeOperation.VIEW_NODE);
				List<Node> nodes = new ArrayList<>();
				nodes.add(node);
				nodeResponse.setNodes(nodes);
				return GeneralUtil.buildResponse(nodeResponse);
			}
		}
			
	 return GeneralUtil.buildResponse(nodeResponse);			
	}

	@PATCH
	@Path("/{nodeId}/services")
	@Consumes(MediaType.APPLICATION_JSON)	
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateServices(@PathParam(BPOConstants.NODE_ID) String nodeId,String json){			
		NodeOperationResponse response = NodeUtil.updateNodeServices(nodeId, json);
	 return GeneralUtil.buildResponse(response);			
	}
}
