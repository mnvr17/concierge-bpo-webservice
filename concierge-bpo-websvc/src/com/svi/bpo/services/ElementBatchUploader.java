package com.svi.bpo.services;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;



@Path("elementsupload")
public class ElementBatchUploader {	
	
	private ElementService elementUploader;
	
	@POST
	@Path("/batchcreate")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response ElementBatchUpload(@FormDataParam("bpo-batch-create-element") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-create-element") FormDataContentDisposition fileDetail){		
		
		elementUploader = new ElementService();		
		return elementUploader.elementBatchUpload(uploadedInputStream, fileDetail);
	}
	
	@POST
	@Path("/batchupdate")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response ElementBatchUpdate(@FormDataParam("bpo-batch-update-element") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-update-element") FormDataContentDisposition fileDetail){		
		
		elementUploader = new ElementService();		
		return elementUploader.elementBatchUpdate(uploadedInputStream, fileDetail);		
	}	
	

}