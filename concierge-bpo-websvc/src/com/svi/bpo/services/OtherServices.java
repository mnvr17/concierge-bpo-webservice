package com.svi.bpo.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import com.svi.bpo.backup.Backup;
import com.svi.bpo.backup.BackupEnum;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.nodestats.NodeStatEnum;
import com.svi.bpo.response.OtherServicesResponse;

@Path("others")
public class OtherServices {	
	
	
	
	@GET
	@Path("/write-backup")
	@Produces(MediaType.APPLICATION_JSON)
	public Response writeBAckup(){	
		OtherServicesResponse resp;
		if(BackupEnum.BACKUP_ENABLED.getVal().trim().equalsIgnoreCase("Y")){
			resp = new OtherServicesResponse(false,"WRITING BACKUP", 473, "Automatic Writing of Backup was Enabled");			
			return Response.status(resp.getErrorCode()).entity(resp.toString())
					   .build();
		}
		Backup.writeBackup();
		
		resp = new OtherServicesResponse(true,"WRITING BACKUP");		
		
		return Response.status(resp.getErrorCode()).entity(resp.toString())
				   .build();
	}
	
	
	
	

}
