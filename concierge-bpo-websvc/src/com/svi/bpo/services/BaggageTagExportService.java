package com.svi.bpo.services;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("exportBaggagetag")
public class BaggageTagExportService {
	// dummy hard coded path, to be edited
	private BaggageTagService baggageTagService;	
	
	@GET
	@Path("/yearly/{year}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagYearly(@PathParam("year") String year) throws IOException{	
		
		baggageTagService = new BaggageTagService();
		return baggageTagService.getBaggagetagYearly(year);				
	}
	
	@GET
	@Path("/monthly/{year}/{month}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagMonthly(@PathParam("year") String year, @PathParam("month") String month) throws IOException{			
		
		baggageTagService = new BaggageTagService();
		return baggageTagService.getBaggagetagMonthly(year, month);	
	}
	
	
	@GET
	@Path("/daily/{year}/{month}/{day}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagDaily(@PathParam("year") String year, @PathParam("month") String month, @PathParam("day") String day) throws IOException{			
		
		baggageTagService = new BaggageTagService();
		return baggageTagService.getBaggagetagDaily(year, month, day);	
	}
	
	@GET
	@Path("/status/{filesLocation}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkFilePathStatus(@PathParam("filesLocation") String filesLocation) throws IOException{	
		
		baggageTagService = new BaggageTagService();
		return baggageTagService.checkFilePathStatus(filesLocation);		
	}
	
}
