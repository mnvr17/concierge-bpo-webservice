package com.svi.bpo.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.svi.bpo.baggagetag.BaggageTagEnum;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.ElementRepository;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.response.ElementOperationResponse;
import com.svi.bpo.response.OperationResponseCode;
import com.svi.bpo.util.ElementUtil;
import com.svi.bpo.util.GeneralUtil;

@Path("elements")
public class ElementService {
	private int columnNumErrorCreate;
	private int columnNumErrorUpdate;
	private static final int ELEMENT_TEMPLATE_HEADER_COUNT = 13;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response elementBatchUpload(
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail) {
		
		ByteArrayOutputStream out = null;
		if (!(uploadedInputStream == null || fileDetail == null)) {			
			Map<String, Integer> erroneousElements = new HashMap<>();
			Map<String, String> errorMessages = new HashMap<>();	

			try {
				// getting the excel extension whether it is xls or xlsx
				String extension = FilenameUtils.getExtension(fileDetail.getFileName());

				// create a workbook based on excel file version
				Workbook workbook = null;
				if (extension.equalsIgnoreCase(BPOConstants.XLS)) {
					workbook = new HSSFWorkbook(uploadedInputStream);
				} else {
					workbook = new XSSFWorkbook(uploadedInputStream);
				}

				// getting the first work sheet of the excel file
				Sheet sheet = workbook.getSheetAt(0);

				// iterating in each row of the file
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) {
					// converting each row data into node object
					Row row = sheet.getRow(i);
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = createElement(row, errorMessages);
						if (!isSuccessful) {
							erroneousElements.put("" + (i + 1), columnNumErrorCreate);
						}
					}
				}

				if (!erroneousElements.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousElements, extension, errorMessages, ELEMENT_TEMPLATE_HEADER_COUNT);
				}

				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}

			if (erroneousElements.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();
			}
		} else {
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}

	@PATCH
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response elementBatchUpdate(
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) InputStream uploadedInputStream,
			@FormDataParam(BPOConstants.BPO_BATCH_UPLOAD) FormDataContentDisposition fileDetail) {
		Map<String, Integer> erroneousElements = new HashMap<>();
		Map<String, String> errorMessages = new HashMap<>();	
		ByteArrayOutputStream out = null;

		if (!(uploadedInputStream == null || fileDetail == null)) {

			try {
				// getting the excel extension whether it is xls or xlsx
				String extension = FilenameUtils.getExtension(fileDetail.getFileName());
				// create a workbook based on excel file version
				Workbook workbook = null;
				if (extension.equalsIgnoreCase(BPOConstants.XLS)) {
					workbook = new HSSFWorkbook(uploadedInputStream);
				} else {
					workbook = new XSSFWorkbook(uploadedInputStream);
				}
				// getting the first work sheet of the excel file
				Sheet sheet = workbook.getSheetAt(0);
				// iterating in each row of the file
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) {
					// converting each row data into node object
					Row row = sheet.getRow(i);
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = updateElement(row, errorMessages);
						if (!isSuccessful) {							
							erroneousElements.put("" + (i + 1), columnNumErrorUpdate);
						}
					}
				}

				if (!erroneousElements.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousElements, extension, errorMessages, ELEMENT_TEMPLATE_HEADER_COUNT);
				}

				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}

			if (erroneousElements.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();
			}
		} else {
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}
	
	/**
	 * Creates new element per row.
	 * 
	 * @param row
	 *            The row to be parsed as one element.
	 * @return TRUE if creation of new element is successful. Otherwise, returns
	 *         FALSE.
	 */
	private boolean createElement(Row row, Map<String, String> errorMessages) {
		// getting basic info
		String elementId = "";
		String elementName = "";
		String nodeId = "";
		// String workerId = ""; // ignore when creating new elements
		int priority = 0;
		String fileLocation = "";
		// String status = ""; //default is WAITING when creating new elements
		int queueIndex = 0; // default value is zero

		// getting element id
		if (!GeneralUtil.isNullOrEmpty(row.getCell(0))) {
			elementId = GeneralUtil.getCellValue(row.getCell(0));
		} else {
			columnNumErrorCreate = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Element Id");
			return false;
		}

		if (StringUtils.isNotBlank(elementId) && ElementRepository.contains(elementId)) {
			columnNumErrorCreate = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Element Id Already Exists");
			return false;
		}

		// getting element name
		if (!GeneralUtil.isNullOrEmpty(row.getCell(1))) {
			elementName = GeneralUtil.getCellValue(row.getCell(1));
		} else {
			columnNumErrorCreate = 2;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Element Name");
			return false;
		}

		// getting element node id
		if (!GeneralUtil.isNullOrEmpty(row.getCell(2))) {
			nodeId = GeneralUtil.getCellValue(row.getCell(2));
		} else {
			columnNumErrorCreate = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Node Id");
			return false;
		}

		// getting element priority
		if (!GeneralUtil.isNullOrEmpty(row.getCell(4))) {
			try {
				priority = (int) Double.parseDouble(GeneralUtil.getCellValue(row.getCell(4)));
				if (priority < 0 || priority > 9) {
					columnNumErrorCreate = 5;
					errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Priority Value, Should be from 0 to 9");
					return false;
				}
			} catch (NumberFormatException nfe) {
				columnNumErrorCreate = 5;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Priority Value");
				return false;
			}
		} else {
			columnNumErrorCreate = 5;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Priority Input");
			return false;
		}

		// getting elem fileLocation
		if (!GeneralUtil.isNullOrEmpty(row.getCell(5))) {
			fileLocation = GeneralUtil.getCellValue(row.getCell(5));
		}

		// getting extra details
		Map<String, String> extraDetails = new HashMap<String, String>();
		List<String> validExtraDetails = new ArrayList<>();

		if (StringUtils.isNotBlank(BaggageTagEnum.EXTRA_DETAIL_KEYS.getVal())) {
			validExtraDetails = Arrays.asList(BaggageTagEnum.EXTRA_DETAIL_KEYS.getVal().split("\\|"));
		}

		for (int k = 0; k < 3; k++) { // reading the three extra details
			boolean isValidLabel = true;
			boolean isValidDetail = true;

			String label = "";
			String detail = "";

			// getting the label
			if (!GeneralUtil.isNullOrEmpty(row.getCell(6 + (2 * k) + 1))) {
				label = GeneralUtil.getCellValue(row.getCell(6 + (2 * k) + 1));
			} else {
				isValidLabel = false;
			}

			// Check if label is valid
			if (isValidLabel && !validExtraDetails.contains(label)) {
				isValidLabel = false;
			}

			// getting the extra detail
			if (!GeneralUtil.isNullOrEmpty(row.getCell(6 + (2 * k) + 2))) {
				detail = GeneralUtil.getCellValue(row.getCell(6 + (2 * k) + 2));
			} else {
				isValidDetail = false;
			}

			// skip extra detail if both are empty or null
			if ((!isValidLabel) && (!isValidDetail)) {
				continue;
			}

			if (!isValidLabel) {
				columnNumErrorCreate = 6 + (2 * k) + 2;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Production Label");
				return false;
			}
			if (!isValidDetail) {
				columnNumErrorCreate = 6 + (2 * k) + 3;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Production Detail");
				return false;
			}

			// adding in the list
			if (!extraDetails.containsKey(label)) {
				extraDetails.put(label, detail);
			} else {
				columnNumErrorCreate = 6 + (2 * k) + 2;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Duplicate Production Label");
				return false;
			}

		}

		// getting and adding the indicated node
		Node node = NodeRepository.getNode(nodeId);
		if (node != null) {
			if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
				columnNumErrorCreate = 3;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Node status is CLOSED. Cannot add element to CLOSED node.");
				return false;
			}
			Element element = new Element(elementId, elementName, nodeId, queueIndex, priority, fileLocation,
					extraDetails);
			if (node.enqueueElement(element, true)) {
				return true;
			} else {				
				columnNumErrorCreate = 1;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Cannot Specify Cause of Error");
				return false;
			}
		} else {
			columnNumErrorCreate = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Node Id Does Not Exist");
			return false;
		}
	}

	/**
	 * Updates element as defined in each row.
	 * 
	 * @param row
	 *            The row associated to the element to be updated.
	 * @return TRUE if update of element is successful. Otherwise, returns
	 *         FALSE.
	 */
	private boolean updateElement(Row row, Map<String, String> errorMessages) {
		// getting basic info
		String elementId = "";
		String elementName = "";
		int priority = 0;
		String fileLocation = "";
		// getting element id
		if (!GeneralUtil.isNullOrEmpty(row.getCell(0))) {
			elementId = GeneralUtil.getCellValue(row.getCell(0));
		} else {
			columnNumErrorUpdate = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Empty Element Id");
			return false;
		}
		// checks if the element exists
		Element element = ElementRepository.getElement(elementId);
		if (element == null) {
			columnNumErrorUpdate = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Element Id Does Not Exist");
			return false;
		}

		// getting element name
		if (!GeneralUtil.isNullOrEmpty(row.getCell(1))) {
			elementName = GeneralUtil.getCellValue(row.getCell(1));
		}

		// getting element priority
		if (!GeneralUtil.isNullOrEmpty(row.getCell(4))) {
			try {
				priority = (int) Double.parseDouble(GeneralUtil.getCellValue(row.getCell(4)));
				if (priority < 0 || priority > 9) {
					columnNumErrorUpdate = 5;
					errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Invalid Priority Value, Should be from 0 to 9");
					return false;
				}
			} catch (NumberFormatException nfe) {
				columnNumErrorUpdate = 5;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Invalid Priority Value");
				return false;
			}
		}
		// getting elem fileLocation
		if (!GeneralUtil.isNullOrEmpty(row.getCell(5))) {
			fileLocation = GeneralUtil.getCellValue(row.getCell(5));
		}

		// getting extra details
		Map<String, String> extraDetails = new HashMap<String, String>();
		// reading the three extra details
		for (int k = 0; k < 3; k++) {
			boolean isValidLabel = true;
			boolean isValidDetail = true;
			String label = "";
			String detail = "";
			// getting the label
			if (!GeneralUtil.isNullOrEmpty(row.getCell(6 + (2 * k) + 1))) {
				label = GeneralUtil.getCellValue(row.getCell(6 + (2 * k) + 1));
			} else {
				isValidLabel = false;
			}
			// getting the extra detail
			if (!GeneralUtil.isNullOrEmpty(row.getCell(6 + (2 * k) + 2))) {
				detail = GeneralUtil.getCellValue(row.getCell(6 + (2 * k) + 2));
			} else {
				isValidDetail = false;
			}
			// skip extra detail if both are empty or null
			if ((!isValidLabel) && (!isValidDetail)) {
				continue;
			}
			// checks if key is valid and exists in the map of element extra
			// details
			if (!isValidLabel || !element.extraDetailExists(label)) {
				columnNumErrorUpdate = 6 + (2 * k) + 2;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Empty label or Label Does Not Exist in this Element");
				return false;
			}
			// adding in the list
			if (!extraDetails.containsKey(label)) {
				extraDetails.put(label, detail);
			} else {
				columnNumErrorUpdate = 6 + (2 * k) + 2;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Duplicate Label");
				return false;
			}
		}

		// updating element name
		if (elementName.trim().length() != 0) {
			element.setElementName(elementName);
		}
		// updating element priority
		if (!GeneralUtil.isNullOrEmpty(row.getCell(4))) {
			element.setPriority(priority);
		}
		// updating element file location
		if (fileLocation.trim().length() != 0) {
			element.setFileLocation(fileLocation);
		}
		// updating extra details
		for (Map.Entry<String, String> entry : extraDetails.entrySet()) {
			// updating the map whose update value is non-empty
			if (entry.getValue().trim().length() != 0) {
				element.updateExtraDetail(entry.getKey(), entry.getValue());
			}
		}
		return true;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteElements() {

		ElementOperationResponse response = ElementUtil.deleteAllElements();
		if (response.isSuccessful()) {
			return Response.ok().entity(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString()).build();
		}

	}

	@GET
	@Path("/{elementId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getElement(@PathParam("elementId") String elementId) {

		ElementOperationResponse response = ElementUtil.getElement(elementId);
		if (response.isSuccessful()) {
			return Response.ok().entity(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString()).build();
		}
	}

}
