package com.svi.bpo.services;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.zeroturnaround.zip.ZipUtil;

import com.svi.bpo.nodestats.NodeStatEnum;
import com.svi.bpo.response.ExportFileResponse;
import com.svi.bpo.response.OperationResponseCode;

@Path("nodestats")
public class NodeStatsService {

	@GET
	@Path("/{year}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagYearly(@PathParam("year") String year)
			throws IOException {
		// getting the date now
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		Date now = new Date();
		String strDate = sdf.format(now).replace("-", "");
		// getting and creating the files
		File yearDirectory = new File(NodeStatEnum.NODE_STATS_DIR.getVal() + "/" + year);
		File yearZipDirectory = new File("nodestats-" + year + "-asof-"
				+ strDate + ".zip");
		// checks if the directory exists and the path is a directory
		if (yearDirectory.exists() && yearDirectory.isDirectory()) {
			// zipping the directory
			ZipUtil.pack(yearDirectory, yearZipDirectory);
			// returning the zipped file
			return Response
					.ok(yearZipDirectory, MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition",
							"attachment; filename=\""
									+ yearZipDirectory.getName() + "\"") // optional
					.build();
		} else {
			return Response
					.status(OperationResponseCode.FILE_NOT_FOUND.getCode())
					.entity("Requested file does not exist or is not a directory.")
					.type(MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/{year}/{month}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagMonthly(@PathParam("year") String year,
			@PathParam("month") String month) throws IOException {
		// getting the date now
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		Date now = new Date();
		String strDate = sdf.format(now).replace("-", "");
		// getting and creating the files
		File monthDirectory = new File(NodeStatEnum.NODE_STATS_DIR.getVal() + "/" + year + "/" + month);
		File monthZipDirectory = new File("nodestats-" + year + month
				+ "-asof-" + strDate + ".zip");
		// checks if the directory exists and the path is a directory
		if (monthDirectory.exists() && monthDirectory.isDirectory()) {
			// zipping the directory
			ZipUtil.pack(monthDirectory, monthZipDirectory);
			// returning the zipped file
			return Response
					.ok(monthZipDirectory, MediaType.APPLICATION_OCTET_STREAM)
					.header("Content-Disposition",
							"attachment; filename=\""
									+ monthZipDirectory.getName() + "\"") // optional
					.build();
		} else {
			return Response
					.status(OperationResponseCode.FILE_NOT_FOUND.getCode())
					.entity("Requested file does not exist or is not a directory.")
					.type(MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/{year}/{month}/{day}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getBaggagetagDaily(@PathParam("year") String year,
			@PathParam("month") String month, @PathParam("day") String day)
			throws IOException {
		// getting the date now
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		Date now = new Date();
		String strDate = sdf.format(now).replace("-", "");
		// getting and creating the files
		File monthDirectory = new File(NodeStatEnum.NODE_STATS_DIR.getVal() + "/" + year + "/" + month);
		File dailyZipDirectory = new File("nodestats-" + year + month + day
				+ "-asof-" + strDate + ".zip");
		// checks if the directory exists and the path is a directory
		if (monthDirectory.exists() && monthDirectory.isDirectory()) {
			// getting all the files to be zipped
			File[] directoryListing = monthDirectory.listFiles();
			ArrayList<File> filesToZip = new ArrayList<File>();
			for (File child : directoryListing) {
				if (child.getName().startsWith(year + month + day)) {
					filesToZip.add(child);
				}
			}
			if (filesToZip.size() != 0) {
				// zipping the file
				File[] filesTobeZipped = filesToZip.toArray(new File[filesToZip
						.size()]);
				ZipUtil.packEntries(filesTobeZipped, dailyZipDirectory);
				// returning the zipped file
				return Response
						.ok(dailyZipDirectory,
								MediaType.APPLICATION_OCTET_STREAM)
						.header("Content-Disposition",
								"attachment; filename=\""
										+ dailyZipDirectory.getName() + "\"") // optional
						.build();
			} else {
				return Response
						.status(OperationResponseCode.FILE_NOT_FOUND.getCode())
						.entity("Requested file does not exist or is not a directory.")
						.type(MediaType.APPLICATION_JSON).build();			
			}
		} else {
			return Response
					.status(OperationResponseCode.FILE_NOT_FOUND.getCode())
					.entity("Requested file does not exist or is not a directory.")
					.type(MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/status/{filesLocation}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkFilePathStatus(
			@PathParam("filesLocation") String filesLocation)
			throws IOException {
		String[] fileLoc = filesLocation.split("-");
		if (fileLoc.length <= 2) {

			String path = "";
			for (String str : fileLoc) {
				path = path + "/" + str;
			}
			File monthDirectory = new File(NodeStatEnum.NODE_STATS_DIR.getVal() + path);
			if (monthDirectory.exists() && monthDirectory.isDirectory()) {
				ExportFileResponse response = new ExportFileResponse(true,
						filesLocation);
				return Response.ok().entity(response.toString()).build();
			} else {
				ExportFileResponse response = new ExportFileResponse(false,
						filesLocation,
						OperationResponseCode.FILE_NOT_FOUND.getCode());
				return Response.status(response.getErrorCode())
						.entity(response.toString()).build();
			}

		} else {
			String path = "";
			String fileName = "";
			int counter = 0;
			for (String str : fileLoc) {
				if (counter <= 1) {
					path = path + "/" + str;
				}
				fileName = fileName + str;
				counter++;
			}
			File monthDirectory = new File(NodeStatEnum.NODE_STATS_DIR.getVal() + path);
			if (monthDirectory.exists() && monthDirectory.isDirectory()) {
				// getting all the files to be zipped
				File[] directoryListing = monthDirectory.listFiles();
				ArrayList<File> filesToZip = new ArrayList<File>();
				for (File child : directoryListing) {
					if (child.getName().startsWith(fileName)) {
						filesToZip.add(child);
					}
				}
				if (filesToZip.size() != 0) {
					ExportFileResponse response = new ExportFileResponse(true,
							filesLocation);
					return Response.ok().entity(response.toString()).build();
				} else {
					ExportFileResponse response = new ExportFileResponse(false,
							filesLocation,
							OperationResponseCode.FILE_NOT_FOUND.getCode());
					return Response.status(response.getErrorCode())
							.entity(response.toString()).build();
				}
			} else {
				ExportFileResponse response = new ExportFileResponse(false,
						filesLocation,
						OperationResponseCode.FILE_NOT_FOUND.getCode());
				return Response.status(response.getErrorCode())
						.entity(response.toString()).build();
			}
		}
	}

}
