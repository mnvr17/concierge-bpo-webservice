package com.svi.bpo.services;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;



@Path("nodesupload")
public class NodeBatchUploader {
	private NodeService nodeService;	
	
	@POST
	@Path("/batchcreate")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response NodeBatchUpload(@FormDataParam("bpo-batch-create-node") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-create-node") FormDataContentDisposition fileDetail){	
		
		nodeService = new NodeService();
		return nodeService.nodeBatchUpload(uploadedInputStream, fileDetail);
		
	}
	
	@POST
	@Path("/batchupdate")		
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response NodeBatchUpdate(@FormDataParam("bpo-batch-update-node") InputStream uploadedInputStream,
			@FormDataParam("bpo-batch-update-node") FormDataContentDisposition fileDetail){	
		
		nodeService = new NodeService();
		return nodeService.nodeBatchUpdate(uploadedInputStream, fileDetail);
	}
}
