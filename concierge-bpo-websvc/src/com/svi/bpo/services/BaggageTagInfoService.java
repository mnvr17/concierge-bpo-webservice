package com.svi.bpo.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svi.bpo.baggagetag.BaggageTagBuffer;

@Path("baggagetags0")
public class BaggageTagInfoService {
	
	/**
	 * This class seems not to be used in other projects and classes.
	 * To be confirmed if this should be deleted.
	 * Old root path name: baggagetags
	 * Reason for change: So as not to conflict with root path of BaggageTagService class
	 * 
	 * This can actually be deleted since this service was transferred to BaggageTagService class.
	 * 
	 */
	
	
	@GET
	@Path("/count")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response getCount(){	
		return Response.ok()
				.entity("{\"count\":"+BaggageTagBuffer.getBaggageTagCount()+"}")
				.build();
	}
}
