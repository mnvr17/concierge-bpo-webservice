package com.svi.bpo.nodestats;

import org.ini4j.Profile.Section;



public enum NodeStatEnum {
	NODE_STATS_DIR("NODE_STATS_DIR"),
	NODE_STATS_SCHED_DUR_WRITE_SLEEP_SEC("NODE_STATS_SCHED_DUR_WRITE_SLEEP_SEC"),
	NODE_STATS_TIMEZONE("NODE_STATS_TIMEZONE");
	
	private String val = "";
	
	NodeStatEnum(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
	
	public static void setContext(Section workers){
		for(NodeStatEnum each : values()){
			each.setVal(workers.get(each.getVal()));
		}
	}
}
