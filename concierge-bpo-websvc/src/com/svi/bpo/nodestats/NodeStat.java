package com.svi.bpo.nodestats;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.svi.bpo.baggagetag.BaggageTagEnum;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.object.ServiceType;

public class NodeStat {
	// --------------------------------- VARIABLES
	// ------------------------------------//
	private static boolean threadIsRunning = true;
	private static long interval = 10000;
	private static Thread bufferThread;
	private static Interval today = new Interval(DateTime.now().withTimeAtStartOfDay(), Days.ONE);
	private static DateTimeFormatter dtf;
	private static final String DELIMITER = ",";
	private static Logger logger = null;

	static class NodeStatThread implements Runnable {

		@Override
		public void run() {
			while (threadIsRunning) {
				try {
					Thread.sleep(interval);
					writeNodeStat();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				if (!threadIsRunning) {
					return;
				}
			}

		}

	}

	private static void writeNodeStat() {
		List<Node> nodeList = NodeRepository.getAllNodes();

		for (Node node : nodeList) {
			StringBuilder sb = new StringBuilder();
			
			sb.append(dtf.print(System.currentTimeMillis()));
			sb.append(DELIMITER);
			sb.append(node.getNodeId());
			
			sb.append(DELIMITER);
			sb.append(node.getMinWaitDur());
			sb.append(DELIMITER);
			sb.append(node.getAveWaitDur());
			sb.append(DELIMITER);
			sb.append(node.getMaxWaitDur());
			sb.append(DELIMITER);
			sb.append(node.getMinProcDur());
			sb.append(DELIMITER);
			sb.append(node.getAveProcDur());
			sb.append(DELIMITER);
			sb.append(node.getMaxProcDur());
			
			for(String s : BaggageTagEnum.PROD_OUTPUT_UNIT_KEYS.getVal().split("\\|")){
				ProductionOutputUnit prod = node.getProductionOutputUnit(s);
				if(prod != null){
					sb.append(DELIMITER);
					sb.append(prod.getOutputCount());
					sb.append(DELIMITER);
					sb.append(prod.getErrorCount());
				} else {
					sb.append(",0,0");
				}
			}

			sb.append(DELIMITER);
			sb.append(node.getTotalWaitingElemCount());
			sb.append(DELIMITER);
			sb.append(node.getTotalProcessingElemCount());
			sb.append(DELIMITER);
			sb.append(node.getTotalCompletedElemCount());
			sb.append(DELIMITER);
			sb.append(node.getTotalExceptionCount());
			
			
			logger.info(sb.toString());
			if(!today.contains(System.currentTimeMillis())){
				nodeReset(node);
			}
		}

		if(!today.contains(System.currentTimeMillis())){
			today = new Interval(DateTime.now().withTimeAtStartOfDay(), Days.ONE);
		}
	}

	private static void nodeReset(Node node) {
		node.setAveProcDur(new BigDecimal(0));
		node.setMaxProcDur(0L);
		node.setMinProcDur(0L);
		node.setAveWaitDur(new BigDecimal(0));
		node.setMaxWaitDur(0L);
		node.setMinWaitDur(0L);
		node.setTotalExceptionCount(0);
		node.setTotalCompletedElemCount(0);
	}

	public static void stopThread() {
		if (bufferThread != null && bufferThread.isAlive()) {
			threadIsRunning = false;
			bufferThread.interrupt();
		}
	}

	public static void startThread() {
		int bufferTimeInMinutes = Integer.parseInt(NodeStatEnum.NODE_STATS_SCHED_DUR_WRITE_SLEEP_SEC.getVal());
		interval = 1L * (bufferTimeInMinutes*1000);
		dtf = DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss.SSS").withZone(DateTimeZone.forID(NodeStatEnum.NODE_STATS_TIMEZONE.getVal()));
		logger = LoggerFactory.getLogger("nodestats");
		bufferThread = new Thread(new NodeStatThread());
		bufferThread.start();
	}
}
