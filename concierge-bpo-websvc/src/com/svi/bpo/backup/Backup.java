package com.svi.bpo.backup;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.google.common.base.Strings;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;
import com.svi.bpo.baggagetag.BaggageTagEnum;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.bpo.nodestats.NodeStatEnum;
import com.svi.object.ServiceObj;
import com.svi.object.TriggerType;

import ch.qos.logback.classic.ClassicConstants;

/**
 * author: jmhinolan and rfuentes
 * start date: 1/30/2017
 * end feb 1
 * ***/

/***
 * The backup class backups the nodes and elements on a specified path on
 * logback xml
 * 
 **/
public class Backup {
	private static boolean threadIsRunning = true;
	private static long interval = 10000;
	private static Thread bufferThread;
	private static DateTimeFormatter dtf;
	private static final String DELIMITER = ",";
	private static Logger nodeLogger = null;
	private static Logger elemLogger = null;
	// private static Logger workerLogger = null;

	private static ExecutorService executor = Executors.newSingleThreadExecutor();

	static class BackupThread implements Runnable {

		@Override
		public void run() {

			while (threadIsRunning) {
				try {
					Thread.sleep(interval);
					writeBackup();
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				if (!threadIsRunning) {
					return;
				}
			}
		}

	}

	public static void writeBackup() {
		Backup.dtf = DateTimeFormat.forPattern("yyyyMMdd'T'HHmmssSSS")
				.withZone(DateTimeZone.forID(BackupEnum.BACKUP_TIMEZONE.getVal()));
		nodeLogger = LoggerFactory.getLogger("nodebackup");
		elemLogger = LoggerFactory.getLogger("elembackup");
		String fileNameDate = dtf.toString();
		writeNodeBackup(fileNameDate);
		writeElementBackup(fileNameDate);

		// deleting unnecessary files, files beyond set backup file count limit
		deleteOldBackup();
	}

	private static void deleteOldBackup() {
		int maxFileCountNodes = Integer.parseInt(BackupEnum.BACKUP_NODES_MAX_FILECOUNT.getVal());
		int maxFileCountElems = Integer.parseInt(BackupEnum.BACKUP_ELEMS_MAX_FILECOUNT.getVal());
		String nodeFolder = "node";
		String elementFolder = "element";

		// the string path use is in the restore is in config.ini
		// the string path use in back up can be found in logback xml
		// need to modify them if you modify one
		String nodePath = BackupEnum.BACKUP_DIR.getVal() + File.separator + nodeFolder;
		String elementPath = BackupEnum.BACKUP_DIR.getVal() + File.separator + elementFolder;

		// System.out.println(String.format("Paths: %s\n%s", nodePath,
		// elementPath));

		removeOldFilesExceptLatest(maxFileCountNodes, nodePath);
		removeOldFilesExceptLatest(maxFileCountElems, elementPath);
	}

	private static void removeOldFilesExceptLatest(int maxFileCount, String filePath) {

		File fl = new File(filePath);
		if (fl.listFiles() == null) {
			return;
		}

		File[] files = fl.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		TreeSet<File> set = new TreeSet<>(NameFileComparator.NAME_INSENSITIVE_COMPARATOR);

		set.addAll(Arrays.asList(files));

		while (set.size() > maxFileCount) {
			File logFile = set.pollFirst();
			logFile.delete();
		}

	}

	/**
	 * writes the backup element format : elemLocation(process,queue or
	 * returned),elementId,nodeId,workerId,fileLocation,priority,queueIndex,status,
	 * startProcTime,startWaitTime,endProc,processingDuration,waitingDuration,extradetails(key,Value)
	 */
	private static void writeElementBackup(String fileNameDate) {
		System.out.println("Writing element backup...");
		List<Node> nodeList = NodeRepository.getAllNodes();
		MDC.put("elem_key", dtf.print(System.currentTimeMillis()));
		
		// create directory if it does not exist
		String directoryPath = BackupEnum.BACKUP_DIR.getVal() + File.separator + "element";
		File dir = new File(directoryPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String backupFileName = directoryPath + File.separator + "elem_backup_" + fileNameDate;
		Path tempPath = Paths.get(backupFileName + "_temp" + ".csv");

		try {
			// create writer
			Writer writer = Files.newBufferedWriter(tempPath);

			CSVWriter csvWriter = new CSVWriter(writer, ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER, ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END);
					
			for (Node node : nodeList) {

				List<Element> elems = node.getElements();
				for (Element elem : elems) {
					ReentrantLock lock = elem.getLock();
					lock.lock();
					try {
						StringBuilder sb = new StringBuilder();
						String elementId = elem.getElementId();
						String nodeId = elem.getNodeId();

						Node n = NodeRepository.getNode(nodeId);
						String elemLoc = "";

						if (isContains(n.getProcessingElements(), elementId)) {
							elemLoc = "PROCESS";
						} else if (isContains(n.getReturnedElements(), elementId)) {
							elemLoc = "RETURNED";
						} else if (isContains(n.getWaitingElements(), elementId)) {
							elemLoc = "QUEUE";
						}

						sb.append(validateString(elemLoc));// 1
						sb.append(DELIMITER);

						sb.append(validateString(elementId));// 2
						sb.append(DELIMITER);

						sb.append(validateString(elem.getElementName()));// 3
						sb.append(DELIMITER);
						sb.append(validateString(nodeId));// 4
						sb.append(DELIMITER);
						sb.append(validateString(elem.getWorkerId()));// 5
						sb.append(DELIMITER);
						sb.append(validateString(elem.getFileLocation()));// 6
						sb.append(DELIMITER);
						sb.append(validateString(elem.getPriority()));// 7
						sb.append(DELIMITER);
						sb.append(validateString(elem.getQueueIndex()));// 8
						sb.append(DELIMITER);
						sb.append(validateString(elem.getStatus()));// 9

						sb.append(DELIMITER);
						sb.append(validateString(elem.getStartProcTime()));// 10
						sb.append(DELIMITER);
						sb.append(validateString(elem.getStartWaitTime()));// 11
						sb.append(DELIMITER);
						sb.append(validateString(elem.getEndProcTime()));// 12

						sb.append(DELIMITER);
						sb.append(validateString(elem.getProcessingDuration()));// 13
						sb.append(DELIMITER);
						sb.append(validateString(elem.getWaitingDuration()));// 14
						// for(String s :
						// BaggageTagEnum.PROD_OUTPUT_UNIT_KEYS.getVal().split("\\|")){
						// ProductionOutputUnit prod =
						// elem.getProductionOutputUnit(s);
						// if(prod != null){
						// sb.append(DELIMITER);
						// sb.append("OutputUnit: " +prod.getOutputCount());
						// sb.append(DELIMITER);
						// sb.append("ErrorUnit: " + prod.getErrorCount());
						// } else {
						// sb.append(",0,0");
						// }
						// }
						//
						// sb.append(DELIMITER);

						for (String s : BaggageTagEnum.EXTRA_DETAIL_KEYS.getVal().split("\\|")) {
							sb.append(DELIMITER);
							sb.append(validateString(s));// key
							Object extraDetail = elem.getExtraDetailValue(s);

							sb.append(DELIMITER);
							String str = String.valueOf(extraDetail);
							sb.append(validateString(str)); // value

						}
						csvWriter.writeNext(new String[] {sb.toString()} );
						elemLogger.info(sb.toString());
					} finally {
						lock.unlock();
					}
				}
			}
			csvWriter.close();
			writer.close();
			
			elemLogger.info(ClassicConstants.FINALIZE_SESSION_MARKER, "");
		} catch (IOException e) {			
			e.printStackTrace();
		}	
		
	}

	/**
	 * Checks if the given list have an element that have the same elementId
	 ***/
	private static boolean isContains(List<Element> elementList, String elementId) {

		for (Element e : elementList) {
			if (e.getElementId().contains(elementId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Write node backups on a given path
	 * 
	 ***/
	private static void writeNodeBackup(String fileNameDate) {
		System.out.println("Writing node backup...");
		List<Node> nodeList = NodeRepository.getAllNodes();
		MDC.put("node_key", dtf.print(System.currentTimeMillis()));

		try {
			// create directory if it does not exist
			String directoryPath = BackupEnum.BACKUP_DIR.getVal() + File.separator + "node";
			File dir = new File(directoryPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			// create writer
			Writer writer = Files.newBufferedWriter(
					Paths.get(directoryPath + File.separator + "node_backup_" + fileNameDate + ".csv"));

			CSVWriter csvWriter = new CSVWriter(writer, ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER,
					ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END);

			for (Node node : nodeList) {

				StringBuilder sb = new StringBuilder();

				sb.append(validateString(node.getNodeId()));// 1

				// node.getWorkers();
				sb.append(DELIMITER);
				sb.append(validateString(node.getNodeName()));// 2
				sb.append(DELIMITER);
				sb.append(validateString(node.getCluster()));// 3
				sb.append(DELIMITER);
				sb.append(validateString(node.getStatus()));// 4
				sb.append(DELIMITER);
				sb.append(validateString(node.getElemType()));// 5
				sb.append(DELIMITER);

				sb.append(validateString(node.getAveWaitDurLimit()));// 6
				sb.append(DELIMITER);
				sb.append(validateString(node.getAveProcDurLimit()));// 7
				sb.append(DELIMITER);

				sb.append(validateString(node.getTotalWaitingElemCount()));// 8
				sb.append(DELIMITER);
				sb.append(validateString(node.getTotalProcessingElemCount()));// 9
				sb.append(DELIMITER);
				sb.append(validateString(node.getTotalCompletedElemCount()));// 10
				sb.append(DELIMITER);
				sb.append(validateString(node.getTotalExceptionCount()));// 11
				sb.append(DELIMITER);

				sb.append(validateString(node.getMinWaitDur()));// 12
				sb.append(DELIMITER);
				sb.append(validateString(node.getAveWaitDur()));// 13
				sb.append(DELIMITER);
				sb.append(validateString(node.getMaxWaitDur()));// 14
				sb.append(DELIMITER);
				sb.append(validateString(node.getMinProcDur()));// 15
				sb.append(DELIMITER);
				sb.append(validateString(node.getAveProcDur()));// 16
				sb.append(DELIMITER);
				sb.append(validateString(node.getMaxProcDur()));// 17

				for (String s : BaggageTagEnum.PROD_OUTPUT_UNIT_KEYS.getVal().split("\\|")) {
					ProductionOutputUnit prod = node.getProductionOutputUnit(s);
					if (prod != null) {
						sb.append(DELIMITER);
						sb.append(validateString(s));// key
						sb.append(DELIMITER);
						sb.append(validateString(prod.getMeasurementUnit()));
						sb.append(DELIMITER);
						sb.append(validateString(prod.getCost()));
						sb.append(DELIMITER);
						sb.append(validateString(prod.getOutputCount()));
						sb.append(DELIMITER);
						sb.append(validateString(prod.getErrorCount()));
					} else {
						sb.append(",,,0,0,0");// key,measure,cost,output,error
					}
				}

				// List<ServiceObj> services = node.getServices();
				// if(services == null || services.isEmpty()){
				// sb.append(",,,");
				// }else{
				// for (ServiceObj servicesToRun : services) {
				// sb.append(DELIMITER);
				// sb.append(validateString(servicesToRun.getServicePath()));
				// sb.append(DELIMITER);
				// sb.append(validateString(servicesToRun.getServiceType().getVal()));
				// sb.append(DELIMITER);
				// sb.append(validateString(servicesToRun.getRequestType().getVal()));
				//
				// }
				// }

				ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun = node.getServicesToRun();
				if (servicesToRun == null || servicesToRun.isEmpty()) {
					sb.append(",,,");
				} else {
					servicesToRun.forEach((a, b) -> {
						String trigger = a.getVal();
						b.forEach(serviceToRun -> {
							sb.append(DELIMITER);
							sb.append(validateString(trigger));
							sb.append(DELIMITER);
							sb.append(validateString(serviceToRun.getServicePath()));
							sb.append(DELIMITER);
							sb.append(validateString(serviceToRun.getServiceType().getVal()));
							sb.append(DELIMITER);
							sb.append(validateString(serviceToRun.getRequestType().getVal()));
							sb.append(DELIMITER);
							sb.append(validateString(serviceToRun.getParameter()));
						});
					});
				}

				csvWriter.writeNext(new String[] { sb.toString() });
				nodeLogger.info(sb.toString());

			}
			csvWriter.close();
			writer.close();
			nodeLogger.info(ClassicConstants.FINALIZE_SESSION_MARKER, "");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// private static void writeWorkerBackup(){
	// List<Worker> workers = WorkerRepository.getAllWorkers();
	//
	// MDC.put("worker_key", dtf.print(System.currentTimeMillis()));
	// System.out.println("workers size: " + workers.size());
	// for(Worker w : workers){
	// StringBuilder sb = new StringBuilder();
	// sb.append("WORKER");
	// sb.append(DELIMITER);
	// sb.append(w.getWorkerId());
	// sb.append(DELIMITER);
	// sb.append(w.getWorkerName());
	//
	// //sb.append(DELIMITER);
	//
	// workerLogger.info(sb.toString());
	//
	// }
	//
	// workerLogger.info(ClassicConstants.FINALIZE_SESSION_MARKER, "");
	//
	// }

	/***
	 * Stops the thread
	 * 
	 **/
	public static void stopThread() {
		if (bufferThread != null && bufferThread.isAlive()) {
			threadIsRunning = false;
			executor.shutdown();
			try {
				executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("BPO 5 App terminating. Flushing contents...");
		writeBackup();
	}

	/***
	 * Starts the thread
	 ***/
	public static void startThread() {
		int bufferTimeInMinutes = Integer.parseInt(BackupEnum.BACKUP_SCHED_DUR_WRITE_SLEEP_SEC.getVal());
		interval = 1L * (bufferTimeInMinutes * 1000);
		dtf = DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss.SSS")
				.withZone(DateTimeZone.forID(BackupEnum.BACKUP_TIMEZONE.getVal()));
		bufferThread = new Thread(new BackupThread());
		executor.execute(bufferThread);
	}

	/****
	 * Validations for string and integers etc.
	 ****/

	private static String validateString(String str) {
		return Strings.isNullOrEmpty(str) ? "" : StringEscapeUtils.escapeCsv(str);
	}

	private static String validateString(BigDecimal num) {
		String str = String.valueOf(num);
		return validateString(str);
	}

	private static String validateString(int num) {
		String str = String.valueOf(num);
		return validateString(str);
	}

	private static String validateString(long num) {
		String str = String.valueOf(num);
		return validateString(str);
	}

	private static String validateString(double num) {
		String str = String.valueOf(num);
		return validateString(str);

	}
}
