package com.svi.bpo.backup;

import org.ini4j.Profile.Section;

/**
 * author: jmhinolan
 * start date: 1/30/2017
 * ***/

public enum BackupEnum {

	BACKUP_DIR("BACKUP_DIR"),
	BACKUP_SCHED_DUR_WRITE_SLEEP_SEC("BACKUP_SCHED_DUR_WRITE_SLEEP_SEC"),
	BACKUP_TIMEZONE("BACKUP_TIMEZONE"),
	BACKUP_NODES_MAX_FILECOUNT("BACKUP_NODES_MAX_FILECOUNT"),	
	BACKUP_ELEMS_MAX_FILECOUNT("BACKUP_ELEMS_MAX_FILECOUNT"),
	BACKUP_ENABLED("BACKUP_ENABLED");
	
	private String val = "";
	
	BackupEnum(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
	
	public static void setContext(Section workers){
		for(BackupEnum each : values()){
			each.setVal(workers.get(each.getVal()));
		}
	}
	
}
