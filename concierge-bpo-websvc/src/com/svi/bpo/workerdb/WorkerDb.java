package com.svi.bpo.workerdb;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="workers")
public class WorkerDb{
	private List<WorkerEntity> workers;
	
	public WorkerDb(){}
	
	public WorkerDb(List<WorkerEntity> workers) {
		this.workers = workers;
	}

	@XmlElement(name="worker")
	public List<WorkerEntity> getWorkers() {
		return workers;
	}

	public void setWorkers(List<WorkerEntity> workers) {
		this.workers = workers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WorkerDb [");
		if (workers != null) {
			for(WorkerEntity worker : workers){
				builder.append(worker+"\r\n");
			}
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
