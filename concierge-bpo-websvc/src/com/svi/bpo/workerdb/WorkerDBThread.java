package com.svi.bpo.workerdb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.Worker;
import com.svi.bpo.core.WorkerRepository;

public class WorkerDBThread implements Runnable {	
	public enum WorkerDBTransaction{
		READ, WRITE
	}
	
	WorkerDBTransaction transaction = null;
	
	public WorkerDBThread(WorkerDBTransaction transaction){
		this.transaction = transaction;
	}
	

	@Override
	public void run() {
		if(transaction == WorkerDBTransaction.READ){
			readXml();
		} else if(transaction == WorkerDBTransaction.WRITE){
			writeXml();
		}
	}
	
	private void readXml() {
		JAXBContext context;
		FileInputStream stream = null;
		try {
			context = JAXBContext.newInstance(WorkerDb.class);
	        Unmarshaller um = context.createUnmarshaller();
	        File file = new File(WorkerDBEnum.WORKERS_XML.getVal());
	        if(file.exists()){
		        stream = new FileInputStream(WorkerDBEnum.WORKERS_XML.getVal());
		        for(WorkerEntity worker : ((WorkerDb)um.unmarshal(stream)).getWorkers()){
		        	Worker w = new Worker(worker.getWorkerId(), worker.getWorkerName());
		        	WorkerRepository.addWorker(w.getWorkerId(), w);
		        	for(NodeEntity node : worker.getNodes()){
		        		Node n = NodeRepository.getNode(node.getNodeId());
		        		if(n != null){
		        			n.assignWorkerToNode(worker.getWorkerId(), node.getQueueIndices());
		        		}
		        	}
		        }
	        } else {
		        file.getParentFile().mkdirs();
	        }
		} catch (JAXBException | IOException e) {
			System.err.println(e.getMessage());
		} finally {
			if(stream != null){
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void writeXml(){
		List<WorkerEntity> workers = new ArrayList<>();
		JAXBContext context;
		FileOutputStream stream = null;
		
		for(Worker w : WorkerRepository.getAllWorkers()){
			WorkerEntity worker = new WorkerEntity(w.getWorkerId(), w.getWorkerName());
			workers.add(worker);
			for(Node n : NodeRepository.getAllNodes()){
				if(n.getWorkers().containsKey(w.getWorkerId())){
					NodeEntity node = new NodeEntity(n.getNodeId(), n.getWorkers().get(w.getWorkerId()));
					worker.getNodes().add(node);
				}
			}
		}
		
		try {
			context = JAXBContext.newInstance(WorkerDb.class);
	        Marshaller m = context.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        File file = new File(WorkerDBEnum.WORKERS_XML.getVal());
	        file.getParentFile().mkdirs();
	        stream = new FileOutputStream(file);
	        m.marshal(new WorkerDb(workers), stream);
		} catch (JAXBException | IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			if(stream != null){
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
