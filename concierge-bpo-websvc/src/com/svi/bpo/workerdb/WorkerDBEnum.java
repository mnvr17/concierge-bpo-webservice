package com.svi.bpo.workerdb;

import org.ini4j.Profile.Section;

public enum WorkerDBEnum {
	WORKERS_XML("WORKERS_XML");
	
	private String val = "";
	
	WorkerDBEnum(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
	
	public static void setContext(Section workers){
		for(WorkerDBEnum each : values()){
			each.setVal(workers.get(each.getVal()));
		}
	}
}
