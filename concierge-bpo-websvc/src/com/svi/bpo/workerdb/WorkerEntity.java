package com.svi.bpo.workerdb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class WorkerEntity {
	private String workerId;
	private String workerName;
	private List<NodeEntity> nodes = new ArrayList<>();
	
	public WorkerEntity(){}

	public WorkerEntity(String workerId, String workerName) {
		this.workerId = workerId;
		this.workerName = workerName;
	}

	public String getWorkerId() {
		return workerId;
	}

	public void setWorkerId(String workerId) {
		this.workerId = workerId;
	}

	public String getWorkerName() {
		return workerName;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}
	
	@XmlElementWrapper(name="nodes")
	@XmlElement(name="node")
	public List<NodeEntity> getNodes() {
		return nodes;
	}

	public void setWorkers(List<NodeEntity> nodes) {
		this.nodes = nodes;
	}

	@Override
	public String toString() {
		return "WorkerEntity [workerId=" + workerId + ", workerName=" + workerName + ", nodes=" + nodes + "]";
	}
	
	
}
