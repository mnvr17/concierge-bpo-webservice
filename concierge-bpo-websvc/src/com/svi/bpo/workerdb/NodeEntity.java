package com.svi.bpo.workerdb;

import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class NodeEntity {
	private String nodeId;
	private TreeSet<Integer> queueIndices = new TreeSet<>();
	
	public NodeEntity(){}

	public NodeEntity(String nodeId, TreeSet<Integer> queueIndices) {
		this.nodeId = nodeId;
		this.queueIndices = queueIndices;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	@XmlElementWrapper(name="queueIndices")
	@XmlElement(name="queueIndex")
	public TreeSet<Integer> getQueueIndices() {
		return queueIndices;
	}

	public void setQueueIndices(TreeSet<Integer> queueIndices) {
		this.queueIndices = queueIndices;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NodeEntity [");
		if (nodeId != null) {
			builder.append("nodeId=");
			builder.append(nodeId);
			builder.append(", ");
		}
		if (queueIndices != null) {
			builder.append("queueIndices=");
			builder.append(queueIndices);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
}
