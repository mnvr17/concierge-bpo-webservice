package com.svi.bpo.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.svi.object.ServiceObj;

public class ListServiceTypeAdapter extends TypeAdapter<List<ServiceObj>>{

	@Override
	public List<ServiceObj> read(JsonReader in) throws IOException {

		List<ServiceObj> value = null;

		JsonParser jsonParser = new JsonParser();
		JsonElement je = jsonParser.parse(in);

		if (je instanceof JsonObject) {
			JsonObject jsonObject = (JsonObject) je;
			value = new CopyOnWriteArrayList<ServiceObj>();
//			value.addAll((List<>jsonObject);
		}

		return value;
	}

	@Override
	public void write(JsonWriter out, List<ServiceObj> value)
			throws IOException {
		if (value != null) {
			out.value(value.toString());
		}
	}
}
