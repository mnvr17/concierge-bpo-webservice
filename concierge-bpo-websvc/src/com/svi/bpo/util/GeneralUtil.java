package com.svi.bpo.util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVReader;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.response.ElementOperationResponse;
import com.svi.bpo.response.NodeOperationResponse;
import com.svi.bpo.response.WorkerOperationResponse;

public class GeneralUtil {

	/**
	 * Parser of json String into Json object
	 * 
	 * @param json
	 * @return
	 */
	public static JsonObject parseToJson(String json) {
		JsonParser parser = new JsonParser();
		return parser.parse(json).getAsJsonObject();
	}

	/**
	 * Checks if the cell is empty, null or trailing spaces.
	 * 
	 * @param cell
	 *            The cell to be checked.
	 * @return TRUE if the cell is empty, null or trailing spaces. Otherwise,
	 *         returns FALSE.
	 */
	public static boolean isNullOrEmpty(Cell cell) {
		if (cell == null) {
			return true;
		}
		if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return true;
		}

		cell.setCellType(Cell.CELL_TYPE_STRING);
		if (cell.getStringCellValue().trim().length() == 0) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the cell value as a String.
	 * 
	 * @param cell
	 *            The cell where the value is to be retrieved.
	 * @return The value of the cell.
	 */
	public static String getCellValue(Cell cell) {
		cell.setCellType(Cell.CELL_TYPE_STRING);
		return cell.getStringCellValue().trim();
	}

	/**
	 * Checker if parameters are empty string or null
	 * 
	 * @param params
	 *            The parameters
	 * @return Returns true if one parameter is null or empty
	 */
	public static boolean isParamsNullOrEmpty(String... params) {
		for (String param : params) {
			if (Strings.isNullOrEmpty(param))
				return true;
		}
		return false;
	}

	/**
	 * Returns appropriate response based on response status, whether the
	 * response is successful or not.
	 * 
	 * @param response
	 * @return
	 */
	public static Response buildResponse(NodeOperationResponse response) {
		if (response.isSuccessful()) {
			return Response.ok().entity(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString()).build();
		}
	}

	/**
	 * Returns appropriate response based on response status, whether the
	 * response is successful or not.
	 * 
	 * @param response
	 * @return
	 */
	public static Response buildResponse(ElementOperationResponse response) {
		if (response.isSuccessful()) {
			return Response.ok().entity(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString()).build();
		}
	}

	/**
	 * Returns appropriate response based on response status, whether the
	 * response is successful or not.
	 * 
	 * @param response
	 * @return
	 */
	public static Response buildResponse(WorkerOperationResponse response) {
		if (response.isSuccessful()) {
			return Response.ok(response.toString()).build();
		} else {
			return Response.status(response.getErrorCode()).entity(response.toString()).build();
		}
	}

	public static boolean isNullOrEmpty(Row row) {
		boolean result = true;

		for (int i = 0; i < row.getLastCellNum(); i++) {
			if (!isNullOrEmpty(row.getCell(i))) {
				result = false;
				break;
			}
		}

		return result;
	}

	public static ByteArrayOutputStream getExceptionFile(Workbook workbook,
			Map<String, Integer> erroneousElements, String extension, Map<String, String> errorMessages,
			int errorMessageCol) {
		ByteArrayOutputStream out = null;

		System.out.println("erroneousElements: " + erroneousElements);
		System.out.println("errorMessages: " + errorMessages);

		try {
			AtomicInteger startRow = new AtomicInteger(1);

			// create a workbook based on excel file version
			Workbook workbook2 = null;
			if (extension.equalsIgnoreCase(BPOConstants.XLS)) {
				workbook2 = new HSSFWorkbook();
			} else {
				workbook2 = new XSSFWorkbook();
			}

			// getting the first work sheet of the excel file
			Sheet sheet = workbook.getSheetAt(0);
			Sheet errorSheet = workbook2.createSheet();

			CellStyle style = workbook2.createCellStyle();
			style.setFillForegroundColor(IndexedColors.RED.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

			// Copy Headers;

			try {
				Row newHeaderRow = errorSheet.createRow(0);
				Row headerRow = sheet.getRow(0);
				IntStream.rangeClosed(0, headerRow.getLastCellNum()).forEach(n -> {
					if (!GeneralUtil.isNullOrEmpty(headerRow.getCell(n))) {
						Cell cell = newHeaderRow.createCell(n);
						cell.setCellValue(GeneralUtil.getCellValue(headerRow.getCell(n)));
					}
				});
			} catch (Exception e2) {
				// TODO: handle exception
			}
			// Copy error rows
			erroneousElements.forEach((k, v) -> {
				try {
					Row newRow = errorSheet.createRow(startRow.get());
					Row row = sheet.getRow(Integer.valueOf(k) - 1);
					if (!GeneralUtil.isNullOrEmpty(row)) {
						IntStream.rangeClosed(0, row.getLastCellNum()).forEach(n -> {
							Cell cell = newRow.createCell(n);
							if (!GeneralUtil.isNullOrEmpty(row.getCell(n))) {
								cell.setCellValue(GeneralUtil.getCellValue(row.getCell(n)));
							}
						});

						if (errorMessages.containsKey("" + k + v)) {
							Cell cellRemark = newRow.createCell(errorMessageCol);
							cellRemark.setCellValue(errorMessages.get("" + k + v));
						}

						Cell cell = newRow.getCell(v - 1);
						cell.setCellStyle(style);
						startRow.incrementAndGet();
					}
				} catch (Exception e) {

				}
			});
			IntStream.rangeClosed(0, errorSheet.getRow(0).getLastCellNum())
					.forEach(n -> errorSheet.autoSizeColumn(n));
			out = new ByteArrayOutputStream();
			workbook2.write(out);
			workbook2.close();
		} catch (Exception e) {
			// System.err.println(e.getMessage());
			e.printStackTrace();
		}

		return out;
	}

	public static Reader getReader(String filePath)
			throws UnsupportedEncodingException, FileNotFoundException {
		InputStream inputStream = new FileInputStream(filePath);
		Reader inputStreamReader = new InputStreamReader(inputStream);
		return inputStreamReader;
	}

	public static List<String[]> readCsvFile(String filePath) {
		List<String[]> list = new ArrayList<>();

		try {
			Reader fileReader = GeneralUtil.getReader(filePath);
			CSVReader reader = new CSVReader(fileReader);
			String[] line;
			while ((line = reader.readNext()) != null) {
				if(CollectionUtils.isNotEmpty(Arrays.asList(reader))) {
					list.add(line);
				}
			}
			fileReader.close();
			reader.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//
		return list;
	}

}
