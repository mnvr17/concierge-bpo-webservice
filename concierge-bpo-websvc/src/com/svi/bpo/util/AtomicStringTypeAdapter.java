package com.svi.bpo.util;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AtomicStringTypeAdapter extends TypeAdapter<AtomicReference<String>> {

    @Override
    public AtomicReference<String> read(JsonReader in) throws IOException {

    	AtomicReference<String> value = null;

        JsonParser jsonParser = new JsonParser();
        JsonElement je = jsonParser.parse(in);

        if (je instanceof JsonPrimitive) {
            value = new AtomicReference<String>();
            value.set(((JsonPrimitive) je).getAsString());
        } else if (je instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) je;
            value = new AtomicReference<String>();
            value.set(jsonObject.get("value").getAsString());
        }

        return value;
    }

    @Override
    public void write(JsonWriter out, AtomicReference<String> value) throws IOException {
        if (value != null) {
			out.value(value.get());
        }
    }
}
