package com.svi.bpo.util;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AtomicIntegerTypeAdapter extends TypeAdapter<AtomicInteger> {

    @Override
    public AtomicInteger read(JsonReader in) throws IOException {

    	AtomicInteger value = null;

        JsonParser jsonParser = new JsonParser();
        JsonElement je = jsonParser.parse(in);

        if (je instanceof JsonPrimitive) {
            value = new AtomicInteger();
            value.set(Integer.parseInt(((JsonPrimitive) je).getAsString()));
        } else if (je instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) je;
            value = new AtomicInteger();
            value.set(Integer.parseInt(jsonObject.get("value").getAsString()));
        }

        return value;
    }

    @Override
    public void write(JsonWriter out, AtomicInteger value) throws IOException {
        if (value != null) {
			out.value(value.get());
        }
    }
}