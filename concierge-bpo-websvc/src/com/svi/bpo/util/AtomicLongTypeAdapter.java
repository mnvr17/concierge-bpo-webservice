package com.svi.bpo.util;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AtomicLongTypeAdapter extends TypeAdapter<AtomicLong> {

    @Override
    public AtomicLong read(JsonReader in) throws IOException {

    	AtomicLong value = null;

        JsonParser jsonParser = new JsonParser();
        JsonElement je = jsonParser.parse(in);

        if (je instanceof JsonPrimitive) {
            value = new AtomicLong();
            value.set(Long.parseLong(((JsonPrimitive) je).getAsString()));
        } else if (je instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) je;
            value = new AtomicLong();
            value.set(Long.parseLong(jsonObject.get("value").getAsString()));
        }

        return value;
    }

    @Override
    public void write(JsonWriter out, AtomicLong value) throws IOException {
        if (value != null) {
			out.value(value.get());
        }
    }
}
