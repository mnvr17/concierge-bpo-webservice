package com.svi.bpo.util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.svi.bpo.baggagetag.BaggageTag;
import com.svi.bpo.baggagetag.BaggageTagBuffer;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.ElementRepository;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.bpo.core.Worker;
import com.svi.bpo.core.WorkerRepository;
import com.svi.bpo.response.ElementOperation;
import com.svi.bpo.response.ElementOperationResponse;
import com.svi.bpo.response.NodeOperationResponse;
import com.svi.bpo.response.OperationResponseCode;

@SuppressWarnings("serial")
public class ElementUtil {
	private static final String DELETED = "DELETED";
	private static final String WAITING = "WAITING";
	private static final String PROCESSING = "PROCESSING";	
	private static final String TRANSFERRED = "TRANSFERRED";
	private static final String ENDPOINT = "endpoint";
	
	
	private static Gson gson = new GsonBuilder()
			.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
			}.getType(), new AtomicStringTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicReference<BigDecimal>>() {
			}.getType(), new AtomicBigDecimalTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicLong>() {
			}.getType(), new AtomicLongTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicInteger>() {
			}.getType(), new AtomicIntegerTypeAdapter())
			.registerTypeAdapter(new TypeToken<AtomicDouble>() {
			}.getType(), new AtomicDoubleTypeAdapter())
			.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
			.create();

	// no longer used
	public static ElementOperationResponse insertElement(String nodeId,
			String json) {
		JsonObject values = GeneralUtil.parseToJson(json);
		if (!values.has(BPOConstants.ELEMENT_ID) || !values.has(BPOConstants.ELEMENT_NAME)
				|| !values.has(BPOConstants.QUEUE_INDEX) || !values.has(BPOConstants.PRIORITY)) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		String elementId = values.get(BPOConstants.ELEMENT_ID).getAsString();
		String elementName = values.get(BPOConstants.ELEMENT_NAME).getAsString();
		int queueIndex = -1;
		int priority = -1;

		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
			priority = values.get(BPOConstants.PRIORITY).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		if (GeneralUtil.isParamsNullOrEmpty(elementId, elementName, nodeId)
				|| (queueIndex < 0 || queueIndex > 99)
				|| (priority < 0 || priority > 9)) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		String fileLocation = "";
		if (values.has(BPOConstants.FILE_LOC)) {
			fileLocation = values.get(BPOConstants.FILE_LOC).getAsString();
		}

		Map<String, String> extraDetails = new HashMap<>();
		if (values.has(BPOConstants.EXTRA_DETAILS)) {
			extraDetails = new Gson().fromJson(values.get(BPOConstants.EXTRA_DETAILS),
					new TypeToken<Map<String, String>>() {
					}.getType());
		}

		Node node = NodeRepository.getNode(nodeId);
		if (node != null) {
			if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
				return new ElementOperationResponse(false,
						ElementOperation.CREATE_ELEM, json,
						OperationResponseCode.NODE_IS_CLOSED.getCode());
			}
			Element element = new Element(elementId, elementName, nodeId,
					queueIndex, priority, fileLocation, extraDetails);
			if (node.enqueueElement(element, true)) {
				return new ElementOperationResponse(true,
						ElementOperation.CREATE_ELEM);

			} else {
				return new ElementOperationResponse(false,
						ElementOperation.CREATE_ELEM, json,
						OperationResponseCode.ELEMENT_ALREADY_EXISTS.getCode());

			}
		} else {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}

	}

	public static ElementOperationResponse insertElement(String nodeId, String elementId,
			String json) {
		JsonObject values = GeneralUtil.parseToJson(json);

		if (!values.has(BPOConstants.ELEMENT_NAME)|| 
				!values.has(BPOConstants.QUEUE_INDEX) || !values.has(BPOConstants.PRIORITY)) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		//String elementId = values.get(ELEMENT_ID).getAsString();
		String elementName = values.get(BPOConstants.ELEMENT_NAME).getAsString().trim();
		int queueIndex = -1;
		int priority = -1;

		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
			priority = values.get(BPOConstants.PRIORITY).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		if (GeneralUtil.isParamsNullOrEmpty(elementId.trim(), elementName, nodeId.trim())				
				|| (queueIndex < 0 || queueIndex > 99)
				|| (priority < 0 || priority > 9)) {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		String fileLocation = "";
		if (values.has(BPOConstants.FILE_LOC)) {
			fileLocation = values.get(BPOConstants.FILE_LOC).getAsString();
		}

		Map<String, String> extraDetails = new HashMap<>();
		if (values.has(BPOConstants.EXTRA_DETAILS)) {
			System.out.println(" ito ang jsonnnnnnn: "+values.get(BPOConstants.EXTRA_DETAILS).toString());
			extraDetails = new Gson().fromJson(values.get(BPOConstants.EXTRA_DETAILS),
					new TypeToken<Map<String, String>>() {
					}.getType());
		}

		Node node = NodeRepository.getNode(nodeId);
		if (node != null) {
			if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
				return new ElementOperationResponse(false,
						ElementOperation.CREATE_ELEM, json,
						OperationResponseCode.NODE_IS_CLOSED.getCode());
			}
			Element element = new Element(elementId, elementName, nodeId,
					queueIndex, priority, fileLocation, extraDetails);
			if (node.enqueueElement(element, true)) {
				return new ElementOperationResponse(true,
						ElementOperation.CREATE_ELEM,json,element);

			} else {
				return new ElementOperationResponse(false,
						ElementOperation.CREATE_ELEM, json,
						OperationResponseCode.ELEMENT_ALREADY_EXISTS.getCode());

			}
		} else {
			return new ElementOperationResponse(false,
					ElementOperation.CREATE_ELEM, json,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}

	}
	
	// no longer used
	public static ElementOperationResponse changePriority(String nodeId,
			String elementId, String json) {
		if (!Strings.isNullOrEmpty(nodeId) || !Strings.isNullOrEmpty(elementId)) {
			Node node = NodeRepository.getNode(nodeId);
			if (node != null) {
				List<String> elemList = new ArrayList<String>();
				elemList.add(elementId);
				Element element = null;
				List<Element> elementList = node.getWaitingElements(elemList);
				for (Element elem : elementList) {
					if (elem.getElementId().equalsIgnoreCase(elementId))
						element = elem;
				}
				JsonParser parser = new JsonParser();
				JsonObject values = parser.parse(json).getAsJsonObject();
				Integer priority = null;
				Integer queueIndex = null;
				boolean hasPriority = false;
				boolean hasQueueIndex = false;
				try {
					if (values.has(BPOConstants.PRIORITY)) {
						priority = values.get(BPOConstants.PRIORITY).getAsInt();
						if (priority >= 0 && priority <= 9) {
							hasPriority = true;
						} else {
							return new ElementOperationResponse(false,
									ElementOperation.CHANGE_PRIORITY, json,
									OperationResponseCode.INVALID_PARAMS.getCode());
						}
					}
					if (values.has(BPOConstants.QUEUE_INDEX)) {
						queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
						if (queueIndex >= 0 && queueIndex <= 99) {
							hasQueueIndex = true;
						} else {
							return new ElementOperationResponse(false,
									ElementOperation.CHANGE_PRIORITY, json,
									OperationResponseCode.INVALID_PARAMS.getCode());
						}
					}
				} catch (ClassCastException | IllegalStateException e) {
					return new ElementOperationResponse(false,
							ElementOperation.CHANGE_PRIORITY, json,
							OperationResponseCode.INVALID_PARAMS.getCode());
				}
				if (!hasPriority && !hasQueueIndex) {
					return new ElementOperationResponse(false,
							ElementOperation.CHANGE_PRIORITY, json,
							OperationResponseCode.INVALID_PARAMS.getCode());

				} else if (element == null) {
					return new ElementOperationResponse(false,
							ElementOperation.CHANGE_PRIORITY, json,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST
									.getCode());
				} else {
					if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
						return new ElementOperationResponse(false,
								ElementOperation.CREATE_ELEM, json,
								OperationResponseCode.NODE_IS_CLOSED.getCode());
					}

					if (node.removeFromWaitingQueue(elementId,
							element.getQueueIndex()) != null) {
						// node.decTotalWaitingElemCount();
						if (hasPriority) {
							element.setPriority(priority);
						}
						if (hasQueueIndex) {
							element.setQueueIndex(queueIndex);
						}
						if (node.enqueueElement(element, false, false)) {
							ElementOperationResponse response = new ElementOperationResponse(
									true, ElementOperation.CHANGE_PRIORITY);
							return response;
						}
					}

					return new ElementOperationResponse(false,
							ElementOperation.CHANGE_PRIORITY, json,
							OperationResponseCode.OPERATION_FAILED.getCode());
				}
			} else {
				ElementOperationResponse response = new ElementOperationResponse(
						false, ElementOperation.CHANGE_PRIORITY, json,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}

		} else {
			ElementOperationResponse response = new ElementOperationResponse(
					false, ElementOperation.CHANGE_PRIORITY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return response;
		}
	}

	// no longer used
	public static ElementOperationResponse updateElement(String nodeId,
			String elementId, String json) {
		if (!Strings.isNullOrEmpty(nodeId) || !Strings.isNullOrEmpty(elementId)) {
			Node node = NodeRepository.getNode(nodeId);
			if (node != null) {
				List<String> elemList = new ArrayList<String>();
				elemList.add(elementId);
				Element element = null;
				List<Element> elementList = node.getElements(elemList);
				for (Element elem : elementList) {
					if (elem.getElementId().equalsIgnoreCase(elementId))
						element = elem;
				}
				
				if(element == null){
					ElementOperationResponse response = new ElementOperationResponse(
							false, ElementOperation.UPDATE_ELEM, OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
					return response;
				}

				JsonParser parser = new JsonParser();
				JsonObject values = parser.parse(json).getAsJsonObject();
				String elementName =  null;
				String fileLocation = null;
				Map<String, String> extraDetails = null;
				try {
					if(values.has(BPOConstants.ELEMENT_NAME)){
						elementName = values.get(BPOConstants.ELEMENT_NAME).getAsString();
					}
					
					if(values.has(BPOConstants.FILE_LOC)){
						fileLocation = values.get(BPOConstants.FILE_LOC).getAsString();
					}					
					
					if(values.has(BPOConstants.EXTRA_DETAILS)){
						JsonElement temp = values.get(BPOConstants.EXTRA_DETAILS);
						Type type = new TypeToken<Map<String, String>>(){}.getType();
						extraDetails = gson.fromJson(temp, type);
					}
				} catch (ClassCastException|IllegalStateException e){
					ElementOperationResponse response = new ElementOperationResponse(
							false, ElementOperation.UPDATE_ELEM, OperationResponseCode.INVALID_PARAMS.getCode());
					return response;
				}
				
				if(!Strings.isNullOrEmpty(elementName)){
					element.setElementName(elementName);
				}

				if(!Strings.isNullOrEmpty(fileLocation)){
					element.setFileLocation(fileLocation);
				}
				
				if(extraDetails != null){
					for(Entry<String, String> each : extraDetails.entrySet()){
						if(element.extraDetailExists(each.getKey())){
							element.updateExtraDetail(each.getKey(), each.getValue());
						} else {
							element.addExtraDetail(each.getKey(), each.getValue());
						}
					}
				}
				
				ElementOperationResponse response = new ElementOperationResponse(
						true, ElementOperation.UPDATE_ELEM);
				
				return response;
			} else {
				ElementOperationResponse response = new ElementOperationResponse(
						false, ElementOperation.UPDATE_ELEM, nodeId,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
		} else {
			ElementOperationResponse response = new ElementOperationResponse(
					false, ElementOperation.UPDATE_ELEM, nodeId,
					OperationResponseCode.INVALID_PARAMS.getCode());
			return response;
		}
	}

	
	public static ElementOperationResponse updateElement2(String nodeId, String elementId, String json) {
		// validate inputs
		if(GeneralUtil.isParamsNullOrEmpty(nodeId, elementId, json)){
			return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
		}		
		
		// check existence of node
		Node node = NodeRepository.getNode(nodeId);		
		if(node == null){
			return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
		if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)){
			return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, json, OperationResponseCode.NODE_IS_CLOSED.getCode());
		}
		
		// getting the element
		Element element = node.getElements().stream().filter(elem -> elem.getElementId().equalsIgnoreCase(elementId)).findFirst().get();
		if(element == null){
			return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
		}
		
		// parsing json string
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(json).getAsJsonObject();
		
		// getting and validating inputs
		String elementName = null;
		String fileLocation = null;
		Integer priority = null;
		Integer queueIndex = null;	
		Map<String, String> extraDetails = null;
		
		if (jsonObject.has(BPOConstants.PRIORITY)) {			
			try {
				priority = jsonObject.get(BPOConstants.PRIORITY).getAsInt();
			} catch (Exception e) {
				return new ElementOperationResponse(false, ElementOperation.CHANGE_PRIORITY, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
			
			if (priority < 0 || priority > 9) {
				return new ElementOperationResponse(false, ElementOperation.CHANGE_PRIORITY, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			} 
		}
		if (jsonObject.has(BPOConstants.QUEUE_INDEX)) {			
			try {
				queueIndex = jsonObject.get(BPOConstants.QUEUE_INDEX).getAsInt();
			} catch (Exception e) {
				return new ElementOperationResponse(false, ElementOperation.CHANGE_QUEUE_INDEX, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
			
			if (queueIndex < 0 || queueIndex > 99) {
				return new ElementOperationResponse(false, ElementOperation.CHANGE_QUEUE_INDEX, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			} 
		}
		if (jsonObject.has(BPOConstants.ELEMENT_NAME)) {
			elementName = jsonObject.get(BPOConstants.ELEMENT_NAME).getAsString();
			if(GeneralUtil.isParamsNullOrEmpty(elementName.trim())){
				return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
		}

		if (jsonObject.has(BPOConstants.FILE_LOC)) {
			fileLocation = jsonObject.get(BPOConstants.FILE_LOC).getAsString();
			if(GeneralUtil.isParamsNullOrEmpty(fileLocation.trim())){
				return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
		}	
		if (jsonObject.has(BPOConstants.EXTRA_DETAILS)) {
			JsonElement temp = jsonObject.get(BPOConstants.EXTRA_DETAILS);
			Type type = new TypeToken<Map<String, String>>() {
			}.getType();
			
			try {
				extraDetails = gson.fromJson(temp, type);
			} catch (JsonSyntaxException e) {
				return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
		}
			
		// setting element properties
		if (priority != null) {
			element.setPriority(priority);
		}
		
		if (elementName != null) {
			element.setElementName(elementName);
		}

		if (fileLocation != null) {
			element.setFileLocation(fileLocation);
		}
		if (extraDetails != null) {
			for (Entry<String, String> each : extraDetails.entrySet()) {
				if (element.extraDetailExists(each.getKey())) {
					element.updateExtraDetail(each.getKey(), each.getValue());
				} else {
					element.addExtraDetail(each.getKey(), each.getValue());
				}
			}
		}
		if(queueIndex != null){					
			if (node.removeFromWaitingQueue(elementId, element.getQueueIndex()) != null) {	
				element.setQueueIndex(queueIndex);	
				if (!node.enqueueElement(element, false, false)) {
					
					return new ElementOperationResponse(false, ElementOperation.UPDATE_ELEM, OperationResponseCode.OPERATION_FAILED.getCode());
				}
			}
		}		
		return new ElementOperationResponse(true, ElementOperation.UPDATE_ELEM);		
	}
	
	
	
	
	
	
	
	private static boolean transferElementAnotherIntance(String nodeId,
			Element element, String endpoint) throws IOException {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(BPOConstants.ELEMENT_ID, element.getElementId());
		jsonObject.addProperty(BPOConstants.ELEMENT_NAME, element.getElementName());
		jsonObject.addProperty(BPOConstants.QUEUE_INDEX, element.getQueueIndex());
		jsonObject.addProperty(BPOConstants.PRIORITY, element.getPriority());
		jsonObject.addProperty(BPOConstants.FILE_LOC, element.getFileLocation());
		jsonObject.add(BPOConstants.EXTRA_DETAILS, gson.toJsonTree(element.getExtraDetails()));
		
		ElementOperationResponse response = insertElement(nodeId, element.getElementId(), gson.toJson(jsonObject));
		if (response.isSuccessful()) {
			return true;
		} else {
			return false;			
		}
	}
	// no longer used
	public static ElementOperationResponse transferElement(String nodeId,
			String elementId, String json) {
		
		JsonObject values = GeneralUtil.parseToJson(json);
		
		if(!values.has(BPOConstants.NEXT_NODE_ID)
			|| !values.has(BPOConstants.QUEUE_INDEX)){
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		
		String nextNodeId = values.get(BPOConstants.NEXT_NODE_ID).getAsString();

		if (values.has(ENDPOINT)) {
			if (!NodeRepository.contains(nodeId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			}

			Node currentNode = NodeRepository.getNode(nodeId);

			if (!currentNode.hasElement(elementId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}
			List<String> elemList = new ArrayList<String>();
			elemList.add(elementId);
			Element element = currentNode.getElements(elemList).get(0);

			String endpoint = values.get(ENDPOINT).getAsString();

			boolean isSuccess = false;
			int origQIndex = element.getQueueIndex();
			int origPriority = element.getPriority();
			try {
				if (values.has(BPOConstants.QUEUE_INDEX)) {
					int qIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
					element.setQueueIndex(qIndex);
				}

				if (values.has(BPOConstants.PRIORITY)) {
					int priority = values.get(BPOConstants.PRIORITY).getAsInt();
					element.setPriority(priority);
				}

				isSuccess = transferElementAnotherIntance(nextNodeId, element,
						endpoint);
			} catch (IOException e) {
				e.printStackTrace();
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());
			} catch (Exception e) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
			if (isSuccess) {
				// remove from former node
				if (element.getStatus().equalsIgnoreCase(PROCESSING)) {
					Element inprocessElement = currentNode
							.removeFromInProcElemList(elementId);

					if (inprocessElement != null) {
						// sets the endProcTime of the element to current time
						// in ms
						// inprocessElement.setEndProcTime(System.currentTimeMillis());

						// increases the processing duration (endProcTime �
						// startProcTime) of the element
						long startProcTime = inprocessElement
								.getStartProcTime();
						// long endProcTime = inprocessElement.getEndProcTime();
						long procDur = System.currentTimeMillis()
								- startProcTime;
						inprocessElement.incProcessingDuration(procDur);
						incNodeProcessingMeasures(currentNode, procDur);

						// sets the status of the element to COMPLETE
						inprocessElement.setStatus(TRANSFERRED);
						currentNode.decTotalProcessingElemCount();
						String workerId = inprocessElement.getWorkerId();
						Worker worker = WorkerRepository.getWorker(workerId);
						worker.removeElement(elementId);
						return new ElementOperationResponse(true,
								ElementOperation.TRANSFER_ELEMENT);
					} else {
						return new ElementOperationResponse(false,
								ElementOperation.TRANSFER_ELEMENT, json,
								OperationResponseCode.OPERATION_FAILED
										.getCode());
					}

				} else if (element.getStatus().equalsIgnoreCase(WAITING)) {
					Element waitingElement = currentNode
							.removeFromWaitingQueue(elementId,
									element.getQueueIndex());

					if (waitingElement != null) {
						waitingElement.setEndProcTime(System
								.currentTimeMillis());

						long startWaitTime = waitingElement.getStartWaitTime();
						long waitingDuration = System.currentTimeMillis()
								- startWaitTime;

						incNodeWaitingMeasures(currentNode, waitingDuration);
						waitingElement.setStatus(TRANSFERRED);

						currentNode.decTotalWaitingElemCount();

						return new ElementOperationResponse(true,
								ElementOperation.TRANSFER_ELEMENT);

					} else {
						return new ElementOperationResponse(false,
								ElementOperation.TRANSFER_ELEMENT, json,
								OperationResponseCode.OPERATION_FAILED
										.getCode());
					}

				}
				ElementRepository.removeElement(elementId);
				return new ElementOperationResponse(true,
						ElementOperation.TRANSFER_ELEMENT);
			} else {
				element.setQueueIndex(origQIndex);
				element.setPriority(origPriority);
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());
			}
		}

		int queueIndex = -1;
		int priority = -1;
		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		try {
			if(values.has(BPOConstants.PRIORITY)){
				priority = values.get(BPOConstants.PRIORITY).getAsInt();
			}
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		if (GeneralUtil.isParamsNullOrEmpty(nodeId, elementId, json, nextNodeId)
				|| queueIndex > 99
				|| queueIndex < 0) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if(values.has(BPOConstants.PRIORITY) && (priority > 9 || priority < 0)){
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		if (NodeRepository.contains(nodeId)
				&& NodeRepository.contains(nextNodeId)) {

			Node currentNode = NodeRepository.getNode(nodeId);

			Node nextNode = NodeRepository.getNode(nextNodeId);

			if (nextNode.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_IS_CLOSED.getCode());
			}

			// loop
			List<String> elemList = new ArrayList<String>();
			elemList.add(elementId);
			Element elem = null;
			List<Element> elementList = currentNode.getElements(elemList);
			for (Element e : elementList) {
				if (e.getElementId().equalsIgnoreCase(elementId)) {
					elem = e;
				}
			}

			if (elem == null) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}

			String status = elem.getStatus();
			// removes the element from the in-proc list of the node
			if (status.equalsIgnoreCase(PROCESSING)) {

				Element inprocessElement = currentNode
						.removeFromInProcElemList(elementId);

				// check if the the element is not NULL
				if (inprocessElement != null) {
					// sets the endProcTime of the element to current time in ms
					// inprocessElement.setEndProcTime(System.currentTimeMillis());

					// increases the processing duration (endProcTime �
					// startProcTime) of the element
					long startProcTime = inprocessElement.getStartProcTime();
					// long endProcTime = inprocessElement.getEndProcTime();
					long procDur = System.currentTimeMillis() - startProcTime;
					inprocessElement.incProcessingDuration(procDur);

					// sets the status of the element to COMPLETE
					inprocessElement.setStatus(TRANSFERRED);
					currentNode.decTotalProcessingElemCount();

					// resets the startProcTime and endProcTime to 0, and
					// startWaitTime to current time
					inprocessElement.setStartProcTime(0L);
					inprocessElement.setEndProcTime(0L);
					inprocessElement.setStartWaitTime(System
							.currentTimeMillis());

					// resets the waiting duration and processing duration to 0
					inprocessElement.setWaitingDuration(0L);
					inprocessElement.setProcessingDuration(0L);

					incNodeProcessingMeasures(currentNode, procDur);
					String workerId = inprocessElement.getWorkerId();
					Worker worker = WorkerRepository.getWorker(workerId);
					// resets the workerId of the element to empty
					inprocessElement.setWorkerId("");

					// sets the nodeId of the element to nextNodeId
					inprocessElement.setNodeId(nextNodeId);

					// sets the status of the element to WAITING
					inprocessElement.setStatus(WAITING);
					if(priority != -1){
						inprocessElement.setPriority(priority);
					}
					inprocessElement.setQueueIndex(queueIndex);

					// enqueues the element to the next node, isNewElement flag
					// = false
					// getting the next node

					nextNode.enqueueElement(inprocessElement, false);
					nextNode.incTotalWaitingElemCount();

					// removes the element Id from the elements list of the
					// worker
					// worker.getElement(nextNodeId)elements.remove(inprocessElement.getElementId());
					worker.removeElement(elementId);
					return new ElementOperationResponse(true,
							ElementOperation.TRANSFER_ELEMENT);
				} else {
					return new ElementOperationResponse(false,
							ElementOperation.TRANSFER_ELEMENT, json,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST
									.getCode());
				}
			} else if (status.equalsIgnoreCase(WAITING)) {
				Element waitingElement = currentNode.removeFromWaitingQueue(
						elementId, elem.getQueueIndex());

				if (waitingElement != null) {
					waitingElement.setEndProcTime(System.currentTimeMillis());

					long startWaitTime = waitingElement.getStartWaitTime();
					long waitingDuration = System.currentTimeMillis()
							- startWaitTime;

					incNodeWaitingMeasures(currentNode, waitingDuration);
					waitingElement.setStatus(TRANSFERRED);
					currentNode.decTotalWaitingElemCount();

					// resets the startProcTime and endProcTime to 0, and
					// startWaitTime to current time
					waitingElement.setStartProcTime(0L);
					waitingElement.setEndProcTime(0L);
					waitingElement.setStartWaitTime(System.currentTimeMillis());

					// resets the waiting duration and processing duration to 0
					waitingElement.setWaitingDuration(0L);
					waitingElement.setProcessingDuration(0L);

					// resets the workerId of the element to empty
					waitingElement.setWorkerId("");

					// sets the nodeId of the element to nextNodeId
					waitingElement.setNodeId(nextNodeId);

					// sets the status of the element to WAITING
					waitingElement.setStatus(WAITING);
					if(priority != -1){
						waitingElement.setPriority(priority);
					}
					waitingElement.setQueueIndex(queueIndex);

					// enqueues the element to the next node, isNewElement flag
					// = false
					// getting the next node
					nextNode.enqueueElement(waitingElement, false);
					nextNode.incTotalWaitingElemCount();

					System.out.println(waitingElement
							.getProductionOutputUnits());
					return new ElementOperationResponse(true,
							ElementOperation.TRANSFER_ELEMENT);

				} else {
					return new ElementOperationResponse(false,
							ElementOperation.TRANSFER_ELEMENT, json,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST
									.getCode());
				}

			} else {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());

			}
		} else {
			if (!NodeRepository.contains(nodeId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			} else {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NEXT_NODE_DOES_NOT_EXIST
								.getCode());
			}
		}
	}

	public static ElementOperationResponse transferElement(String nodeId,
			String elementId, String nextNodeId, String json) {
		
		JsonObject values = GeneralUtil.parseToJson(json);
		
		if(Strings.isNullOrEmpty(nextNodeId.trim()) || !values.has(BPOConstants.QUEUE_INDEX)){
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}		

		if (values.has(ENDPOINT)) {
			if (!NodeRepository.contains(nodeId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			}

			Node currentNode = NodeRepository.getNode(nodeId);

			if (!currentNode.hasElement(elementId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}
			List<String> elemList = new ArrayList<String>();
			elemList.add(elementId);
			Element element = currentNode.getElements(elemList).get(0);

			String endpoint = values.get(ENDPOINT).getAsString();

			boolean isSuccess = false;
			int origQIndex = element.getQueueIndex();
			int origPriority = element.getPriority();
			try {
				if (values.has(BPOConstants.QUEUE_INDEX)) {
					int qIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
					element.setQueueIndex(qIndex);
				}

				if (values.has(BPOConstants.PRIORITY)) {
					int priority = values.get(BPOConstants.PRIORITY).getAsInt();
					element.setPriority(priority);
				}

				isSuccess = transferElementAnotherIntance(nextNodeId, element,
						endpoint);
			} catch (IOException e) {
				e.printStackTrace();
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());
			} catch (Exception e) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}
			if (isSuccess) {
				// remove from former node				
				if (element.getStatus().equalsIgnoreCase(PROCESSING)) {
					Element inprocessElement = currentNode
							.removeFromInProcElemList(elementId);

					if (inprocessElement != null) {
						// sets the endProcTime of the element to current time
						// in ms
						// inprocessElement.setEndProcTime(System.currentTimeMillis());

						// increases the processing duration (endProcTime �
						// startProcTime) of the element
						long startProcTime = inprocessElement
								.getStartProcTime();
						// long endProcTime = inprocessElement.getEndProcTime();
						long procDur = System.currentTimeMillis()
								- startProcTime;
						inprocessElement.incProcessingDuration(procDur);
						incNodeProcessingMeasures(currentNode, procDur);

						// sets the status of the element to COMPLETE
						inprocessElement.setStatus(TRANSFERRED);
						currentNode.decTotalProcessingElemCount();
						String workerId = inprocessElement.getWorkerId();
						Worker worker = WorkerRepository.getWorker(workerId);
						worker.removeElement(elementId);
						return new ElementOperationResponse(true,
								ElementOperation.TRANSFER_ELEMENT,json,inprocessElement);
					} else {
						return new ElementOperationResponse(false,
								ElementOperation.TRANSFER_ELEMENT, json,
								OperationResponseCode.OPERATION_FAILED
										.getCode());
					}

				} else if (element.getStatus().equalsIgnoreCase(WAITING)) {
					Element waitingElement = currentNode
							.removeFromWaitingQueue(elementId,
									element.getQueueIndex());

					if (waitingElement != null) {
						waitingElement.setEndProcTime(System
								.currentTimeMillis());

						long startWaitTime = waitingElement.getStartWaitTime();
						long waitingDuration = System.currentTimeMillis()
								- startWaitTime;

						incNodeWaitingMeasures(currentNode, waitingDuration);
						waitingElement.setStatus(TRANSFERRED);

						currentNode.decTotalWaitingElemCount();

						return new ElementOperationResponse(true,
								ElementOperation.TRANSFER_ELEMENT,json,waitingElement);

					} else {
						return new ElementOperationResponse(false,
								ElementOperation.TRANSFER_ELEMENT, json,
								OperationResponseCode.OPERATION_FAILED
										.getCode());
					}

				}
				Element removeElement = ElementRepository.removeElement(elementId);
				return new ElementOperationResponse(true,
						ElementOperation.TRANSFER_ELEMENT,json,removeElement);
			} else {
				element.setQueueIndex(origQIndex);
				element.setPriority(origPriority);
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());
			}
		}

		int queueIndex = -1;
		int priority = -1;
		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		try {
			if(values.has(BPOConstants.PRIORITY)){
				priority = values.get(BPOConstants.PRIORITY).getAsInt();
			}
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		try {
			queueIndex = values.get(BPOConstants.QUEUE_INDEX).getAsInt();
		} catch (ClassCastException | IllegalStateException e) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		if (GeneralUtil.isParamsNullOrEmpty(nodeId, elementId, json, nextNodeId)
				|| queueIndex > 99
				|| queueIndex < 0) {
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if(values.has(BPOConstants.PRIORITY) && (priority > 9 || priority < 0)){
			return new ElementOperationResponse(false,
					ElementOperation.TRANSFER_ELEMENT, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		if (NodeRepository.contains(nodeId)
				&& NodeRepository.contains(nextNodeId)) {

			Node currentNode = NodeRepository.getNode(nodeId);

			Node nextNode = NodeRepository.getNode(nextNodeId);

			if (nextNode.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_IS_CLOSED.getCode());
			}

			// loop
			List<String> elemList = new ArrayList<String>();
			elemList.add(elementId);
			Element elem = null;
			List<Element> elementList = currentNode.getElements(elemList);
			for (Element e : elementList) {
				if (e.getElementId().equalsIgnoreCase(elementId)) {
					elem = e;
				}
			}

			if (elem == null) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}

			String status = elem.getStatus();
			// removes the element from the in-proc list of the node
			if (status.equalsIgnoreCase(PROCESSING)) {

				Element inprocessElement = currentNode
						.removeFromInProcElemList(elementId);

				// check if the the element is not NULL
				if (inprocessElement != null) {
					// sets the endProcTime of the element to current time in ms
					// inprocessElement.setEndProcTime(System.currentTimeMillis());

					// increases the processing duration (endProcTime �
					// startProcTime) of the element
					long startProcTime = inprocessElement.getStartProcTime();
					// long endProcTime = inprocessElement.getEndProcTime();
					long procDur = System.currentTimeMillis() - startProcTime;
					inprocessElement.incProcessingDuration(procDur);

					// sets the status of the element to COMPLETE
					inprocessElement.setStatus(TRANSFERRED);
					currentNode.decTotalProcessingElemCount();

					// resets the startProcTime and endProcTime to 0, and
					// startWaitTime to current time
					inprocessElement.setStartProcTime(0L);
					inprocessElement.setEndProcTime(0L);
					inprocessElement.setStartWaitTime(System
							.currentTimeMillis());

					// resets the waiting duration and processing duration to 0
					inprocessElement.setWaitingDuration(0L);
					inprocessElement.setProcessingDuration(0L);

					incNodeProcessingMeasures(currentNode, procDur);
					String workerId = inprocessElement.getWorkerId();
					Worker worker = WorkerRepository.getWorker(workerId);
					// resets the workerId of the element to empty
					inprocessElement.setWorkerId("");

					// sets the nodeId of the element to nextNodeId
					inprocessElement.setNodeId(nextNodeId);

					// sets the status of the element to WAITING
					inprocessElement.setStatus(WAITING);
					if(priority != -1){
						inprocessElement.setPriority(priority);
					}
					inprocessElement.setQueueIndex(queueIndex);

					// enqueues the element to the next node, isNewElement flag
					// = false
					// getting the next node

					nextNode.enqueueElement(inprocessElement, false);
					nextNode.incTotalWaitingElemCount();

					// removes the element Id from the elements list of the
					// worker
					// worker.getElement(nextNodeId)elements.remove(inprocessElement.getElementId());
					worker.removeElement(elementId);
					return new ElementOperationResponse(true,
							ElementOperation.TRANSFER_ELEMENT,json,inprocessElement);
				} else {
					return new ElementOperationResponse(false,
							ElementOperation.TRANSFER_ELEMENT, json,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST
									.getCode());
				}
			} else if (status.equalsIgnoreCase(WAITING)) {
				Element waitingElement = currentNode.removeFromWaitingQueue(
						elementId, elem.getQueueIndex());

				if (waitingElement != null) {
					waitingElement.setEndProcTime(System.currentTimeMillis());

					long startWaitTime = waitingElement.getStartWaitTime();
					long waitingDuration = System.currentTimeMillis()
							- startWaitTime;

					incNodeWaitingMeasures(currentNode, waitingDuration);
					waitingElement.setStatus(TRANSFERRED);
					currentNode.decTotalWaitingElemCount();

					// resets the startProcTime and endProcTime to 0, and
					// startWaitTime to current time
					waitingElement.setStartProcTime(0L);
					waitingElement.setEndProcTime(0L);
					waitingElement.setStartWaitTime(System.currentTimeMillis());

					// resets the waiting duration and processing duration to 0
					waitingElement.setWaitingDuration(0L);
					waitingElement.setProcessingDuration(0L);

					// resets the workerId of the element to empty
					waitingElement.setWorkerId("");

					// sets the nodeId of the element to nextNodeId
					waitingElement.setNodeId(nextNodeId);

					// sets the status of the element to WAITING
					waitingElement.setStatus(WAITING);
					if(priority != -1){
						waitingElement.setPriority(priority);
					}
					waitingElement.setQueueIndex(queueIndex);

					// enqueues the element to the next node, isNewElement flag
					// = false
					// getting the next node
					nextNode.enqueueElement(waitingElement, false);
					nextNode.incTotalWaitingElemCount();

					System.out.println(waitingElement
							.getProductionOutputUnits());
					return new ElementOperationResponse(true,
							ElementOperation.TRANSFER_ELEMENT,json,waitingElement);

				} else {
					return new ElementOperationResponse(false,
							ElementOperation.TRANSFER_ELEMENT, json,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST
									.getCode());
				}

			} else {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.OPERATION_FAILED.getCode());

			}
		} else {
			if (!NodeRepository.contains(nodeId)) {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			} else {
				return new ElementOperationResponse(false,
						ElementOperation.TRANSFER_ELEMENT, json,
						OperationResponseCode.NEXT_NODE_DOES_NOT_EXIST
								.getCode());
			}
		}
	}

	private static void incNodeWaitingMeasures(Node node, long duration) {
		if (duration > node.getMaxWaitDur()){
			node.setMaxWaitDur(duration);
		}
		if (duration < node.getMinWaitDur() || node.getMinWaitDur() == 0) {
			node.setMinWaitDur(duration);
		}

		// node.incTotalWaitingElemCount();
		int n = node.getTotalWaitingElemCount();
		if (n == 0) {
			n = 1;
		}
//		System.out.println("n is " + n);
		long oldAve = node.getAveWaitDur().longValue();
		long newAve = 0;

		newAve = oldAve * (n - 1) / n + duration / n;
		BigDecimal big = new BigDecimal(newAve);
		node.setAveWaitDur(big);

	}

	private static void incNodeProcessingMeasures(Node node, long duration) {
		if (duration > node.getMaxProcDur())
			;
		{
			node.setMaxProcDur(duration);
		}
		if (duration < node.getMinProcDur() || node.getMinProcDur() == 0) {
			node.setMinProcDur(duration);
		}

		// node.incTotalProcessingElemCount();
		int n = node.getTotalProcessingElemCount();
		if (n == 0) {
			n = 1;
		}
		long oldAve = node.getAveProcDur().longValue();
		long newAve = 0;

		newAve = oldAve * (n - 1) / n + duration / n;
		BigDecimal big = new BigDecimal(newAve);
		node.setAveProcDur(big);

	}

	public static ElementOperationResponse correctEntry(String nodeId,
			String elementId, String json) {

		if (GeneralUtil.isParamsNullOrEmpty(nodeId, elementId, json)) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		JsonObject values = null;
		String nodeIdExceptionFound = null;

		try {
			values = new JsonParser().parse(json).getAsJsonObject();
			nodeIdExceptionFound = values.get(BPOConstants.NODE_ID).getAsString();
		} catch (JsonParseException | IllegalStateException
				| ClassCastException | NullPointerException e) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		if (GeneralUtil.isParamsNullOrEmpty(nodeIdExceptionFound)) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		if (!NodeRepository.contains(nodeId)
				|| !NodeRepository.contains(nodeIdExceptionFound)) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}

		if (!ElementRepository.contains(elementId)) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
		}

		Map<String, ProductionOutputUnit> prods = null;
		try {
			prods = gson.fromJson(values.get(BPOConstants.PROD_OUTPUT_UNITS),
					new TypeToken<Map<String, ProductionOutputUnit>>() {
					}.getType());
		} catch (JsonParseException | IllegalStateException
				| ClassCastException | NullPointerException e) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		Map<String, ProductionOutputUnit> productionOutputUnits = new HashMap<>();

		if (prods == null || prods.isEmpty()) {
			return new ElementOperationResponse(false,
					ElementOperation.CORRECT_ENTRY, json,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else {
			Map<String, ProductionOutputUnit> nodeProds = NodeRepository
					.getNode(nodeIdExceptionFound).getProductionOutputUnits();
			for (Entry<String, ProductionOutputUnit> each : prods.entrySet()) {
				String key = each.getKey();
				ProductionOutputUnit inputProd = each.getValue();
				if (Strings.isNullOrEmpty(key) || inputProd == null
						|| !nodeProds.containsKey(key)) {
					continue;
				}

				inputProd.setMeasurementUnit(nodeProds.get(key)
						.getMeasurementUnit());
				inputProd.setCost(nodeProds.get(key).getCost());
				productionOutputUnits.put(key, inputProd);
			}
		}

		BaggageTag baggageTag = new BaggageTag();
		baggageTag.setNodeId(nodeIdExceptionFound);
		baggageTag.setElementId(elementId);
		baggageTag.setProductionOutputUnits(productionOutputUnits);
		BaggageTagBuffer.enqueueBaggageTag(baggageTag);
		NodeRepository.getNode(nodeIdExceptionFound).incTotalExceptionCount();
		return new ElementOperationResponse(true,
				ElementOperation.CORRECT_ENTRY);

	}

	public static ElementOperationResponse deleteElement(String nodeId,
			String elementId) {
		if (GeneralUtil.isParamsNullOrEmpty(nodeId, elementId)) {
			return new ElementOperationResponse(false,
					ElementOperation.DELETE_ELEM, nodeId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}
		
		if (!NodeRepository.contains(nodeId)) {
			return new ElementOperationResponse(false,
					ElementOperation.DELETE_ELEM, nodeId,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}		
		
		Node currentNode = NodeRepository.getNode(nodeId);
		List<String> elemList = new ArrayList<String>();
		elemList.add(elementId);
		Element elem = null;
		List<Element> elementList = currentNode.getElements(elemList);
		for (Element e : elementList) {
			if (e.getElementId().equalsIgnoreCase(elementId)) {
				elem = e;
			}
		}

		if (elem == null) {
			return new ElementOperationResponse(false,
					ElementOperation.DELETE_ELEM,
					OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
		} else if (elem.getStatus().equalsIgnoreCase(PROCESSING)) {
			Element inprocessElement = currentNode
					.removeFromInProcElemList(elementId);
			ReentrantLock lock = inprocessElement.getLock();
			lock.lock();
			try {
			long startProcTime = inprocessElement.getStartProcTime();
			inprocessElement.incProcessingDuration(System.currentTimeMillis() - startProcTime);
			
			inprocessElement.setStatus(DELETED);
			currentNode.decTotalProcessingElemCount();
			Worker worker = WorkerRepository.getWorker(elem.getWorkerId());

			worker.removeElement(elementId);
			ElementRepository.removeElement(elementId);
			} finally {
				lock.unlock();
			}
			return new ElementOperationResponse(true,
					ElementOperation.DELETE_ELEM);

		} else if (elem.getStatus().equalsIgnoreCase(WAITING)) {

			Element deleted = currentNode.removeFromWaitingQueue(elementId,
					elem.getQueueIndex());
			
			if (deleted != null) {
				ReentrantLock lock = deleted.getLock();
				lock.lock();
				try {
				long startWaitTime = deleted.getStartWaitTime();
				long waitingDuration = System.currentTimeMillis() - startWaitTime;
				if (deleted.getWaitingDuration() == 0L) {
					deleted.setWaitingDuration(waitingDuration);
					
				} else {
					deleted.incWaitingDuration(waitingDuration);
				}
				
				elem.setStatus(DELETED);
				currentNode.decTotalWaitingElemCount();
				ElementRepository.removeElement(elementId);
				} finally {
					lock.unlock();
				}
				return new ElementOperationResponse(true,
						ElementOperation.DELETE_ELEM);
			} else {
				return new ElementOperationResponse(false,
						ElementOperation.DELETE_ELEM,
						OperationResponseCode.OPERATION_FAILED.getCode());
			}

		} else {
			return new ElementOperationResponse(false,
					ElementOperation.DELETE_ELEM,
					OperationResponseCode.INVALID_PARAMS.getCode());

		}

	}

	/**
	 * Search element across all nodes
	 * 
	 * @param elementId
	 * @return
	 */
	public static ElementOperationResponse getElement(String elementId) {
		if (ElementRepository.contains(elementId)) {
			Element element = ElementRepository.getElement(elementId);
			ElementOperationResponse response = new ElementOperationResponse(true, ElementOperation.GET_ELEM);
			response.setElement(element);
			return response;
		} else {
			return new ElementOperationResponse(false, ElementOperation.GET_ELEM,
					OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
		}
	}

	/**
	 * Delete all elements in BPO
	 * 
	 * @return
	 */
	public static ElementOperationResponse deleteAllElements() {

		try {
			NodeOperationResponse nodeResponse = NodeUtil.getNodes();
			List<Node> nodes = nodeResponse.getNodes();
			for (Node node : nodes) {
				NodeUtil.deleteNodeElements(node.getNodeId());
			}
			return new ElementOperationResponse(true, ElementOperation.DELETE_ELEM);
		} catch (Exception e) {
			return new ElementOperationResponse(false, ElementOperation.DELETE_ELEM,
					OperationResponseCode.OPERATION_FAILED.getCode());
		}

	}


}
