package com.svi.bpo.util;

import java.io.IOException;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AtomicDoubleTypeAdapter extends TypeAdapter<AtomicDouble> {

    @Override
    public AtomicDouble read(JsonReader in) throws IOException {

    	AtomicDouble value = null;

        JsonParser jsonParser = new JsonParser();
        JsonElement je = jsonParser.parse(in);

        if (je instanceof JsonPrimitive) {
            value = new AtomicDouble();
            value.set(((JsonPrimitive) je).getAsDouble());
        } else if (je instanceof JsonObject) {
            JsonObject jsonObject = (JsonObject) je;
            value = new AtomicDouble();
            value.set(jsonObject.get("value").getAsDouble());
        }

        return value;
    }

    @Override
    public void write(JsonWriter out, AtomicDouble value) throws IOException {
        if (value != null) {
			out.value(value.get());
        }
    }
}
