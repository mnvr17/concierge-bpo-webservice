package com.svi.bpo.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.svi.bpo.core.BPOConstants;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.bpo.core.Worker;
import com.svi.bpo.core.WorkerRepository;
import com.svi.bpo.response.NodeOperation;
import com.svi.bpo.response.NodeOperationResponse;
import com.svi.bpo.response.NodeOperationResponse.WorkerQueueKV;
import com.svi.bpo.response.OperationResponseCode;
import com.svi.bpo.workerdb.WorkerDBThread;
import com.svi.bpo.workerdb.WorkerDBThread.WorkerDBTransaction;
import com.svi.object.ReqType;
import com.svi.object.ServiceObj;
import com.svi.object.ServiceType;
import com.svi.object.TriggerType;

@SuppressWarnings("serial")
public class NodeUtil {
	private static final String NODE_NAME = "nodeName";
	private static final String CLUSTER = "cluster";
	private static final String STATUS = "status";
	private static final String WORKER_NAME = "workerName";
	private static final String ELEM_TYPE = "elemType";
	private static final String AVE_WAIT_DUR_LIMIT = "aveWaitDurLimit";
	private static final String AVE_PROC_DUR_LIMIT = "aveProcDurLimit";
	private static final int NODE_TEMPLATE_HEADER_COUNT = 22;
	private static final int WORKER_TEMPLATE_HEADER_COUNT = 3;
	private static final String SERVICES = "services";
	private static final String SERVICES_TO_RUN = "servicesToRun";
	private int columnNumErrorCreate;
	private int columnNumErrorUpdate;
	private int columnNumErrorAssign;
	private int columnNumErrorUnassign;	
	
	
	private static Gson gson = new GsonBuilder()
					.registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
					}.getType(), new AtomicStringTypeAdapter())
					.registerTypeAdapter(
							new TypeToken<AtomicReference<BigDecimal>>() {
							}.getType(), new AtomicBigDecimalTypeAdapter())
					.registerTypeAdapter(new TypeToken<AtomicLong>() {
					}.getType(), new AtomicLongTypeAdapter())
					.registerTypeAdapter(new TypeToken<AtomicInteger>() {
					}.getType(), new AtomicIntegerTypeAdapter())
					.registerTypeAdapter(new TypeToken<AtomicDouble>() {
					}.getType(), new AtomicDoubleTypeAdapter())
					.excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
					.create();
	
	public static NodeOperationResponse createNode(String json, String nodeId) {
		JsonParser parser = new JsonParser();
		JsonObject values = parser.parse(json).getAsJsonObject();
		//String nodeId = values.get(BPOConstants.NODE_ID).getAsString();
		String nodeName = values.get(NODE_NAME).getAsString();
		String cluster = values.get(CLUSTER).getAsString();
		String status = values.get(STATUS).getAsString();
		String elemType = values.get(ELEM_TYPE).getAsString();
		
		if(GeneralUtil.isParamsNullOrEmpty(nodeId, nodeName, cluster, elemType, status)				
				|| !(status.equalsIgnoreCase(BPOConstants.OPEN) || status.equalsIgnoreCase(BPOConstants.CLOSE))){
			return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
		}
		Map<String, ProductionOutputUnit> prods = gson.fromJson(
				values.get(BPOConstants.PROD_OUTPUT_UNITS),
				new TypeToken<Map<String, ProductionOutputUnit>>(){}.getType());
		Map<String, ProductionOutputUnit> productionOutputUnits = new HashMap<>();
		if(prods.isEmpty()){
			throw new IllegalArgumentException();
		} else {
			for(Entry<String, ProductionOutputUnit> each : prods.entrySet()){
				String key = each.getKey();
				ProductionOutputUnit prod = each.getValue();
				if(GeneralUtil.isParamsNullOrEmpty(key,prod.getMeasurementUnit())
					|| prod == null){
					return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				productionOutputUnits.put(key,
						new ProductionOutputUnit(prod.getMeasurementUnit(), prod.getCost()));
			}
		}
		
		long aveWaitDurLimit = 0L;
		long aveProcDurLimit = 0L;
		List<ServiceObj> services = null;
		ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun = null;
		if(values.has(AVE_WAIT_DUR_LIMIT)){
			aveWaitDurLimit = values.get(AVE_WAIT_DUR_LIMIT).getAsLong();
		}
		if(values.has(AVE_PROC_DUR_LIMIT)){
			aveProcDurLimit = values.get(AVE_PROC_DUR_LIMIT).getAsLong();
		}
		
		if(values.has(AVE_PROC_DUR_LIMIT)){
			aveProcDurLimit = values.get(AVE_PROC_DUR_LIMIT).getAsLong();
		}
		if(values.has(SERVICES)){
			services = new Gson().fromJson(values.get(SERVICES).toString(), new TypeToken<List<ServiceObj>>(){}.getType());
		}
		if(values.has(SERVICES_TO_RUN)){
			servicesToRun = new Gson().fromJson(values.get(SERVICES_TO_RUN).toString(), new TypeToken<ConcurrentHashMap<TriggerType, List<ServiceObj>>>(){}.getType());
		}
		
		
		//No need to catch the thrown exception because nodeId and node is assured to be not null
	
//		Node node = new Node(nodeId, nodeName, cluster, status, productionOutputUnits, elemType, aveWaitDurLimit, aveProcDurLimit, services);
		Node node = new Node(nodeId, nodeName, cluster, status, productionOutputUnits, elemType, aveWaitDurLimit, aveProcDurLimit, servicesToRun);
		if(NodeRepository.addNode(
					nodeId,
					node)){
			return new NodeOperationResponse(true, NodeOperation.CREATE, nodeId, json);
		} else {
			return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, json, OperationResponseCode.NODE_ALREADY_EXISTS.getCode());
		}
	}
	
	public static NodeOperationResponse deleteNode(String nodeId){
		if(!Strings.isNullOrEmpty(nodeId)){
			if(NodeRepository.contains(nodeId)){
				NodeRepository.removeNode(nodeId);
				return new NodeOperationResponse(true, NodeOperation.DELETE, nodeId);
			} else {
				return new NodeOperationResponse(false, NodeOperation.DELETE, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			}
		} else {
			return new NodeOperationResponse(false, NodeOperation.DELETE, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
		}
	}
	
	public static NodeOperationResponse deleteNodeElements(String nodeId){
		if(!Strings.isNullOrEmpty(nodeId)){
			if(NodeRepository.contains(nodeId)){
				NodeOperationResponse nodeOperationResponse = getElements(nodeId);
				
				if(nodeOperationResponse.isSuccessful()) {
					List<Element> elements= nodeOperationResponse.getElements();
					for(Element element : elements) {
						ElementUtil.deleteElement(element.getNodeId(), element.getElementId());
					}
					return new NodeOperationResponse(true, NodeOperation.DELETE_ELEMS, nodeId);
				} else {
					return nodeOperationResponse;
				}
				
			} else {
				return new NodeOperationResponse(false, NodeOperation.DELETE, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			}
		} else {
			return new NodeOperationResponse(false, NodeOperation.DELETE, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
		}
	}

	
	public static NodeOperationResponse updateNode(String nodeId, String json){
		JsonParser parser = new JsonParser();
		JsonObject values = parser.parse(json).getAsJsonObject();
		
		String nodeName = "";
		String status = "";
		
		if(Strings.isNullOrEmpty(nodeId)){
			return new NodeOperationResponse(false, NodeOperation.UPDATE_NODE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
		}
		
		if(values.has(NODE_NAME)){
			nodeName = values.get(NODE_NAME).getAsString();
		}
		
		if(values.has(STATUS)){
			status = values.get(STATUS).getAsString();
			if(!(status.equalsIgnoreCase(BPOConstants.OPEN) || status.equalsIgnoreCase(BPOConstants.CLOSE))){
				return new NodeOperationResponse(false, NodeOperation.UPDATE_NODE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
			}
		}
		
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			if(values.has(NODE_NAME)){
				node.setNodeName(nodeName);
			}
			if(values.has(STATUS)){
				node.setStatus(status);
			}
			return new NodeOperationResponse(true, NodeOperation.UPDATE_NODE, nodeId, json);
		} else {
			return new NodeOperationResponse(false, NodeOperation.UPDATE_NODE, nodeId, json, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse updateNodeServices(String nodeId, String json){
		
		ObjectMapper oMapper = new ObjectMapper();
		try {
			ConcurrentHashMap<TriggerType, List<ServiceObj>> map = oMapper.readValue(json, new TypeReference<ConcurrentHashMap<TriggerType, List<ServiceObj>>>(){});
			Node node = NodeRepository.getNode(nodeId);
			node.setServicesToRun(map);
		} catch (IOException e) {
			e.printStackTrace();
		}
		JsonParser parser = new JsonParser();
		
		if(Strings.isNullOrEmpty(nodeId)){
			return new NodeOperationResponse(false, NodeOperation.UPDATE_NODE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
		}
		
		
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
//			if(values.has(NODE_NAME)){
//				node.setNodeName(nodeName);
//			}
//			if(values.has(STATUS)){
//				node.setStatus(status);
//			}
			return new NodeOperationResponse(true, NodeOperation.UPDATE_NODE, nodeId, json);
		} else {
			return new NodeOperationResponse(false, NodeOperation.UPDATE_NODE, nodeId, json, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse getElements(String nodeId){
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_ELEM, nodeId);
			//System.out.println("ELEMENTS: "+ node.getElements());
			response.setElements(node.getElements());
			return response;
		} else {
			return new NodeOperationResponse(false, NodeOperation.VIEW_ELEM, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse getElements(String nodeId, String... elements){
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_ELEM, nodeId);
			response.setElements(node.getElements(Arrays.asList(elements)));
			return response;
		} else {
			return new NodeOperationResponse(false, NodeOperation.VIEW_ELEM, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse getWaitingElements(String nodeId){
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_WAIT_ELEM, nodeId);
			response.setElements(node.getWaitingElements());
			return response;
		} else {
			return new NodeOperationResponse(false, NodeOperation.VIEW_WAIT_ELEM, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse getProcessingElements(String nodeId){
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_PROC_ELEM, nodeId);
			response.setElements(node.getProcessingElements());
			return response;
		} else {
			return new NodeOperationResponse(false, NodeOperation.VIEW_PROC_ELEM, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}
	
	public static NodeOperationResponse getNodes(){
		NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_NODE);
		response.setNodes(NodeRepository.getAllNodes());
		return response;
	}
	
	public static NodeOperationResponse getNodes(String nodeId){
		if(!Strings.isNullOrEmpty(nodeId)){
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){
				NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.VIEW_NODE);
				List<Node> nodes = new ArrayList<>();
				nodes.add(node);
				response.setNodes(nodes);
				return response;
			} else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.VIEW_NODE, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
		} else {
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.VIEW_NODE, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
			return response;
		}
	}
	
	// no longer used
	public static NodeOperationResponse assignWorker(String nodeId, String jsonString){
		if(!Strings.isNullOrEmpty(nodeId)){ 
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){
				// parse json
				JsonObject values = GeneralUtil.parseToJson(jsonString);
				String workerId = values.get(BPOConstants.WORKER_ID).getAsString();
				String workerName = "";
				
				if(values.has(WORKER_NAME)){
					workerName = values.get(WORKER_NAME).getAsString().trim();
				}
				
				TreeSet<Integer> queueIndices = null;
				try{
					Integer[] qArr = gson.fromJson(
							values.get(BPOConstants.QUEUE_INDEX),
							Integer[].class);
					queueIndices = new TreeSet<>(Arrays.asList(qArr));
				}catch (JsonSyntaxException|
						ClassCastException|
						JsonIOException|
						IllegalStateException e){
					return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, jsonString, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				if(!Strings.isNullOrEmpty(workerId)
						&& queueIndices != null
						&& !queueIndices.isEmpty()){
					// check if worker exists in worker repository, if not, create a worker object					
					Worker worker = WorkerRepository.getWorker(workerId);
					if(worker == null){
						worker = new Worker(workerId, workerName);
					}
				
					if(node.assignWorkerToNode(workerId, queueIndices)){
						WorkerRepository.addWorker(workerId, worker);
						new WorkerDBThread(WorkerDBTransaction.WRITE).run();
						return new NodeOperationResponse(true, NodeOperation.ASSIGN_WORKER);
					}else{
						return new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId,OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
					}
				
				}else{
					NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
					return response;
				}
				
			}else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
			
		}else{
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			return response;			
		}
			
	}
	
	public static NodeOperationResponse assignWorker(String nodeId, String workerId, String jsonString){
		if(!Strings.isNullOrEmpty(nodeId)){ 
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){
				// parse json				
				JsonObject values = GeneralUtil.parseToJson(jsonString);
				//String workerId = values.get(WORKER_ID).getAsString();
				String workerName = "";
				
				if(values.has(WORKER_NAME)){
					workerName = values.get(WORKER_NAME).getAsString().trim();
				}
				
				TreeSet<Integer> queueIndices = null;
				try{
					Integer[] qArr = gson.fromJson(
							values.get(BPOConstants.QUEUE_INDEX),
							Integer[].class);
					queueIndices = new TreeSet<>(Arrays.asList(qArr));
				}catch (JsonSyntaxException|
						ClassCastException|
						JsonIOException|
						IllegalStateException e){
					return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, jsonString, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				if(!Strings.isNullOrEmpty(workerId.trim())
						&& queueIndices != null
						&& !queueIndices.isEmpty()){
					// check if worker exists in worker repository, if not, create a worker object					
					Worker worker = WorkerRepository.getWorker(workerId);
					if(worker == null){
						worker = new Worker(workerId, workerName);
					}
				
					if(node.assignWorkerToNode(workerId, queueIndices)){
						WorkerRepository.addWorker(workerId, worker);
						new WorkerDBThread(WorkerDBTransaction.WRITE).run();
						return new NodeOperationResponse(true, NodeOperation.ASSIGN_WORKER);
					}else{
						return new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId,OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
					}
				
				}else{
					NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
					return response;
				}
				
			}else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
			
		}else{
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			return response;			
		}
			
	}
		
	public static NodeOperationResponse unassignWorker(String nodeId, String jsonString, String workerId){
		
		if(!Strings.isNullOrEmpty(nodeId)){ 
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){
				// parse json
				JsonParser parser = new JsonParser();
				JsonObject values = parser.parse(jsonString).getAsJsonObject();
				//String workerId = values.get(BPOConstants.WORKER_ID).getAsString();
				TreeSet<Integer> queueIndices = null;
				try{
					Integer[] qArr = gson.fromJson(
							values.get(BPOConstants.QUEUE_INDEX),
							Integer[].class);
					queueIndices = new TreeSet<>(Arrays.asList(qArr));
				}catch (JsonSyntaxException|
						ClassCastException|
						JsonIOException|
						IllegalStateException e){
					return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, jsonString, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				if(!(Strings.isNullOrEmpty(workerId))){
					boolean isSuccess = false;
					
					if(!node.getWorkers().containsKey(workerId)){
						return new NodeOperationResponse(false, NodeOperation.UNASSIGN_WORKER,
								OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
					}
					
					if(queueIndices == null || queueIndices.isEmpty()){					
						isSuccess = node.unassignWorkerFromNode(workerId);
					} else if (queueIndices != null && !queueIndices.isEmpty()){
						isSuccess = node.unassignWorkerFromNode(workerId, queueIndices);
					} 
					
					if(isSuccess){
						new WorkerDBThread(WorkerDBTransaction.WRITE).run();
						return new NodeOperationResponse(true, NodeOperation.UNASSIGN_WORKER);
					}else{
						return new NodeOperationResponse(false, NodeOperation.UNASSIGN_WORKER,
								OperationResponseCode.OPERATION_FAILED.getCode());
					}
				
				}else{
					NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.UNASSIGN_WORKER, nodeId, OperationResponseCode.INVALID_PARAMS.getCode());
					return response;
				}
				
			}else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.UNASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
			
		}else{
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.UNASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			return response;			
		}
		
	}
	
	// no longer used
	public static NodeOperationResponse shiftWorker(String nodeId,String json){
		if(!Strings.isNullOrEmpty(nodeId)){ 
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){				
				JsonObject values = GeneralUtil.parseToJson(json);
				if(Strings.isNullOrEmpty(json)){

					return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				String workerId = values.get(BPOConstants.WORKER_ID).getAsString();
				String newNodeId = values.get(BPOConstants.NODE_ID).getAsString();

				TreeSet<Integer> queueIndices = null;
				try{
					Integer[] qArr = gson.fromJson(
							values.get(BPOConstants.QUEUE_INDEX),
							Integer[].class);
					queueIndices = new TreeSet<>(Arrays.asList(qArr));
				}catch (JsonSyntaxException|
						ClassCastException|
						JsonIOException|
						IllegalStateException e){
					return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				if(GeneralUtil.isParamsNullOrEmpty(workerId.trim(), newNodeId.trim())
					|| queueIndices == null
					|| queueIndices.isEmpty()){
					return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
				}
				
				TreeSet<Integer> prevQIdx = node.getWorkers().get(workerId);
				if(prevQIdx == null){
					return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
				}
								
				// unassign on nodeId
				Node newNode = NodeRepository.getNode(newNodeId);
				if(newNode != null){
					if(!node.unassignWorkerFromNode(workerId, prevQIdx)){
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.OPERATION_FAILED.getCode());
					}
				} else {
					return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				}
				
				boolean assign = newNode.assignWorkerToNode(workerId, queueIndices);
				if(!assign){
					node.assignWorkerToNode(workerId, prevQIdx);
					return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.OPERATION_FAILED.getCode());
				} 
				
				// assign on newNodeId
				new WorkerDBThread(WorkerDBTransaction.WRITE).run();
				return new NodeOperationResponse(true, NodeOperation.SHIFT_WORKER, nodeId);
			}else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
			
		}else{
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			return response;			
		}
		
	}
	
	// no longer used
	public static NodeOperationResponse shiftWorker(String nodeId, String workerId, String newNodeId, String json){
			if(!Strings.isNullOrEmpty(nodeId)){ 
				Node node = NodeRepository.getNode(nodeId);
				if(node != null){					
					JsonObject values = GeneralUtil.parseToJson(json);
					
					if(Strings.isNullOrEmpty(json)){
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
					}					
					
					TreeSet<Integer> queueIndices = null;
					try{
						Integer[] qArr = gson.fromJson(
								values.get(BPOConstants.QUEUE_INDEX),
								Integer[].class);
						queueIndices = new TreeSet<>(Arrays.asList(qArr));
					}catch (JsonSyntaxException|
							ClassCastException|
							JsonIOException|
							IllegalStateException e){
						return new NodeOperationResponse(false, NodeOperation.CREATE, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
					}
					
					if(GeneralUtil.isParamsNullOrEmpty(workerId.trim(), newNodeId.trim())
						|| queueIndices == null
						|| queueIndices.isEmpty()){
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.INVALID_PARAMS.getCode());
					}
					
					TreeSet<Integer> prevQIdx = node.getWorkers().get(workerId);
					if(prevQIdx == null){
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
					}
									
					// unassign on nodeId
					Node newNode = NodeRepository.getNode(newNodeId);
					if(newNode != null){
						if(!node.unassignWorkerFromNode(workerId, prevQIdx)){
							return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.OPERATION_FAILED.getCode());
						}
					} else {
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
					}
					
					boolean assign = newNode.assignWorkerToNode(workerId, queueIndices);
					if(!assign){
						node.assignWorkerToNode(workerId, prevQIdx);
						return new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, json, OperationResponseCode.OPERATION_FAILED.getCode());
					} 
					
					// assign on newNodeId
					new WorkerDBThread(WorkerDBTransaction.WRITE).run();
					return new NodeOperationResponse(true, NodeOperation.SHIFT_WORKER, nodeId);
				}else {
					NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.SHIFT_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
					return response;
				}
				
			}else{
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.ASSIGN_WORKER, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;			
			}
			
		}
		
	public static NodeOperationResponse getWorkers(String nodeId){
		if(!Strings.isNullOrEmpty(nodeId)){
			Node node = NodeRepository.getNode(nodeId);
			if(node != null){
				NodeOperationResponse response = new NodeOperationResponse(true, NodeOperation.GET_WORKERS);
				List<WorkerQueueKV> workers = new ArrayList<>();
				for(Entry<String, TreeSet<Integer>> each : node.getWorkers().entrySet()){
					workers.add(new WorkerQueueKV(each.getKey(), each.getValue().toArray(new Integer[each.getValue().size()])));
				}
				response.setWorkers(workers);
				// how to return
				return response;
			}else {
				NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.GET_WORKERS, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
				return response;
			}
			
		}else{
			NodeOperationResponse response = new NodeOperationResponse(false, NodeOperation.GET_WORKERS, nodeId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			return response;			
		}
		
	}
	
	public Response batchUpload(InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail){
		Map<String, Integer> erroneousNodes = new HashMap<>();		
		Map<String, String> errorMessages = new HashMap<>();		
		ByteArrayOutputStream out = null;
		if(!(uploadedInputStream == null || fileDetail == null)){	
					
			// getting the excel extension whether it is xls or xlsx
			String extension = FilenameUtils.getExtension(fileDetail.getFileName());	
			try {
				
				// create a workbook based on excel file version
				Workbook workbook = null;			
				if(extension.equalsIgnoreCase(BPOConstants.XLS)){				
					workbook =  new HSSFWorkbook(uploadedInputStream);
				}
				else{
					workbook  = new XSSFWorkbook(uploadedInputStream);
				}
				
				// getting the first work sheet of the excel file 
				Sheet sheet = workbook.getSheetAt(0);
				
				// iterating in each row of the file			
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) { // skip first row which is expected to be a header
					// converting each row data into node object
					Row row = sheet.getRow(i);				
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = createNode(row, errorMessages);				
						if(!isSuccessful){								
							erroneousNodes.put("" + (i + 1), columnNumErrorCreate);							
						}
					}
				}		
				
				if (!erroneousNodes.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousNodes, extension, errorMessages, NODE_TEMPLATE_HEADER_COUNT);
				}
				
				workbook.close();
			} catch (IOException e) {				
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}	
		
			
			if (erroneousNodes.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();

			}
		}
		else{
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}
	
	public Response batchUpdate(InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail){
		Map<String, Integer> erroneousNodes = new HashMap<>();
		Map<String, String> errorMessages = new HashMap<>();		
		ByteArrayOutputStream out = null;
		if(!(uploadedInputStream == null || fileDetail == null)){
			
			try {			
				// getting the excel extension whether it is xls or xlsx
				String extension = FilenameUtils.getExtension(fileDetail.getFileName());	
				
				// create a workbook based on excel file version
				Workbook workbook = null;			
				if(extension.equalsIgnoreCase(BPOConstants.XLS)){				
					workbook =  new HSSFWorkbook(uploadedInputStream);
				}
				else{
					workbook  = new XSSFWorkbook(uploadedInputStream);
				}
				
				// getting the first work sheet of the excel file 
				Sheet sheet = workbook.getSheetAt(0);
				
				// iterating in each row of the file			
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) { // skip first row which is expected to be a header
					// converting each row data into node object
					Row row = sheet.getRow(i);				
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = updateNode(row,errorMessages);				
						if(!isSuccessful){	
							erroneousNodes.put("" + (i + 1), columnNumErrorUpdate);							
						}
					}
				}			
				
				if (!erroneousNodes.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousNodes, extension, errorMessages, NODE_TEMPLATE_HEADER_COUNT);
				}
				
				workbook.close();
			} catch (IOException e) {				
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}				

			
			if (erroneousNodes.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();
			}
		}
		else{
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}
	
	public Response batchAssignWorker(InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail){
		Map<String, Integer> erroneousWorkers = new HashMap<>();
		Map<String, String> errorMessages = new HashMap<>();		
		ByteArrayOutputStream out = null;
		if(!(uploadedInputStream == null || fileDetail == null)){
			
			try {			
				// getting the excel extension whether it is xls or xlsx
				String extension = FilenameUtils.getExtension(fileDetail.getFileName());	
				
				// create a workbook based on excel file version
				Workbook workbook = null;			
				if(extension.equalsIgnoreCase(BPOConstants.XLS)){				
					workbook =  new HSSFWorkbook(uploadedInputStream);
				}
				else{
					workbook  = new XSSFWorkbook(uploadedInputStream);
				}
				
				// getting the first work sheet of the excel file 
				Sheet sheet = workbook.getSheetAt(0);
				
				// iterating in each row of the file			
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) { // skip first row which is expected to be a header
					// converting each row data into node object
					Row row = sheet.getRow(i);				
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = assignWorker(row, errorMessages);				
						if(!isSuccessful){							
							erroneousWorkers.put("" + (i + 1), columnNumErrorAssign);
						}
					}
				}
				if (!erroneousWorkers.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousWorkers, extension, errorMessages, WORKER_TEMPLATE_HEADER_COUNT);
				}
				workbook.close();
			} catch (IOException e) {				
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}	
			
			if (erroneousWorkers.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();
			}
		}
		else{
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}
		
	public Response batchUnassignWorker( InputStream uploadedInputStream,
			FormDataContentDisposition fileDetail){
		Map<String, Integer> erroneousWorkers = new HashMap<>();
		Map<String, String> errorMessages = new HashMap<>();		
		ByteArrayOutputStream out = null;
		if(!(uploadedInputStream == null || fileDetail == null)){
			
			try {			
				// getting the excel extension whether it is xls or xlsx
				String extension = FilenameUtils.getExtension(fileDetail.getFileName());	
				
				// create a workbook based on excel file version
				Workbook workbook = null;			
				if(extension.equalsIgnoreCase(BPOConstants.XLS)){				
					workbook =  new HSSFWorkbook(uploadedInputStream);
				}
				else{
					workbook  = new XSSFWorkbook(uploadedInputStream);
				}
				
				// getting the first work sheet of the excel file 
				Sheet sheet = workbook.getSheetAt(0);
				
				// iterating in each row of the file			
				for (int i = 1, j = sheet.getLastRowNum() + 1; i < j; i++) { // skip first row which is expected to be a header
					// converting each row data into node object
					Row row = sheet.getRow(i);				
					if (!GeneralUtil.isNullOrEmpty(row)) {
						// creating node object for each row
						boolean isSuccessful = unassignWorker(row, errorMessages);				
						if(!isSuccessful){						
							erroneousWorkers.put("" + (i + 1), columnNumErrorUnassign);
						}
					}
				}		
				if (!erroneousWorkers.isEmpty()) {
					out = GeneralUtil.getExceptionFile(workbook, erroneousWorkers, extension, errorMessages, WORKER_TEMPLATE_HEADER_COUNT);
				}
				workbook.close();
			} catch (IOException e) {				
				e.printStackTrace();
				return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
			}				
			
			if (erroneousWorkers.isEmpty()) {
				return Response.ok().build();
			} else {
				InputStream is = new ByteArrayInputStream(out.toByteArray());
				return Response.ok(is, MediaType.APPLICATION_OCTET_STREAM)
						.header("Access-Control-Expose-Headers", "Content-Disposition")
						.header("Content-Disposition",
								"attachment; filename=\"" + fileDetail.getFileName() + "\"")
						.build();

			}
		}
		else{
			return Response.status(OperationResponseCode.FILE_NOT_FOUND.getCode()).build();
		}
	}
	
	
	/**
	 * Assigns worker in a node.
	 * @param row The row to be parsed as one worker.
	 * @return TRUE if assigning of worker is successful. Otherwise, returns FALSE.
	 */
	private boolean assignWorker(Row row, Map<String, String> errorMessages){		
		// getting basic info
		String workerId = "";
		String workerName = "";
		String nodeId = "";		
		// getting worker id
		if(!GeneralUtil.isNullOrEmpty(row.getCell(0))){
			workerId = GeneralUtil.getCellValue(row.getCell(0));			
		}
		else{
			columnNumErrorAssign = 1;		
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorAssign, "Empty Worker Id");
			return false;
		}		
		// getting worker name
		if(!GeneralUtil.isNullOrEmpty(row.getCell(1))){
			workerName = GeneralUtil.getCellValue(row.getCell(1));
		}
		// getting node id		
		if(!GeneralUtil.isNullOrEmpty(row.getCell(2))){
			nodeId = GeneralUtil.getCellValue(row.getCell(2));
		}
		else{
			columnNumErrorAssign = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorAssign, "Empty Node Id");
			return false;
		}
		
		// create json
		JsonObject jsonWorker = new JsonObject();
		jsonWorker.addProperty(BPOConstants.WORKER_ID,workerId);			
		jsonWorker.addProperty(BPOConstants.QUEUE_INDEX, Arrays.toString(IntStream.rangeClosed(0, 99).toArray()));		
		String json = (new Gson().toJson(jsonWorker)).replaceAll("\"", "");
		
		boolean exists = WorkerRepository.contains(workerId);
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){			
			NodeOperationResponse response = NodeUtil.assignWorker(nodeId, json);			
			// get worker from repo
			Worker worker = WorkerRepository.getWorker(workerId);			
			if(response.isSuccessful()){				
				if(!exists){
					worker.setWorkerName(workerName);
				}
				return true;
			}
			else{
				columnNumErrorAssign = 0;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorAssign, "Cannot Specify Cause of Error");
				return false;
			}
		}
		else{			
			columnNumErrorAssign = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorAssign, "Node Id Does Not Exist");
			return false;
		}	
	}
	
	/**
	 * Unassigns worker in a node.
	 * @param row The row to be parsed as one worker.
	 * @return TRUE if unassigning of worker is successful. Otherwise, returns FALSE.
	 */
	private boolean unassignWorker(Row row, Map<String, String> errorMessages){		
		// getting basic info
		String workerId = "";		
		String nodeId = "";		
		// getting worker id
		if(!GeneralUtil.isNullOrEmpty(row.getCell(0))){
			workerId = GeneralUtil.getCellValue(row.getCell(0));			
		}
		else{
			columnNumErrorUnassign = 1;			
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUnassign, "Empty Worker Id");
			return false;
		}		
		
		// getting node id		
		if(!GeneralUtil.isNullOrEmpty(row.getCell(2))){
			nodeId = GeneralUtil.getCellValue(row.getCell(2));
		}
		else{
			columnNumErrorUnassign = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUnassign, "Empty Node Id");
			return false;
		}
		// checks existence of worker		
		if(!WorkerRepository.contains(workerId)){
			columnNumErrorUnassign = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUnassign, "Worker Id Does Not Exist");
			return false;
		}
		// create json
		JsonObject jsonWorker = new JsonObject();
		jsonWorker.addProperty(BPOConstants.WORKER_ID,workerId);
		jsonWorker.addProperty(BPOConstants.QUEUE_INDEX, Arrays.toString(IntStream.rangeClosed(0, 99).toArray()));					
		String json = (new Gson().toJson(jsonWorker)).replaceAll("\"", "");		
	
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){			
			NodeOperationResponse response = NodeUtil.unassignWorker(nodeId, json,workerId);	
			if(response.isSuccessful()){			
				return true;
			}
			else{
				columnNumErrorUnassign = 0;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUnassign, "Cannot Specify Cause of Error");
				return false;
			}
		}
		else{			
			columnNumErrorUnassign = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUnassign, "Node Id Does Not Exist");
			return false;
		}	
	}
			
	/**
	 * Creates new node per row.
	 * @param row The row to be parsed as one node.
	 * @return TRUE if creation of new node is successful. Otherwise, returns FALSE.
	 */
	private boolean createNode(Row row, Map<String, String> errorMessages){		
		// getting basic info
		String nodeId = "";
		String nodeName = "";
		String cluster = "";
		String status = "";
		String elemType = "";
		
		// getting node id
		if(!GeneralUtil.isNullOrEmpty(row.getCell(0))){
			nodeId = GeneralUtil.getCellValue(row.getCell(0));			
		}
		else{
			columnNumErrorCreate = 1;			
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Node Id");
			return false;
		}
		
		// getting node name
		if(!GeneralUtil.isNullOrEmpty(row.getCell(1))){
			nodeName = GeneralUtil.getCellValue(row.getCell(1));
		}
		else{
			columnNumErrorCreate = 2;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Node Name");
			return false;
		}
		
		// getting node cluster		
		if(!GeneralUtil.isNullOrEmpty(row.getCell(2))){
			cluster = GeneralUtil.getCellValue(row.getCell(2));
		}
		else{
			columnNumErrorCreate = 3;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Node Cluster");
			return false;
		}
		
		// getting node status
		if(!GeneralUtil.isNullOrEmpty(row.getCell(3))){
			status = GeneralUtil.getCellValue(row.getCell(3));
		}
		else{
			columnNumErrorCreate = 4;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Node Status");
			return false;
		}
		if(!(status.equalsIgnoreCase(BPOConstants.OPEN) || status.equalsIgnoreCase(BPOConstants.CLOSE))){
			columnNumErrorCreate = 4;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Node Status, choose OPEN or CLOSE");
			return false;
		}
		// getting node elem type
		if(!GeneralUtil.isNullOrEmpty(row.getCell(4))){
			elemType = GeneralUtil.getCellValue(row.getCell(4));
		}
		else{
			columnNumErrorCreate = 5;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty ELement Type");
			return false;
		}
		
		// getting duration limits
		long aveWaitDurLimit = 0L;
		long aveProcDurLimit = 0L;
		if(!GeneralUtil.isNullOrEmpty(row.getCell(5))){
			try {				
				 aveWaitDurLimit = Long.parseLong(GeneralUtil.getCellValue(row.getCell(5)));	
		     } 
			 catch (NumberFormatException nfe) {
				 columnNumErrorCreate=6;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Node Average Waiting Duration Limit");
				 return false;
		     }
		}
		 
		if(!GeneralUtil.isNullOrEmpty(row.getCell(6))){
			 try {			
				 aveProcDurLimit =  Long.parseLong(GeneralUtil.getCellValue(row.getCell(6)));				
		     } 
			 catch (NumberFormatException nfe) {
				 columnNumErrorCreate=7;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Node Average Processing Duration Limit");
				 return false;
		     }
		}
		
		
		
		// getting production output units
		Map<String, ProductionOutputUnit> productionOutputUnits = new HashMap<String, ProductionOutputUnit>();
		ProductionOutputUnit prod = null;					
		
		for(int k=0; k<5 ;k++){
			boolean isValidLabel = true;
			boolean isValidUOM = true;
			boolean isValidCost = true;
			String label = "";
			String measurementUnit = "";		
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(6+(3*k)+1))){
				label = GeneralUtil.getCellValue(row.getCell(6+(3*k)+1));
			}
			else{
				isValidLabel = false;
			}
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(6+(3*k)+2))){
				measurementUnit  = GeneralUtil.getCellValue(row.getCell(6+(3*k)+2));
			}
			else{
				isValidUOM  = false;
			}
			
			 double cost = 0;
			 try {							
				 if(!GeneralUtil.isNullOrEmpty(row.getCell(6+(3*k)+3))){
					 cost = Double.parseDouble(GeneralUtil.getCellValue(row.getCell(6+(3*k)+3))); 
				 }else{
					 isValidCost = false;
				 }
		     } 
			 catch (NumberFormatException nfe) {
				 columnNumErrorCreate=6+(3*k)+4;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Node Production Cost Value");				
				 return false;
		     }			 
 			// skip blank cells		 
			 if((!isValidLabel) && (!isValidUOM)  && (GeneralUtil.isNullOrEmpty(row.getCell(6+(3*k)+3))) ){
				 continue;
			 }
			 if(!isValidLabel){
				 columnNumErrorCreate=6+(3*k)+2;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Production Label");
				 return false;
			 }		
			 if(!isValidUOM){
				 columnNumErrorCreate=6+(3*k)+3;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Empty Production UOM");
				 return false;
			 }	
			 if(!isValidCost){
				 columnNumErrorCreate=6+(3*k)+4;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Invalid Node Production Cost Value");
				 return false;
			 }	
			 
			 // creating new production output unit
			 prod = new ProductionOutputUnit(measurementUnit, cost);
			 // adding in the list
			 if(!productionOutputUnits.containsKey(label)){
				 productionOutputUnits.put(label, prod);
			 }
			 else{
				 columnNumErrorCreate=6+(3*k)+2;
				 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Duplicate Production Label");
				 return false;
			 }			
		}
		
		// checks if there is at least one production output unit saved
		if(productionOutputUnits.size() == 0){
			columnNumErrorCreate = 8;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "No Production Output Defined");
			return false;
		}
		
		//22
		int servicesStartIndex = 22;
		ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun = new ConcurrentHashMap<TriggerType, List<ServiceObj>>();;
		while(!GeneralUtil.isNullOrEmpty(row.getCell(servicesStartIndex))){
			String serviceType = "";
			String parameter = "";
			String servicePath = "";
			String requestType = "";
			String triggerType = GeneralUtil.getCellValue(row.getCell(servicesStartIndex));
			
			System.out.println("trigger: "+ triggerType);
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(++servicesStartIndex))){
				servicePath = GeneralUtil.getCellValue(row.getCell(servicesStartIndex));
			}else{
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "No Service Path Defined");
				return false;
			}
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(++servicesStartIndex))){
				serviceType = GeneralUtil.getCellValue(row.getCell(servicesStartIndex));
			}else{
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "No Service Type Defined");
				return false;
			}
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(++servicesStartIndex))){
				requestType = GeneralUtil.getCellValue(row.getCell(servicesStartIndex));
			}else{
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "No Request Type Defined");
				return false;
			}
			
			if(!GeneralUtil.isNullOrEmpty(row.getCell(++servicesStartIndex))){
				System.out.println();
				parameter = GeneralUtil.getCellValue(row.getCell(servicesStartIndex));
			}
			
			ServiceObj serviceObj = new ServiceObj(servicePath, ServiceType.valueOf(serviceType), ReqType.valueOf(requestType), "");
			servicesToRun.computeIfAbsent(TriggerType.valueOf(triggerType), k -> new ArrayList<ServiceObj>()).add(serviceObj);
			servicesStartIndex++;
		}
		
		
		// inserting the new node in the repository
		if(NodeRepository.addNode(
					nodeId,
					new Node(nodeId, nodeName, cluster, status, productionOutputUnits, elemType, aveWaitDurLimit, aveProcDurLimit, servicesToRun))){
			return true;
		} else {			
			columnNumErrorCreate = 1;
			 errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorCreate, "Node Id Already Exists");
			return false;
		}
		
	}
	
	/**
	 * Updates the node with node id as indicated in the row.
	 * <p>
	 * @param row The row associated to the node to be updated.
	 * @return TRUE if update of node is successful. Otherwise, returns FALSE.
	 */
	private boolean updateNode(Row row, Map<String, String> errorMessages){		
		// getting basic info
		String nodeId = "";
		String nodeName = "";	
		String status = "";		
		// getting node id
		if(!GeneralUtil.isNullOrEmpty(row.getCell(0))){
			nodeId = GeneralUtil.getCellValue(row.getCell(0));			
		}
		else{
			columnNumErrorUpdate = 1;		
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Empty Node Id");
			return false;
		}
		// getting node name
		if(!GeneralUtil.isNullOrEmpty(row.getCell(1))){
			nodeName = GeneralUtil.getCellValue(row.getCell(1));
		}					
		// getting node status
		if(!GeneralUtil.isNullOrEmpty(row.getCell(3))){
			status = GeneralUtil.getCellValue(row.getCell(3));
		}
		if(status.trim().length() != 0){
			if(!(status.equalsIgnoreCase(BPOConstants.OPEN) || status.equalsIgnoreCase(BPOConstants.CLOSE))){
				columnNumErrorUpdate = 4;
				errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Invalid Node Status, choose OPEN or CLOSE");
				return false;
			}	
		}			
		// check if node id exists
		Node node = NodeRepository.getNode(nodeId);
		if(node != null){
			// updating node name
			if(nodeName.trim().length() != 0){
				node.setNodeName(nodeName);
			}			
			// updating node status
			if(status.trim().length() != 0){
				node.setStatus(status);	
			}				
			return true;
		}else{
			columnNumErrorUpdate = 1;
			errorMessages.put(""+(row.getRowNum()+1)+columnNumErrorUpdate, "Node Id Does not Exist");
			return false;
		}		
	}


}
