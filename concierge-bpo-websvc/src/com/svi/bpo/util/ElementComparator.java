package com.svi.bpo.util;

import java.util.Comparator;

import com.svi.bpo.core.Element;

public class ElementComparator {
	
	/**
	 * Defines the ordering of elements in the list. 
	 * This comparator orders elements by priority (int) in ascending order.
	 * Elements with equal priority will be ordered in the same order in which they were put into the queue
	 * using startWaitTime (long) attribute of the elements.
	 */
	public static Comparator<Element> byPriorityThenByStartWaitTime = new Comparator<Element>() {

		// in ascending order (i.e. highest priority is the last on the list)
		public int compare(Element element1, Element element2) {		
			int priorityElement1 = element1.getPriority();
			int priorityElement2 = element2.getPriority();
			
			int diff = priorityElement1 - priorityElement2;
			
			if(diff == 0){
				int diff2 = element1.getStartWaitTime().compareTo(element2.getStartWaitTime());
				diff2 *= -1;
				if(diff2 == 0){
					return element1.getElementId().compareTo(element2.getElementId());
				} else {
					return diff2;
				}
			} else {
				return diff;
			}
		}	
	};
	
	
	/**
	 * Defines the ordering of elements in the list. 
	 * This comparator orders elements by element ID (String) in ascending order. 
	 */
	public static Comparator<Element> byID = new Comparator<Element>() {
		// in ascending order 
		public int compare(Element element1, Element element2) {		
			String idElement1 = element1.getElementId().toUpperCase();
			String idElement2 = element2.getElementId().toUpperCase();
			
			return idElement1.compareTo(idElement2); 		//in ascending order
			//return idElement2.compareTo(idElement1);		//in descending order					
		}	
	};
			

	/**
	 * Defines the ordering of elements in a list. 
	 * This comparator orders elements by start processing time (long) in ascending order.
	 * Elements with equal start processing time will be ordered using
	 * element ID (String), in ascending order.
	 */
	public static Comparator<Element> byStartProcTimeThenByID = new Comparator<Element>() {
		// in ascending order 
		public int compare(Element element1, Element element2) {		
			Long startProcTimeElement1 = element1.getStartProcTime();
			Long startProcTimeElement2 = element2.getStartProcTime();
			
			int diff = startProcTimeElement1.compareTo(startProcTimeElement2);
			
			if( diff == 0) {	
				String idElement1 = element1.getElementId().toUpperCase();
				String idElement2 = element2.getElementId().toUpperCase();				
				return idElement1.compareTo(idElement2); 		//in ascending order
				//return idElement2.compareTo(idElement1);		//in descending order	
			} else {
				return diff;
			}
		}	
	};
	
}
