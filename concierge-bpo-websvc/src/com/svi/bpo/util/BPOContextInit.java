package com.svi.bpo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.lang3.StringEscapeUtils;
import org.ini4j.Wini;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.util.concurrent.Service;
import com.opencsv.CSVReader;
import com.svi.bpo.backup.Backup;
import com.svi.bpo.backup.BackupEnum;
import com.svi.bpo.baggagetag.BaggageTagBuffer;
import com.svi.bpo.baggagetag.BaggageTagEnum;
import com.svi.bpo.core.Element;
import com.svi.bpo.core.ElementRepository;
import com.svi.bpo.core.Node;
import com.svi.bpo.core.NodeRepository;
import com.svi.bpo.core.ProductionOutputUnit;
import com.svi.bpo.core.Worker;
import com.svi.bpo.core.WorkerRepository;
import com.svi.bpo.nodestats.NodeStat;
import com.svi.bpo.nodestats.NodeStatEnum;
import com.svi.bpo.workerdb.WorkerDBEnum;
import com.svi.bpo.workerdb.WorkerDBThread;
import com.svi.bpo.workerdb.WorkerDBThread.WorkerDBTransaction;
import com.svi.object.ReqType;
import com.svi.object.ServiceObj;
import com.svi.object.ServiceType;
import com.svi.object.TriggerType;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

@WebListener
public class BPOContextInit implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		BaggageTagBuffer.stopThread();
		NodeStat.stopThread();		
		
		Backup.stopThread();		
		
		LoggerContext loggerContext = (LoggerContext) LoggerFactory
				.getILoggerFactory();
		loggerContext.stop();
		// stop backupwriter thread
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String configPath = "/WEB-INF/config/config.ini";
		String logbackPath = "/WEB-INF/config/logback.xml";
		ServletContext context = sce.getServletContext();
		InputStream configStream = null;
		InputStream logbackStream = null;
		try {
			configStream = context.getResourceAsStream(configPath);
			logbackStream = context.getResourceAsStream(logbackPath);
			Wini ini = new Wini(configStream);
			NodeStatEnum.setContext(ini.get("NODE_STATS"));
			WorkerDBEnum.setContext(ini.get("WORKERS"));
			BaggageTagEnum.setContext(ini.get("BAGGAGE_TAGS"));
			BackupEnum.setContext(ini.get("BACKUP"));
			// store current system ms
			 restore();
			// get latest backup file
			// restore

			LoggerContext loggerContext = (LoggerContext) LoggerFactory
					.getILoggerFactory();
			loggerContext.reset();
			JoranConfigurator configurator = new JoranConfigurator();
			configurator.setContext(loggerContext);
			configurator.doConfigure(logbackStream);

			BaggageTagBuffer.startThread();
			NodeStat.startThread();			
			
			if(BackupEnum.BACKUP_ENABLED.getVal().trim().equalsIgnoreCase("Y")){
				System.out.println("Starting thread for backup...");
				Backup.startThread();
			}
			

			new WorkerDBThread(WorkerDBTransaction.READ).run();

			// start backupwriter thread
		} catch (IOException | JoranException e) {
			if (configStream != null) {
				try {
					configStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (configStream != null) {
				try {
					configStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

	}

	/***
	 * Restores the previous state of the nodes and elements base on the latest
	 * backup in the path in logback.xml
	 * ***/
	private void restore() {
		String nodeFolder = "node";
		String elementFolder = "element";

		// the string path use is in the restore is in config.ini
		// the string path use in back up can be found in logback xml
		// need to modify them if you modify one
		String nodePath = BackupEnum.BACKUP_DIR.getVal() + nodeFolder;
		String elementPath = BackupEnum.BACKUP_DIR.getVal() + elementFolder;

		File nodeFile = lastFileModifiedForNodes(nodePath);

		File elementFile = lastFileModifiedForElements(elementPath);

		if (nodeFile != null) {
//			System.out.println("nodeFile is not null!!");

//			List<String> nodeList = readFile(nodeFile);
			 List<String[]> nodeList = GeneralUtil.readCsvFile(nodeFile.getAbsolutePath());

			for (String[] node : nodeList) {
				// to avoid empty string lines
				if (node == null || node.length <= 17) {
					continue;
				}
				String[] nodeStr = node;
				int numBaseKey = 0;
				String nodeId = nodeStr[numBaseKey];// 1
				numBaseKey++;
				String nodeName = nodeStr[numBaseKey];// 2
				numBaseKey++;
				String cluster = nodeStr[numBaseKey];// 3
				numBaseKey++;
				String status = nodeStr[numBaseKey];// 4
				numBaseKey++;
				String elemType = nodeStr[numBaseKey];// 5
				numBaseKey++;
				String aveWaitLimit = nodeStr[numBaseKey];// 6
				numBaseKey++;
				String aveProcLimit = nodeStr[numBaseKey];// 7
				numBaseKey++;
				String totalWaitingElemCount = nodeStr[numBaseKey];// 8
				numBaseKey++;
				String totalProcessingElemCount = nodeStr[numBaseKey];// 9
				numBaseKey++;
				String totalCompletedElemCount = nodeStr[numBaseKey];// 10
				numBaseKey++;
				String totalExceptionElemCount = nodeStr[numBaseKey];// 11
				numBaseKey++;
				String minWaitDur = nodeStr[numBaseKey];// 12
				numBaseKey++;
				String aveWaitDur = nodeStr[numBaseKey];// 13
				numBaseKey++;
				String maxWaitDur = nodeStr[numBaseKey];// 14
				numBaseKey++;
				String minProcDur = nodeStr[numBaseKey];// 15
				numBaseKey++;
				String aveProcDur = nodeStr[numBaseKey];// 16
				numBaseKey++;
				String maxProcDur = nodeStr[numBaseKey];// 17
				numBaseKey++;

				Map<String, ProductionOutputUnit> map = new HashMap<String, ProductionOutputUnit>();

				Set<String> prodKeys = new HashSet<>(
							Arrays.asList(BaggageTagEnum.PROD_OUTPUT_UNIT_KEYS
							.getVal().split("\\|"))
						);

				// loop to find out how many prodOutputUnit Keys are configured
				// the keys used are
				for (String keys : prodKeys) {

					try {
						String key = nodeStr[numBaseKey];
						numBaseKey++;
						String measurementUnit = nodeStr[numBaseKey];
						numBaseKey++;
						String cost = nodeStr[numBaseKey];
						numBaseKey++;
						String outputUnit = nodeStr[numBaseKey];
						numBaseKey++;
						String errorUnit = nodeStr[numBaseKey];
						numBaseKey++;
						
						if(prodKeys.contains(key)){
							ProductionOutputUnit prodUnit = new ProductionOutputUnit(
									measurementUnit,
									Double.parseDouble(outputUnit),
									Double.parseDouble(errorUnit),
									Double.parseDouble(cost));
							map.put(key, prodUnit); 
						}
					} catch (IndexOutOfBoundsException e) { 
						// add empty
						ProductionOutputUnit prodUnit = new ProductionOutputUnit(
								"", 0.0, 0.0, 0.0);
						map.put(keys, prodUnit);
					}
				}
				
				//restore services
//				List<ServiceObj> services = new ArrayList<>();
//				while(nodeStr.length>numBaseKey){
//					String servicePath = nodeStr[numBaseKey++];
//					String serviceType = nodeStr[numBaseKey++];
//					String requestType = nodeStr[numBaseKey++];
////					String parameter = nodeStr[numBaseKey++];
//					System.out.println("narestore: "+servicePath+ " serviceType: "+serviceType + " requestType: " + requestType);
//					
//					
//					ServiceObj serviceObj = new ServiceObj(servicePath, ServiceType.valueOf(serviceType), ReqType.valueOf(requestType), "");
//					services.add(serviceObj); 
//				}
				
				ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun = new ConcurrentHashMap<TriggerType, List<ServiceObj>>();;
				
				while(nodeStr.length>numBaseKey){
					String triggerType = nodeStr[numBaseKey++];
					if(triggerType == null ||triggerType.isEmpty()){
						break;
					}
					String servicePath = nodeStr[numBaseKey++];
					String serviceType = nodeStr[numBaseKey++];
					String requestType = nodeStr[numBaseKey++];
					String parameter = nodeStr[numBaseKey++];
					System.out.println("narestore: "+servicePath+ " serviceType: "+serviceType + " requestType: " + requestType);
					
					
					ServiceObj serviceObj = new ServiceObj(servicePath, ServiceType.valueOf(serviceType), ReqType.valueOf(requestType), "");
					servicesToRun.computeIfAbsent(TriggerType.valueOf(triggerType), k -> new ArrayList<ServiceObj>()).add(serviceObj);
				}
				
				
				Node n = new Node(nodeId, nodeName, cluster, status, map,
						elemType, Long.parseLong(aveWaitLimit),
						Long.parseLong(aveProcLimit));

				n.setTotalWaitingElemCount(Integer
						.parseInt(totalWaitingElemCount));
				n.setTotalProcessingElemCount(Integer
						.parseInt(totalProcessingElemCount));
				n.setTotalExceptionCount(Integer
						.parseInt(totalExceptionElemCount));
				n.setTotalCompletedElemCount(Integer
						.parseInt(totalCompletedElemCount));
				n.setMinWaitDur(Long.parseLong(validateInt(minWaitDur)));
				n.setAveWaitDur(new BigDecimal(validateInt(aveWaitDur)));
				n.setMaxWaitDur(Long.parseLong(validateInt(maxWaitDur)));
				n.setMinProcDur(Long.parseLong(validateInt(minProcDur)));
				n.setAveProcDur(new BigDecimal(validateInt(aveProcDur)));
				n.setMaxProcDur(Long.parseLong(validateInt(maxProcDur)));
//				n.setServices(services);
				n.setServicesToRun(servicesToRun);
				NodeRepository.addNode(nodeId, n);
 
			}

		}

		if (elementFile != null) {
//			System.out.println("elementFile is not null!!");
//			List<String> elementList = readFile(elementFile);
			List<String[]> elementList = GeneralUtil.readCsvFile(elementFile.getAbsolutePath());

			for (String[] element : elementList) {
				if (element == null || element.length <= 10) {
					continue;
				}
				String[] elementStr = element;
				int numBaseKey = 0;
				String elemLoc = elementStr[numBaseKey];
				numBaseKey++;
				String elementId = elementStr[numBaseKey];
				numBaseKey++;
				String elementName = elementStr[numBaseKey];
				numBaseKey++;
				String nodeId = elementStr[numBaseKey];
				// get the node
				Node node = NodeRepository.getNode(nodeId);
				numBaseKey++;
				String workerId = elementStr[numBaseKey];
				numBaseKey++;
//				System.out.println("WORKERID: " + workerId);
				// check if worker already exist here
				if (!Strings.isNullOrEmpty(workerId)) {
//					System.out.println("WORKERID: " + workerId + " is new");
					if (WorkerRepository.getWorker(workerId) == null) {
						// worker doesnt exist add it!
						Worker w = new Worker(workerId);
						
						WorkerRepository.addWorker(workerId, w);
						// assign the worker to node

						if (node != null) {
							TreeSet<Integer> tree = new TreeSet<Integer>();
							tree.add(0);
							node.assignWorkerToNode(workerId, tree);
						}
					}
				}
				String fileLocation = elementStr[numBaseKey];
				numBaseKey++;
				String priority = elementStr[numBaseKey];
				numBaseKey++;
				String queueIndex = elementStr[numBaseKey];
				numBaseKey++;
				String status = elementStr[numBaseKey];
				numBaseKey++;
				String startProcTime = elementStr[numBaseKey];
				numBaseKey++;
				String startWaitTime = elementStr[numBaseKey];
				numBaseKey++;
				String endProcTime = elementStr[numBaseKey];
				numBaseKey++;
				String processingDur = elementStr[numBaseKey];
				numBaseKey++;
				String waitingDur = elementStr[numBaseKey];
				numBaseKey++;

				String[] extra_details = BaggageTagEnum.EXTRA_DETAIL_KEYS
						.getVal().split("\\|");

				Map<String, String> extraDetails = new HashMap<String, String>();
				// list of extra details
				for (String extra : extra_details) {
					try {
						String key = elementStr[numBaseKey];
						numBaseKey++;
						String value = elementStr[numBaseKey];
						numBaseKey++;
						extraDetails.put(key, value);
					} catch (IndexOutOfBoundsException e) {
						extraDetails.put(extra, "");
					}
				}

				Element elem = new Element(elementId, elementName, nodeId,
						Integer.parseInt(queueIndex),
						Integer.parseInt(priority), fileLocation, extraDetails);

				elem.setWaitingDuration(new BigDecimal(waitingDur).longValue());
				elem.setWorkerId(workerId);
				elem.setProcessingDuration(new BigDecimal(processingDur)
						.longValue());
				elem.setEndProcTime(new BigDecimal(endProcTime).longValue());
				elem.setStartWaitTime(new BigDecimal(startWaitTime).longValue());
				elem.setStartProcTime(new BigDecimal(startProcTime).longValue());
				// elem.setStatus(status,true);
				if (node != null) {
					if (elemLoc.equalsIgnoreCase("PROCESS")) {
						elem.setStatus(status, true);
//						System.out
//								.println("ADDING ELEM : elementID: "
//										+ elementId
//										+ ""
//										+ ElementRepository.addElement(
//												elementId, elem));
						ElementRepository.addElement(elementId, elem);
						node.addElemToInProcList(elementId, elem);
						Worker worker = WorkerRepository.getWorker(workerId);
						if (worker != null) {
							worker.addElement(elementId, elem);
						}

					} else if (elemLoc.equalsIgnoreCase("QUEUE")) {

						node.enqueueElement(elem, true, true, status);
					} else if (elemLoc.equalsIgnoreCase("RETURNED")) {
						elem.setStatus(status, true);
						node.addToReturnedElemList(elementId, elem);
//						System.out
//								.println("ADDING ELEM : elementID: "
//										+ elementId
//										+ ""
//										+ ElementRepository.addElement(
//												elementId, elem));
						ElementRepository.addElement(elementId, elem);
						Worker worker = WorkerRepository.getWorker(workerId);
						if (worker != null) {
							worker.addElement(elementId, elem);
						}
					}
				}

			}
		}
	}

	// if there is a blank or emty value for
	private static String validateInt(String str) {
		return Strings.isNullOrEmpty(str) ? "0" : StringEscapeUtils
				.escapeCsv(str);
	}

	/*****
	 * Retrieves the latest back up file Returns null if no file is found
	 * ****/
	public static File lastFileModifiedForNodes(String dir) {
		File fl = new File(dir);
		if (fl.listFiles() == null) {
			return null;
		}

		File[] files = fl.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		File choice = null;
		TreeSet<File> set = new TreeSet<>(
				NameFileComparator.NAME_INSENSITIVE_COMPARATOR);

		set.addAll(Arrays.asList(files));
/*
		if (set.size() > 0) {

			File file = set.pollLast();
			if (file != null) {
//				System.out.println("CHOSEN FILE: " + file.getAbsolutePath());
				choice = file;
			}
		}
*/		
		
		try {
			choice = getLatesFile(dir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(set.size() > 1){
			
		}

		return choice;
	}

	public static File lastFileModifiedForElements(String dir) {
		File fl = new File(dir);
		if (fl.listFiles() == null) {
			return null;
		}

		File[] files = fl.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isFile();
			}
		});

		File choice = null;
		TreeSet<File> set = new TreeSet<>(
				NameFileComparator.NAME_INSENSITIVE_COMPARATOR);

		set.addAll(Arrays.asList(files));

		if (set.size() > 0) {

			File file = set.pollLast();
			if (file != null) {
//				System.out.println("CHOSEN FILE: " + file.getAbsolutePath());
				choice = file;
			}
		}

		
		if(set.size() > 1){
			
		}

		return choice;
	}
	
	public static List<String> readFile(File file) {
		BufferedReader in;
		String str;
		List<String> list = new ArrayList<String>();
		try {
			in = new BufferedReader(new FileReader(file));

			while ((str = in.readLine()) != null) {
				list.add(str);
			}

			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	/**
	 * returns the latest file that are not blank or empty
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static File getLatesFile(String dir) throws IOException {
		File latestFile = null;
		File f = new File(dir);

		File[] files = f.listFiles();

		Arrays.sort(files, new Comparator() {
			public int compare(Object o1, Object o2) {

				if (((File) o1).lastModified() > ((File) o2).lastModified()) {
					return -1;
				} else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
					return +1;
				} else {
					return 0;
				}
			}

		});	
		
		for(File file : files) {
			List<String> elementList = readFile(file);
			for (String line : elementList) {
				if(!(line != null && (line.trim().equals("") || line.trim().equals("\n")))) {
					latestFile = file;
					return latestFile;
				}
			}
		}
		return latestFile;
	}

}
