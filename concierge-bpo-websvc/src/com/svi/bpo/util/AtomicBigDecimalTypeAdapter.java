package com.svi.bpo.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AtomicBigDecimalTypeAdapter extends
		TypeAdapter<AtomicReference<BigDecimal>> {

	@Override
	public AtomicReference<BigDecimal> read(JsonReader in) throws IOException {

		AtomicReference<BigDecimal> value = null;

		JsonParser jsonParser = new JsonParser();
		JsonElement je = jsonParser.parse(in);

		if (je instanceof JsonPrimitive) {
			value = new AtomicReference<BigDecimal>();
			value.set(new BigDecimal(((JsonPrimitive) je).getAsString()));
		} else if (je instanceof JsonObject) {
			JsonObject jsonObject = (JsonObject) je;
			value = new AtomicReference<BigDecimal>();
			value.set(new BigDecimal(jsonObject.get("value").getAsString()));
		}

		return value;
	}

	@Override
	public void write(JsonWriter out, AtomicReference<BigDecimal> value)
			throws IOException {
		if (value != null) {
			out.value(value.get());
		}
	}
}
