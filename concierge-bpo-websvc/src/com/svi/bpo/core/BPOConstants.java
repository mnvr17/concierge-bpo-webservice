package com.svi.bpo.core;

public class BPOConstants {
	public static final String BPO_BATCH_UPLOAD  =  "bpo-batch-upload";
	public static final String CLOSE  =  "CLOSE";	
	public static final String ELEMENT_ID  =  "elementId";	
	public static final String ELEMENT_NAME  =  "elementName";	
	public static final String EXTRA_DETAILS  =  "extraDetails";	
	public static final String FILE_LOC  =  "fileLocation";	
	public static final String NEXT_NODE_ID  =  "nextNodeId";	
	public static final String NODE_ID  =  "nodeId";
	public static final String OPEN  =  "OPEN";
	public static final String PRIORITY  =  "priority";
	public static final String PROD_OUTPUT_UNITS  =  "productionOutputUnits";	
	public static final String QUEUE_INDEX  =  "queueIndex";	
	public static final String WORKER_ID  =  "workerId";	
	public static final String XLS  =  "xls";
	public static final String STATUS  =  "status";
}
