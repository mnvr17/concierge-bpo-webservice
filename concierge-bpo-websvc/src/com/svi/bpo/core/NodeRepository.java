package com.svi.bpo.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;

import com.google.common.base.Strings;

public class NodeRepository {
	
// --------------------------------- VARIABLES ------------------------------------//
	private static ConcurrentSkipListMap<String, Node> nodes =  new ConcurrentSkipListMap<String, Node>(String.CASE_INSENSITIVE_ORDER);

// --------------------------------- OTHER METHODS ------------------------------------//
	/**
	 * Adds new node in the repository.
	 * <p>	
	 * @param key The key associated (to be mapped) to the new node.
	 * @param node The new node to be added.
	 * @return True, if it's not already in the repository. Otherwise, false.
	 * @throws IllegalArgumentException if the parameters invalid
	 */
	public static boolean addNode(String key, Node node) {
		if(!Strings.isNullOrEmpty(key) && (node != null)){
			return (nodes.putIfAbsent(key, node) == null)? true : false;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Removes the node from the repository.
	 * <p>
	 * @param key The key mapped to the node to be removed.
	 */
	public static Node removeNode(String key){
		Node node = nodes.get(key);
		if(node != null){
			for(Element e : node.getProcessingElements()){
				Worker w = WorkerRepository.getWorker(e.getWorkerId());
				w.removeElement(e.getElementId());
				
				long startProcTime = e.getStartProcTime();
				e.incProcessingDuration(System.currentTimeMillis() - startProcTime);
				
				e.setStatus("DELETED");
				ElementRepository.removeElement(e.getElementId());
			}
			for(Element e : node.getReturnedElements()){
				Worker w = WorkerRepository.getWorker(e.getWorkerId());
				w.removeElement(e.getElementId());
				
				long startWaitTime = e.getStartWaitTime();
				long waitingDuration = System.currentTimeMillis() - startWaitTime;
				if (e.getWaitingDuration() == 0L) {
					e.setWaitingDuration(waitingDuration);
					
				} else {
					e.incWaitingDuration(waitingDuration);
				}
				
				e.setStatus("DELETED");
				ElementRepository.removeElement(e.getElementId());
			}
			for(Element e : node.getWaitingElements()){
				long startWaitTime = e.getStartWaitTime();
				long waitingDuration = System.currentTimeMillis() - startWaitTime;
				if (e.getWaitingDuration() == 0L) {
					e.setWaitingDuration(waitingDuration);
					
				} else {
					e.incWaitingDuration(waitingDuration);
				}
				
				e.setStatus("DELETED");
				ElementRepository.removeElement(e.getElementId());
			}
		}
		return nodes.remove(key);		
	}
	
	/**
	 * Returns a node using the specified key (node ID).
	 * <p>
	 * @param nodeId The key mapped to the node to be retrieved.
	 * @return The node mapped to the given key.
	 */
	public static Node getNode(String nodeId){	
		return nodes.get(nodeId);		
	}
	
	
	/**
	 * Returns the list of the nodes in the repository.
	 * <p>
	 * @return List of nodes in the repository.
	 */
	public static List<Node> getAllNodes(){
		List<Node> allNodes = new ArrayList<Node>(nodes.values());
		return allNodes;
	}
		
	/**
	 * Removes ALL the nodes in the repository.
	 */
	public static void clear(){
		nodes.clear();
	}
	
	/**
	 * Checks if a given key (nodeId) exists in the map.
	 * <p>
	 * @param nodeId The ID of the node whose existence is to be tested.
	 * @return true if the key exists. Otherwise, false.
	 */
	public static boolean contains(String nodeId){
		return nodes.containsKey(nodeId);
	}
	
}
