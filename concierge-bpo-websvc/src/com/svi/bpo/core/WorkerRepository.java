package com.svi.bpo.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;

import com.google.common.base.Strings;

public class WorkerRepository {
	
// --------------------------------- VARIABLES ------------------------------------//
	private static ConcurrentSkipListMap<String, Worker> workers =  new ConcurrentSkipListMap<String, Worker>(String.CASE_INSENSITIVE_ORDER);

// --------------------------------- OTHER METHODS ------------------------------------//
	
	/**
	 * Adds new worker in the repository.
	 * <p>	
	 * @param key The key associated (to be mapped) to the new worker.
	 * @param worker The new worker to be added.
	 */
	public static boolean addWorker(String key, Worker worker){
		if(!Strings.isNullOrEmpty(key) && (worker != null)){
			boolean isSuccessful = (workers.putIfAbsent(key, worker) == null)? true : false;			
			return isSuccessful;
		}		
		else{
			throw new IllegalArgumentException();
		}
	}
		
	/** 
	 * Removes the worker from the repository.
	 * <p>
	 * @param key The key mapped to the worker to be removed.
	 */
	public static void removeWorker(String key){		
		workers.remove(key);
	}
	
	/**
	 * Returns a worker using the specified key (worker ID).
	 * <p>
	 * @param workerId The key mapped to the worker to be retrieved.
	 * @return The worker mapped to the given key.
	 */
	public static Worker getWorker(String workerId){	
		return workers.get(workerId);		
	}	

	/**
	 * Returns the list of the workers in the repository.
	 * <p>
	 * @return List of workers in the repository.
	 */
	public static List<Worker> getAllWorkers(){
		List<Worker> allWorkers = new ArrayList<Worker>(workers.values());
		return allWorkers;
	}
		
	/**
	 * Removes ALL the workers in the repository.
	 */
	public static void clear(){
		workers.clear();
	}
	
	/** 
	 * Checks if a given key (workerId) exists in the map.
	 * <p>
	 * @param workerId The ID of the worker whose existence is to be tested.
	 * @return true if the key exists. Otherwise, false.
	 */
	public static boolean contains(String workerId){
		return workers.containsKey(workerId);
	}
		
	
	
	


}
