package com.svi.bpo.core;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.svi.bpo.response.OperationResponseCode;
import com.svi.bpo.response.WorkerOperation;
import com.svi.bpo.response.WorkerOperationResponse;
import com.svi.bpo.util.AtomicBigDecimalTypeAdapter;
import com.svi.bpo.util.AtomicDoubleTypeAdapter;
import com.svi.bpo.util.AtomicIntegerTypeAdapter;
import com.svi.bpo.util.AtomicLongTypeAdapter;
import com.svi.bpo.util.AtomicStringTypeAdapter;

public class Worker {

	// --------------------------------- VARIABLES
	// ------------------------------------//
	@Expose
	private String workerId = new String();
	@Expose
	private String workerName = new String();

	private ConcurrentHashMap<String, Element> elements = new ConcurrentHashMap<String, Element>();

	@SuppressWarnings("serial")
	private static Gson gson = new GsonBuilder().registerTypeAdapter(new TypeToken<AtomicReference<String>>() {
	}.getType(), new AtomicStringTypeAdapter()).registerTypeAdapter(new TypeToken<AtomicReference<BigDecimal>>() {
	}.getType(), new AtomicBigDecimalTypeAdapter()).registerTypeAdapter(new TypeToken<AtomicLong>() {
	}.getType(), new AtomicLongTypeAdapter()).registerTypeAdapter(new TypeToken<AtomicInteger>() {
	}.getType(), new AtomicIntegerTypeAdapter()).registerTypeAdapter(new TypeToken<AtomicDouble>() {
	}.getType(), new AtomicDoubleTypeAdapter()).excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();

	// --------------------------------- CONSTRUCTORS
	// ------------------------------------//

	public Worker() {
	}

	public void addElement(String elementId, Element element) {

		elements.putIfAbsent(elementId, element);

	}

	/**
	 * Initializes the worker ID.
	 * <p>
	 * 
	 * @param workerId
	 *            The ID of the worker.
	 */
	public Worker(String workerId) {
		this.workerId = workerId;
	}

	public Worker(String workerId, String workerName) {
		this.workerId = workerId;
		this.workerName = workerName;
	}

	// --------------------------------- SETTERS AND GETTERS
	// ------------------------------------//

	/**
	 * Returns the worker ID.
	 * <p>
	 * 
	 * @return String workerId
	 */
	public String getWorkerId() {
		return workerId;
	}

	public String getWorkerName() {
		return workerName;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	// --------------------------------- OTHER METHODS
	// ------------------------------------//

	/**
	 * Performs the GET operation of BPO (queue only).
	 * <p>
	 * 
	 * @param nodeID
	 *            The ID of the node.
	 * @return The element to be processed by the worker.
	 */
	public WorkerOperationResponse getElement(String nodeId) {
		if (isParamsNullOrEmpty(nodeId)) {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key

			// getting the node
			Node node = getNodeFromRepository(nodeId);
			
			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {

				// dequeues and get an element from the enqueued elements in the
				// node (element of highest priority)
				Element dequeuedElement = node.dequeueElement(workerId);
				
				// check if the the element is not NULL
				if (dequeuedElement != null) {
					// sets the start processing time of the element
					dequeuedElement.setStartProcTime(System.currentTimeMillis());

					// sets the waiting duration of the element if it is zero
					// otherwise, add it to the previous waiting duration
					long startProcTime = dequeuedElement.getStartProcTime();
					long startWaitTime = dequeuedElement.getStartWaitTime();
					long waitingDuration = startProcTime - startWaitTime;
					if (dequeuedElement.getWaitingDuration() == 0L) {
						dequeuedElement.setWaitingDuration(waitingDuration);

					} else {
						dequeuedElement.incWaitingDuration(waitingDuration);
					}

					node.incTotalGetElement();
					incNodeWaitingMeasures(node, waitingDuration);

					// sets the worker ID property of the element
					dequeuedElement.setWorkerId(workerId);

					// sets the status of the element to PROCESSING
					dequeuedElement.setStatus("PROCESSING");
					node.incTotalProcessingElemCount();
					node.decTotalWaitingElemCount();

					// adds the elementId of the element to the elements list of
					// the worker
					elements.put(dequeuedElement.getElementId(), dequeuedElement);

					// adds the element to the in-process elements list of this
					// node
					node.addElemToInProcList(dequeuedElement.getElementId(), dequeuedElement);
					return new WorkerOperationResponse(true, WorkerOperation.GET, workerId, nodeId, dequeuedElement);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	/**
	 * Performs the GET operation of BPO (queue only).
	 * <p>
	 * 
	 * @param nodeID
	 *            The ID of the node.
	 * @param queueIndex
	 *            The specified queue (0-9).
	 * @return The element to be processed by the worker.
	 */
	public WorkerOperationResponse getElement(String nodeId, int queueIndex) {
		if (isParamsNullOrEmpty(nodeId)) {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key

			// getting the node
			Node node = getNodeFromRepository(nodeId);

			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {

				// dequeues and get an element from the enqueued elements in the
				// node (element of highest priority)
				Element dequeuedElement = node.dequeueElement(workerId, queueIndex);

				// check if the the element is not NULL
				if (dequeuedElement != null) {
					// sets the start processing time of the element
					dequeuedElement.setStartProcTime(System.currentTimeMillis());

					// sets the waiting duration of the element if it is zero
					// otherwise, add it to the previous waiting duration
					long startProcTime = dequeuedElement.getStartProcTime();
					long startWaitTime = dequeuedElement.getStartWaitTime();
					long waitingDuration = startProcTime - startWaitTime;
					if (dequeuedElement.getWaitingDuration() == 0L) {
						dequeuedElement.setWaitingDuration(waitingDuration);

					} else {
						dequeuedElement.incWaitingDuration(waitingDuration);
					}

					node.incTotalGetElement();
					incNodeWaitingMeasures(node, waitingDuration);

					// sets the worker ID property of the element
					dequeuedElement.setWorkerId(workerId);

					// sets the status of the element to PROCESSING
					dequeuedElement.setStatus("PROCESSING");
					node.incTotalProcessingElemCount();
					node.decTotalWaitingElemCount();

					// adds the elementId of the element to the elements list of
					// the worker
					elements.put(dequeuedElement.getElementId(), dequeuedElement);

					// adds the element to the in-process elements list of this
					// node
					node.addElemToInProcList(dequeuedElement.getElementId(), dequeuedElement);
					return new WorkerOperationResponse(true, WorkerOperation.GET, workerId, nodeId, dequeuedElement);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.OPERATION_FAILED.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	public WorkerOperationResponse getElement(String nodeId, int queueIndex, String elementId) {
		if (isParamsNullOrEmpty(nodeId)) {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key

			// getting the node
			Node node = getNodeFromRepository(nodeId);

			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {

				// dequeues the specified element
				Element dequeuedElement = node.dequeueElement(elementId, workerId, queueIndex);

				// check if the the element is not NULL
				if (dequeuedElement != null) {
					// sets the start processing time of the element
					dequeuedElement.setStartProcTime(System.currentTimeMillis());

					// sets the waiting duration of the element if it is zero
					// otherwise, add it to the previous waiting duration
					long startProcTime = dequeuedElement.getStartProcTime();
					long startWaitTime = dequeuedElement.getStartWaitTime();
					long waitingDuration = startProcTime - startWaitTime;
					if (dequeuedElement.getWaitingDuration() == 0L) {
						dequeuedElement.setWaitingDuration(waitingDuration);

					} else {
						dequeuedElement.incWaitingDuration(waitingDuration);
					}

					node.incTotalGetElement();
					incNodeWaitingMeasures(node, waitingDuration);

					// sets the worker ID property of the element
					dequeuedElement.setWorkerId(workerId);

					// sets the status of the element to PROCESSING
					dequeuedElement.setStatus("PROCESSING");
					node.incTotalProcessingElemCount();
					node.decTotalWaitingElemCount();

					// adds the elementId of the element to the elements list of
					// the worker
					elements.put(dequeuedElement.getElementId(), dequeuedElement);

					// adds the element to the in-process elements list of this
					// node
					node.addElemToInProcList(dequeuedElement.getElementId(), dequeuedElement);
					return new WorkerOperationResponse(true, WorkerOperation.GET, workerId, nodeId, dequeuedElement);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.GET, workerId, nodeId, "",
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	private void incNodeWaitingMeasures(Node node, long duration) {
		if (duration > node.getMaxWaitDur()){
			node.setMaxWaitDur(duration);
		}

		if (duration < node.getMinWaitDur() || node.getMinWaitDur() == 0) {
			node.setMinWaitDur(duration);
		}

		int n = node.getTotalGetElement();
		if (n == 0) {
			n = 1;
		}
		long oldAve = node.getAveWaitDur().longValue();
		long newAve = 0;

		newAve = oldAve * (n - 1) / n + duration / n;
		BigDecimal big = new BigDecimal(newAve);
		node.setAveWaitDur(big);

	}

	private void incNodeProcessingMeasures(Node node, long duration) {
		if (duration > node.getMaxProcDur()){
			node.setMaxProcDur(duration);
		}
		if (duration < node.getMinProcDur() || node.getMinProcDur() == 0) {
			node.setMinProcDur(duration);
		}

		int n = node.getTotalCompletedElemCount();
		if (n == 0) {
			n = 1;
		}

		long oldAve = node.getAveProcDur().longValue();
		long newAve = 0;

		newAve = oldAve * (n - 1) / n + duration / n;
		BigDecimal big = new BigDecimal(newAve);
		node.setAveProcDur(big);

	}

	/**
	 * Performs the GET operation of BPO (returned element list only)
	 * <p>
	 * 
	 * @param nodeId
	 *            The ID of the node.
	 * @param elementId
	 *            The ID of the element in the given node.
	 */
	public WorkerOperationResponse getReturnedElement(String nodeId, String elementId) {
		if (isParamsNullOrEmpty(nodeId, elementId)) {
			return new WorkerOperationResponse(false, WorkerOperation.GETRET, workerId, nodeId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key
			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {
				// getting the node
				Node node = getNodeFromRepository(nodeId);

				// removes and get an element from the returned list of elements
				// in the node
				Element returnedElement = node.removeFromReturnedElemList(elementId);

				// check if the the element is not NULL
				if (returnedElement != null) {
					// sets the start processing time of the element
					returnedElement.setStartProcTime(System.currentTimeMillis());

					// sets the waiting duration of the element if it is zero
					// otherwise, add it to the previous waiting duration
					long startProcTime = returnedElement.getStartProcTime();
					long startWaitTime = returnedElement.getStartWaitTime();
					long waitingDuration = startProcTime - startWaitTime;
					if (returnedElement.getWaitingDuration() == 0L) {
						returnedElement.setWaitingDuration(waitingDuration);
					} else {
						returnedElement.incWaitingDuration(waitingDuration);
					}

					incNodeWaitingMeasures(node, waitingDuration);

					// sets the status of the element to PROCESSING
					returnedElement.setStatus("PROCESSING");
					node.incTotalProcessingElemCount();
					node.decTotalWaitingElemCount();

					// adds the element to the in-process elements list of this
					// node
					node.addElemToInProcList(returnedElement.getElementId(), returnedElement);

					return new WorkerOperationResponse(true, WorkerOperation.GETRET, workerId, nodeId, returnedElement);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.GETRET, workerId, nodeId, elementId,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.GETRET, workerId, nodeId, elementId,
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.GETRET, workerId, nodeId, elementId,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	/**
	 * Performs the RETURN operation of BPO.
	 * <p>
	 * 
	 * @param nodeId
	 *            The ID of the node where the element is to be returned.
	 * @param elementId
	 *            The ID of the element to be returned.
	 */
	public WorkerOperationResponse returnElement(String nodeId, String elementId) {
		if (isParamsNullOrEmpty(nodeId, elementId)) {
			return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key
			if (!elements.containsKey(elementId)) {
				return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}

			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {
				// getting the node
				Node node = getNodeFromRepository(nodeId);

				// removes and get an element from the in-process list of
				// elements in the node
				Element inprocessElement = node.removeFromInProcElemList(elementId);

				// check if the element is not NULL)
				if (inprocessElement != null) {
					// increases the processing duration (current time �
					// startProcTime) of the element
					long startProcTime = inprocessElement.getStartProcTime();
					inprocessElement.incProcessingDuration(System.currentTimeMillis() - startProcTime);

					// sets the status of the element to WAITING
					inprocessElement.setStatus("WAITING");
					node.decTotalProcessingElemCount();
					node.incTotalWaitingElemCount();

					// resets the startProcTime to 0 and startWaitTime to
					// current time
					inprocessElement.setStartProcTime(0L);
					inprocessElement.setStartWaitTime(System.currentTimeMillis());

					// adds element to the returned elements list of the node
					node.addToReturnedElemList(inprocessElement.getElementId(), inprocessElement);

					return new WorkerOperationResponse(true, WorkerOperation.RETURN, workerId, nodeId, elementId);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	/**
	 * Performs the RETURN and DROP operation of BPO.
	 * <p>
	 * 
	 * @param nodeId
	 *            The ID of the node where the element is located.
	 * @param elementId
	 *            The ID of the element to be dropped.
	 */
	public WorkerOperationResponse returnElementAndDrop(String nodeId, String elementId, String json) {
		if (isParamsNullOrEmpty(nodeId, elementId, json)) {
			return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key
			if (!elements.containsKey(elementId)) {
				return new WorkerOperationResponse(false, WorkerOperation.RETURN, workerId, nodeId, elementId,
						OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
			}

			@SuppressWarnings("serial")
			Type type = new TypeToken<Map<String, ProductionOutputUnit>>() {
			}.getType();
			Map<String, ProductionOutputUnit> prods;

			try {
				prods = gson.fromJson(json, type);
			} catch (Exception e) {
				return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
						OperationResponseCode.INVALID_PARAMS.getCode());
			}

			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {
				// getting the node
				Node node = getNodeFromRepository(nodeId);

				if (node.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
					return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.NODE_IS_CLOSED.getCode());
				}

				// removes and get an element from the in-process list of
				// elements in the node
				Element inprocessElement = node.removeFromInProcElemList(elementId);
				
				// if not in processing list, get the element from returned element list
				if(inprocessElement == null){
					List<Element> elements = node.getReturnedElements(Arrays.asList(elementId));
					if(!elements.isEmpty()){
						inprocessElement = elements.get(0);
					}
				}				

				// check if the obtained element is not NULL
				if (inprocessElement != null) {

					// increases the processing duration (current time �
					// startProcTime) of the element
					long startProcTime = inprocessElement.getStartProcTime();
					inprocessElement.incProcessingDuration(System.currentTimeMillis() - startProcTime);

					// loop around the hashmaps
					// production OutputUnit
					Map<String, ProductionOutputUnit> nodeProds = node.getProductionOutputUnits();
					Map<String, ProductionOutputUnit> elemProds = inprocessElement.getProductionOutputUnits();

					for (Entry<String, ProductionOutputUnit> each : nodeProds.entrySet()) {
						String key = each.getKey();
						ProductionOutputUnit nodeProd = each.getValue();
						ProductionOutputUnit inputProd = prods.get(key);

						if (inputProd != null) {
							if (!elemProds.containsKey(key)) {
								inputProd.setMeasurementUnit(nodeProds.get(key).getMeasurementUnit());
								inputProd.setCost(nodeProds.get(key).getCost());
								inprocessElement.addProductionOutputUnit(key, inputProd);
							} else {
								inprocessElement.incProductionOutputUnit(key, inputProd);
							}

							nodeProd.incOutputCount(inputProd.getOutputCount());
							nodeProd.incErrorCount(inputProd.getErrorCount());
						}
					}

					// sets the status of the element to WAITING
					inprocessElement.setStatus("WAITING");
					node.decTotalProcessingElemCount();
					node.incTotalWaitingElemCount();

					// resets the startProcTime to 0 and startWaitTime to
					// current time
					inprocessElement.setStartProcTime(0L);
					inprocessElement.setStartWaitTime(System.currentTimeMillis());

					// resets the workerId of the element to empty
					inprocessElement.setWorkerId("");

					// adds element to the priority queue of the node
					node.enqueueElement(inprocessElement, false);

					// removes the element Id from the elements list of the
					// worker
					elements.remove(inprocessElement.getElementId());
					node.removeFromReturnedElemList(inprocessElement.getElementId());

					return new WorkerOperationResponse(true, WorkerOperation.RETDROP, workerId, nodeId, elementId);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.RETDROP, workerId, nodeId, elementId,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	/**
	 * Performs the COMPLETE operation of BPO. Moving the element to the next
	 * node.
	 * <p>
	 * 
	 * @param nodeId
	 *            The ID of the current node of the element.
	 * @param nextNodeId
	 *            The ID of the next node of the element.
	 * @param elementId
	 *            The ID of the element to be completed and to be moved to the
	 *            next node..
	 */
	public WorkerOperationResponse completeElementToNextNode(String nodeId, String nextNodeId, String elementId,
			Map<String, ProductionOutputUnit> prods, Map<String, String> extras) {
		if (isParamsNullOrEmpty(nodeId, nextNodeId, elementId)) {
			return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		}

		// checking if the nodes exist in the repository using node IDs as a key
		if (NodeRepository.contains(nodeId) && NodeRepository.contains(nextNodeId)) {
			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {
				// getting the node
				Node currentNode = getNodeFromRepository(nodeId);
				Node nextNode = getNodeFromRepository(nextNodeId);

				if (nextNode.getStatus().equalsIgnoreCase(BPOConstants.CLOSE)) {
					return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
							elementId, OperationResponseCode.NODE_IS_CLOSED.getCode());
				}

				// removes the element from the in-proc list of the node
				Element inprocessElement = currentNode.removeFromInProcElemList(elementId);

				// check if the the element is not NULL
				if (inprocessElement != null) {
					// sets the endProcTime of the element to current time in ms
					inprocessElement.setEndProcTime(System.currentTimeMillis());

					// increases the processing duration (endProcTime �
					// startProcTime) of the element
					long startProcTime = inprocessElement.getStartProcTime();
					long endProcTime = inprocessElement.getEndProcTime();
					long procDur = endProcTime - startProcTime;

					inprocessElement.incProcessingDuration(procDur);

					// loop arround the hashmaps
					// production OutputUnit
					Map<String, ProductionOutputUnit> nodeProds = currentNode.getProductionOutputUnits();
					Map<String, ProductionOutputUnit> elemProds = inprocessElement.getProductionOutputUnits();

					for (Entry<String, ProductionOutputUnit> each : nodeProds.entrySet()) {
						String key = each.getKey();
						ProductionOutputUnit nodeProd = each.getValue();
						ProductionOutputUnit inputProd = prods.get(key);

						if (inputProd != null) {
							if (!elemProds.containsKey(key)) {
								inputProd.setMeasurementUnit(nodeProds.get(key).getMeasurementUnit());
								inputProd.setCost(nodeProds.get(key).getCost());
								inprocessElement.addProductionOutputUnit(key, inputProd);
							} else {
								inprocessElement.incProductionOutputUnit(key, inputProd);
							}

							nodeProd.incOutputCount(inputProd.getOutputCount());
							nodeProd.incErrorCount(inputProd.getErrorCount());
						}
					}

					// sets the status of the element to COMPLETE
					inprocessElement.setStatus("COMPLETE");					
					currentNode.decTotalProcessingElemCount();
					currentNode.incTotalCompletedElemCount();
					incNodeProcessingMeasures(currentNode, procDur);

					// resets the startProcTime and endProcTime to 0, and
					// startWaitTime to current time
					inprocessElement.setStartProcTime(0L);
					inprocessElement.setEndProcTime(0L);
					inprocessElement.setStartWaitTime(System.currentTimeMillis());

					// resets the waiting duration and processing duration to 0
					inprocessElement.setWaitingDuration(0L);
					inprocessElement.setProcessingDuration(0L);

					// resets the workerId of the element to empty
					inprocessElement.setWorkerId("");

					// sets the nodeId of the element to nextNodeId
					inprocessElement.setNodeId(nextNodeId);

					// loop around the extra det
					for (Entry<String, String> each : extras.entrySet()) {
						String key = each.getKey();
						String extraVal = each.getValue();
						if (Strings.isNullOrEmpty(key) || extraVal == null) {
							continue;
						}

						if (!inprocessElement.getExtraDetails().containsKey(key)) {
							// inprocessElement.getExtraDetails().put(key,
							// extraVal);
							inprocessElement.addExtraDetail(key, extraVal);
						} else {
							// inprocessElement.getExtraDetails().put(key,extraVal);
							inprocessElement.updateExtraDetail(key, extraVal);
						}
					}

					// sets the status of the element to WAITING
					inprocessElement.setStatus("WAITING");

					inprocessElement.clearProductionOutputUnit();

					// enqueues the element to the next node, isNewElement flag
					// = false
					nextNode.enqueueElement(inprocessElement, false);
					nextNode.incTotalWaitingElemCount();

					// removes the element Id from the elements list of the
					// worker
					elements.remove(inprocessElement.getElementId());

//					return new WorkerOperationResponse(true, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
//							elementId);
					
					return new WorkerOperationResponse(true, WorkerOperation.COMPLETE, workerId, nodeId, inprocessElement);
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
							elementId, OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
						elementId, OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			if (!NodeRepository.contains(nodeId)) {
				return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
						elementId, OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
			} else {
				return new WorkerOperationResponse(false, WorkerOperation.COMPLETE, workerId, nodeId, nextNodeId,
						elementId, OperationResponseCode.NEXT_NODE_DOES_NOT_EXIST.getCode());
			}
		}
	}

	/**
	 * Performs the COMPLETE operation of BPO.
	 * <p>
	 * 
	 * @param nodeId
	 *            The ID of the current node of the element.
	 * @param elementId
	 *            The ID of the element to be completed.
	 */
	public WorkerOperationResponse completeElementToEnd(String nodeId, String elementId,
			Map<String, ProductionOutputUnit> prods, Map<String, String> extras) {
		if (isParamsNullOrEmpty(nodeId, elementId)) {
			return new WorkerOperationResponse(false, WorkerOperation.END, workerId, nodeId, elementId,
					OperationResponseCode.INVALID_PARAMS.getCode());
		} else if (NodeRepository.contains(nodeId)) { // checking if the node
														// exists using node ID
														// as a key
			// checks if the worker is assigned to the node
			if (isAssignedToNode(workerId, nodeId)) {
				// getting the node
				Node node = getNodeFromRepository(nodeId);

				// removes the element from the in-proc list of the node
				Element inprocessElement = node.removeFromInProcElemList(elementId);

				// check if the the element is not NULL
				if (inprocessElement != null) {
					ReentrantLock lock = inprocessElement.getLock();
					lock.lock();
					try {
						// sets the endProcTime of the element to current time
						// in ms
						inprocessElement.setEndProcTime(System.currentTimeMillis());

						// increases the processing duration (endProcTime �
						// startProcTime) of the element
						long startProcTime = inprocessElement.getStartProcTime();
						long endProcTime = inprocessElement.getEndProcTime();
						long procDur = endProcTime - startProcTime;

						inprocessElement.incProcessingDuration(procDur);

						// loop arround the hashmaps
						// production OutputUnit
						Map<String, ProductionOutputUnit> nodeProds = node.getProductionOutputUnits();
						Map<String, ProductionOutputUnit> elemProds = inprocessElement.getProductionOutputUnits();

						for (Entry<String, ProductionOutputUnit> each : nodeProds.entrySet()) {
							String key = each.getKey();
							ProductionOutputUnit nodeProd = each.getValue();
							ProductionOutputUnit inputProd = prods.get(key);

							if (inputProd != null) {
								if (!elemProds.containsKey(key)) {
									inputProd.setMeasurementUnit(nodeProds.get(key).getMeasurementUnit());
									inputProd.setCost(nodeProds.get(key).getCost());
									inprocessElement.addProductionOutputUnit(key, inputProd);
								} else {
									inprocessElement.incProductionOutputUnit(key, inputProd);
								}

								nodeProd.incOutputCount(inputProd.getOutputCount());
								nodeProd.incErrorCount(inputProd.getErrorCount());
							}
						}

						// loop around the extra det
						for (Entry<String, String> each : extras.entrySet()) {
							String key = each.getKey();
							String extraVal = each.getValue();
							if (Strings.isNullOrEmpty(key) || extraVal == null) {
								continue;
							}

							if (!inprocessElement.getExtraDetails().containsKey(key)) {
								// inprocessElement.getExtraDetails().put(key,
								// extraVal);
								inprocessElement.addExtraDetail(key, extraVal);
							} else {
								// inprocessElement.getExtraDetails().put(key,extraVal);
								inprocessElement.updateExtraDetail(key, extraVal);
							}
						}

						// sets the status of the element to COMPLETE
						inprocessElement.setStatus("COMPLETE");
						incNodeProcessingMeasures(node, procDur);
						node.decTotalProcessingElemCount();
						node.incTotalCompletedElemCount();
						// removes the element Id from the elements list of the
						// worker

						elements.remove(inprocessElement.getElementId());
						ElementRepository.removeElement(elementId);
						return new WorkerOperationResponse(true, WorkerOperation.END, workerId, nodeId, elementId);
					} finally {
						lock.unlock();
					}
				} else {
					return new WorkerOperationResponse(false, WorkerOperation.END, workerId, nodeId, elementId,
							OperationResponseCode.ELEMENT_DOES_NOT_EXIST.getCode());
				}

			} else {
				return new WorkerOperationResponse(false, WorkerOperation.END, workerId, nodeId, elementId,
						OperationResponseCode.WORKER_NOT_ASSIGNED_TO_NODE.getCode());
			}
		} else {
			return new WorkerOperationResponse(false, WorkerOperation.END, workerId, nodeId, elementId,
					OperationResponseCode.NODE_DOES_NOT_EXIST.getCode());
		}
	}

	public void removeElement(String elementId) {
		elements.remove(elementId);

	}
	// --------------------------------- AUXILLIARY METHODS
	// ------------------------------------//

	/**
	 * Checks if a given worker is assigned to a given node (by ID).
	 * <p>
	 * 
	 * @param workerId
	 *            The ID of the worker.
	 * @param nodeId
	 *            The ID of the node where existence of the worker is to be
	 *            tested.
	 * @return true if the worker is assigned to the node. Otherwise, false.
	 */
	private boolean isAssignedToNode(String workerId, String nodeId) {
		Node node = getNodeFromRepository(nodeId);
		if (node.workerExists(workerId)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the node from the node repository by using node ID.
	 * 
	 * @param nodeId
	 *            The ID of the node.
	 * @return he node from the node repository.
	 */
	private Node getNodeFromRepository(String nodeId) {
		return NodeRepository.getNode(nodeId);
	}

	/**
	 * Checker if parameters are empty string or null
	 * 
	 * @param params
	 *            The parameters
	 * @return Returns true if one parameter is null or empty
	 */
	private boolean isParamsNullOrEmpty(String... params) {
		for (String param : params) {
			if (Strings.isNullOrEmpty(param))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;
		result = prime * result + ((workerId == null) ? 0 : workerId.hashCode());
		return result;
	}

	public List<Element> getWorkload() {
		return new ArrayList<>(elements.values());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Worker other = (Worker) obj;
		if (workerId == null) {
			if (other.workerId != null)
				return false;
		} else if (!workerId.equals(other.workerId))
			return false;
		return true;
	}

}
