package com.svi.bpo.core;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import com.google.common.base.Strings;
import com.google.gson.annotations.Expose;
import com.svi.bpo.baggagetag.BaggageTag;
import com.svi.bpo.baggagetag.BaggageTagBuffer;

public class Element implements Comparable<Object>{

	// --------------------------------- VARIABLES
	// ------------------------------------//
	@Expose private String elementId = new String();
	@Expose private AtomicReference<String> elementName = new AtomicReference<String>();
	@Expose private AtomicReference<String> nodeId = new AtomicReference<String>();
	@Expose private AtomicInteger queueIndex = new AtomicInteger();
	@Expose private AtomicInteger priority = new AtomicInteger();
	@Expose private AtomicReference<String> fileLocation = new AtomicReference<String>();
	@Expose private ConcurrentHashMap<String, ProductionOutputUnit> productionOutputUnits = new ConcurrentHashMap<String, ProductionOutputUnit>();
	@Expose private ConcurrentHashMap<String, String> extraDetails = new ConcurrentHashMap<String, String>();
	@Expose private AtomicReference<String> status = new AtomicReference<String>();
	@Expose private AtomicReference<String> workerId = new AtomicReference<String>();
	@Expose private AtomicLong startWaitTime = new AtomicLong();
	@Expose private AtomicLong startProcTime = new AtomicLong();
	@Expose private AtomicLong endProcTime = new AtomicLong();
	@Expose private AtomicLong waitingDuration = new AtomicLong();
	@Expose private AtomicLong processingDuration = new AtomicLong();
	
	private final ReentrantLock lock = new ReentrantLock();

	// --------------------------------- CONSTRUCTORS
	// ------------------------------------//
	/**
	 * Creates an element with initialized values for attributes as listed
	 * below. This constructor does not allow NULL keys for the Map.
	 * <p>
	 * 
	 * @param elementId
	 *            The ID of this element.
	 * @param elementName
	 *            The name of this element.
	 * @param nodeId
	 *            The id of the node where the element is currently enqueued or
	 *            in-process.
	 * @param priority
	 *            The level of priority of this element ranging from 0 to 9 (9
	 *            as the highest).
	 * @param fileLocation
	 *            The referenced location of the content of the element in the
	 *            file system or GFS.
	 * @param productionOutputUnits
	 *            The production output and error count mapping for each unit of
	 *            measurement.
	 * @param extraDetails
	 *            Stores other details or information specific to this element.
	 */
	public Element(String elementId, String elementName, String nodeId,
			int queueIndex, int priority, String fileLocation,
			Map<String, String> extraDetails) {

		this.elementId = elementId;
		this.elementName.set(elementName);
		this.nodeId.set(nodeId);
		this.queueIndex.set(queueIndex);
		this.priority.set(priority);
		this.fileLocation.set(fileLocation);
		this.extraDetails.putAll(extraDetails);
	}

	public Element(String elementId){
		this.elementId = elementId;
	}
	// --------------------------------- SETTERS AND GETTERS
	// ------------------------------------//
	/**
	 * Returns the ID of the element.
	 * <p>
	 * 
	 * @return The element ID as a String.
	 */
	public String getElementId() {
		return elementId;
	}

	/**
	 * Returns the name of the element.
	 * <p>
	 * 
	 * @return String elementName
	 */
	public String getElementName() {
		return elementName.get();
	}

	/**
	 * Sets the name of the element.
	 * <p>
	 * 
	 * @param elementName
	 */
	public void setElementName(String elementName) {
		this.elementName.set(elementName);
	}

	/**
	 * Returns the id of the node where the element is currently enqueued or
	 * in-process.
	 * <p>
	 * 
	 * @return String nodeId
	 */
	public String getNodeId() {
		return nodeId.get();
	}

	/**
	 * Sets the id of the node where the element is currently enqueued or
	 * in-process.
	 * <p>
	 * 
	 * @param nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId.set(nodeId);
	}

	/**
	 * Returns the id of the worker working on this element.
	 * <p>
	 * 
	 * @return String workerId
	 */
	public String getWorkerId() {
		return workerId.get();
	}

	/**
	 * Sets the id of the worker working on this element.
	 * <p>
	 * 
	 * @param workerId
	 */
	public void setWorkerId(String workerId) {
		this.workerId.set(workerId);
	}
	
	/**
	 * Returns the index of the queue where the element is enqueued in the node
	 * ranging from 0 to 9 (9 as the highest).
	 * <p>
	 * 
	 * @return int queueIndex
	 */
	public int getQueueIndex() {
		return queueIndex.get();
	}
	
	/**
	 * Sets the index of the queue where the element is enqueued in the node
	 * ranging from 0 to 9 (9 as the highest).
	 * <p>
	 * 
	 * @param queueIndex
	 */
	public void setQueueIndex(int queueIndex) {
		this.queueIndex.set(queueIndex);
	}

	/**
	 * Returns the level of priority of the element ranging from 0 to 9 (9 as
	 * the highest).
	 * <p>
	 * 
	 * @return int priority
	 */
	public int getPriority() {
		return priority.get();
	}

	/**
	 * Sets the level of priority of the element ranging from 0 to 9 (9 as the
	 * highest).
	 * <p>
	 * 
	 * @param priority
	 */
	public void setPriority(int priority) {
		this.priority.set(priority);
	}

	/**
	 * Returns the referenced location of the content of the element in the file
	 * system or GFS.
	 * <p>
	 * 
	 * @return String fileLocation
	 */
	public String getFileLocation() {
		return fileLocation.get();
	}

	/**
	 * Sets the referenced location of the content of the element in the file
	 * system or GFS.
	 * <p>
	 * 
	 * @param fileLocation
	 */
	public void setFileLocation(String fileLocation) {
		this.fileLocation.set(fileLocation);
	}

	/**
	 * Returns the status of the element: either WAITING, PROCESSING or
	 * COMPLETE.
	 * <p>
	 * 
	 * @return String status
	 */
	public String getStatus() {
		return status.get();
	}

	/**
	 * Sets the status of the element: either WAITING, PROCESSING or COMPLETE.
	 * <p>
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status.set(status);
		// creates the baggage tag
		createBaggageTag();
	}

	/**
	 * Sets the status of the element: either WAITING, PROCESSING or COMPLETE.
	 * <p>
	 * 
	 * @param status
	 */
	public void setStatus(String status, boolean isFromBackUp) {
		this.status.set(status);
		// creates the baggage tag
		if(!isFromBackUp){
			createBaggageTag();
		}
	}
	/**
	 * Returns the timestamp when the element was enqueued in the node.
	 * <p>
	 * 
	 * @return Long startWaitTime
	 */
	public Long getStartWaitTime() {
		return startWaitTime.get();
	}

	/**
	 * Sets the timestamp when the element was enqueued in the node.
	 * <p>
	 * 
	 * @param startWaitTime
	 */
	public void setStartWaitTime(long startWaitTime) {
		this.startWaitTime.set(startWaitTime);
	}

	/**
	 * Returns the timestamp when work in the element was started.
	 * <p>
	 * 
	 * @return Long startProcTime
	 */
	public Long getStartProcTime() {
		return startProcTime.get();
	}

	/**
	 * Sets the timestamp when work in the element was started.
	 * <p>
	 * 
	 * @param startProcTime
	 */
	public void setStartProcTime(long startProcTime) {
		this.startProcTime.set(startProcTime);
	}

	/**
	 * Returns the timestamp when work in the element was completed
	 * <p>
	 * 
	 * @return Long endProcTime
	 */
	public Long getEndProcTime() {
		return endProcTime.get();
	}

	/**
	 * Sets the timestamp when work in the element was completed
	 * <p>
	 * 
	 * @param endProcTime
	 */
	public void setEndProcTime(long endProcTime) {
		this.endProcTime.set(endProcTime);
	}

	/**
	 * Returns the duration of time (milliseconds) the element was
	 * waiting/enqueued until being processed.
	 * <p>
	 * 
	 * @return Long waitingDuration
	 */
	public Long getWaitingDuration() {
		return waitingDuration.get();
	}

	/**
	 * Sets the duration of time (milliseconds) the element was waiting/enqueued
	 * until being processed.
	 * <p>
	 * 
	 * @param waitingDuration
	 */
	public void setWaitingDuration(long waitingDuration) {
		this.waitingDuration.set(waitingDuration);
	}

	/**
	 * Returns the duration of time (milliseconds) the element was in-process.
	 * <p>
	 * 
	 * @return Long processingDuration
	 */
	public Long getProcessingDuration() {
		return processingDuration.get();
	}

	/**
	 * Sets the duration of time (milliseconds) the element was in-process.
	 * <p>
	 * 
	 * @param processingDuration
	 */
	public void setProcessingDuration(long processingDuration) {
		this.processingDuration.set(processingDuration);
	}

	/**
	 * Returns the production output and error count mapping for each unit of
	 * measurement.
	 * <p>
	 * 
	 * @return Map productionOutputUnits
	 */
	public Map<String, ProductionOutputUnit> getProductionOutputUnits() {
		return new HashMap<String, ProductionOutputUnit>(productionOutputUnits);
	}

	/**
	 * Returns the other details specific to this element.
	 * <p>
	 * 
	 * @return Map extraDetails
	 */
	public Map<String, String> getExtraDetails() {
		return new HashMap<String, String>(extraDetails);
	}

	// --------------------------------- OTHER METHODS
	// ------------------------------------//
	/**
	 * Adds a production output unit. Associates the specified value with the
	 * specified label in this map. If the map previously contained a mapping
	 * for the label, the old value is replaced by the specified value. Does not
	 * allow a NULL key and value.
	 * <p>
	 * 
	 * @param label
	 *            The label with which the specified value is to be associated.
	 * @param productionOutputUnit
	 *            The productionOutputUnit to be associated with the specified
	 *            label.
	 */
	public void addProductionOutputUnit(String label,
			ProductionOutputUnit productionOutputUnit) {
		if ((productionOutputUnit != null) && (label != null)) {
			productionOutputUnits.putIfAbsent(label, productionOutputUnit);
		}
	}
	/**
	 * Increment the errorCount and outputcount.
	 * <p>
	 * 
	 * @param label
	 *            The label with which the specified value is to be associated.
	 * @param productionOutputUnit
	 *            The productionOutputUnit to be associated with the specified
	 *            label.
	 */
	public void incProductionOutputUnit(String label,ProductionOutputUnit productionOutputUnit){
		double out = productionOutputUnit.getOutputCount();
		double error = productionOutputUnit.getErrorCount();
		if ((productionOutputUnit != null) && (label != null)) {
			ProductionOutputUnit prod=productionOutputUnits.get(label);
			prod.incOutputCount(out);
			prod.incErrorCount(error);
		}
	}
	/**
	 * Returns the production unit to which the specified label is mapped, or
	 * null if this map contains no mapping for the label.
	 * <p>
	 * 
	 * @param label
	 *            The label whose associated value is to be returned.
	 * @return The production unit to which the specified label is mapped, or
	 *         null if this map contains no mapping for the label.
	 */
	public ProductionOutputUnit getProductionOutputUnit(String label) {
		if (label == null) {
			return null;
		}
		return productionOutputUnits.get(label);
	}

	/**
	 * Returns true if this map contains a mapping for the specified label. If
	 * the key is null, it returns false.
	 * 
	 * @param label
	 *            The label whose presence in this map is to be tested.
	 * @return true if this map contains a mapping for the specified label.
	 *         Otherwise, false.
	 */
	public boolean productionOutputUnitExists(String label) {
		if (label == null) {
			return false;
		}
		return productionOutputUnits.containsKey(label);
	}

	/**
	 * Removes the mapping for a label from this map if it is present. Does not
	 * allow a NULL key.
	 * <p>
	 * 
	 * @param label
	 *            The label whose mapping is to be removed from the map.
	 */
	public void removeProductionOutputUnit(String label) {
		if (label != null) {
			productionOutputUnits.remove(label);
		}
	}

	/**
	 * Removes all of the mappings from this map. The map will be empty after
	 * this call returns.
	 */
	public void clearProductionOutputUnit() {
		productionOutputUnits.clear();
	}

	/**
	 * Associates the specified value with the specified key in this map. If the
	 * map previously contained a mapping for the key, the new mapping will not
	 * be added. Does not allow a NULL key and value.
	 * <p>
	 * 
	 * @param key
	 *            The key with which the specified value is to be associated.
	 * @param val
	 *            The value to be associated with the specified key.
	 */
	public void addExtraDetail(String key, String val) {
		if ((key != null) && (val != null)) {
			extraDetails.putIfAbsent(key, val);
		}
	}

	/**
	 * Returns the value to which the specified key is mapped, or null if this
	 * map contains no mapping for the key.
	 * <p>
	 * 
	 * @param key
	 *            The key whose associated value is to be returned. If the key
	 *            is null, this method returns false.
	 * @return The value to which the specified key is mapped, or null if this
	 *         map contains no mapping for the key
	 */
	public Object getExtraDetailValue(String key) {
		if (key == null) {
			return null;
		}
		return extraDetails.get(key);
	}

	/**
	 * Updates the value in this map with the specified key, if the key exists.
	 * Otherwise, no change will occur. Does not allow a NULL key and value.
	 * <p>
	 * 
	 * @param key
	 *            The key with which the specified value is to be updated.
	 * @param val
	 *            The new value to be associated with the specified key.
	 */
	public void updateExtraDetail(String key, String val) {
		if ((key != null) && (val != null)) {
			if (extraDetails.containsKey(key)) {
				extraDetails.put(key, val);
			}
		}
	}

	/**
	 * Returns true if this map contains a mapping for the specified key. if key
	 * is null, it returns false.
	 * <p>
	 * 
	 * @param key
	 *            The key whose presence in this map is to be tested.
	 * @return true if this map contains a mapping for the specified key.
	 *         Otherwise, false.
	 */
	public boolean extraDetailExists(String key) {
		if (key == null) {
			return false;
		}
		return extraDetails.containsKey(key);
	}

	/**
	 * Removes the mapping for a key from this map if it is present. Does not
	 * allow a NULL key.
	 * <p>
	 * 
	 * @param key
	 *            The key whose mapping is to be removed from the map.
	 */
	public void removeExtraDetail(String key) {
		if (key != null) {
			extraDetails.remove(key);
		}
	}

	/**
	 * Removes all of the mappings from this map. The map will be empty after
	 * this call returns.
	 */
	public void clearExtraDetails() {
		extraDetails.clear();
	}

	/**
	 * Increases the waiting duration of the element.
	 * <p>
	 * 
	 * @param waitDuration
	 *            Additional waiting duration of the element.
	 */
	public void incWaitingDuration(long waitDuration) {
		this.waitingDuration.addAndGet(waitDuration);
	}

	/**
	 * Decreases the waiting duration of the element.
	 * <p>
	 * 
	 * @param waitDuration
	 *            Waiting duration of the element to be subtracted.
	 */
	public void decWaitingDuration(long waitDuration) {
		this.waitingDuration.addAndGet(-1 * waitDuration);
	}

	/**
	 * Increases the processing duration of the element.
	 * <p>
	 * 
	 * @param procDuration
	 *            Additional processing duration of the element.
	 */
	public void incProcessingDuration(long procDuration) {
		this.processingDuration.addAndGet(procDuration);
	}

	/**
	 * Decreases the processing duration of the element.
	 * <p>
	 * 
	 * @param procDuration
	 *            Processing duration of the element to be subtracted.
	 */
	public void decProcessingDuration(long procDuration) {
		this.processingDuration.addAndGet(-1 * procDuration);
	}
	


	public void createBaggageTag() {
		BaggageTag baggageTag = new BaggageTag();
		Node node = NodeRepository.getNode(nodeId.get());

		// setting the cluster and element type of the the baggage tag
		if (node != null) {
			baggageTag.setCluster(node.getCluster());
			baggageTag.setElemType(node.getElemType());

			// setting other properties of the baggage tag
			// these are required property values when creating a new element
			baggageTag.setElementId(elementId);
			baggageTag.setElementName(elementName.get());
			baggageTag.setNodeId(nodeId.get());
			baggageTag.setWorkerId(workerId.get());
			int i = queueIndex.get();
			int p= priority.get();
			//baggageTag.setPriority(((i>0)?i+""+p : "0"+p));
			baggageTag.setPriority(String.format("%02d", i) + p);
			baggageTag.setProductionOutputUnits(new HashMap<String, ProductionOutputUnit>(productionOutputUnits));
			baggageTag.setExtraDetails(new HashMap<String, String>(extraDetails));

			// other properties are within a try-catch block
			// to cater new elements' null property values
			try {
				baggageTag.setElemStatus(status.get());
			} catch (Exception e) {
				baggageTag.setElemStatus("");
				// e.printStackTrace();
			}

			try {
				baggageTag.setStartWaitTime(startWaitTime.get());
			} catch (Exception e) {
				baggageTag.setStartWaitTime(0);
				// e.printStackTrace();
			}
			try {
				baggageTag.setStartProcTime(startProcTime.get());
			} catch (Exception e) {
				baggageTag.setStartProcTime(0);
				// e.printStackTrace();
			}
			try {
				baggageTag.setEndProcTime(endProcTime.get());
			} catch (Exception e) {
				baggageTag.setEndProcTime(0);
				// e.printStackTrace();
			}
			try {
				baggageTag.setWaitingDuration(waitingDuration.get());
			} catch (Exception e) {
				baggageTag.setWaitingDuration(0);
				// e.printStackTrace();
			}
			try {
				baggageTag.setProcessingDuration(processingDuration.get());
			} catch (Exception e) {
				baggageTag.setProcessingDuration(0);
				// e.printStackTrace();
			}

			// adding the baggage tag in the baggage tag buffer
			BaggageTagBuffer.enqueueBaggageTag(baggageTag);
		}
	}
	
	public ReentrantLock getLock() {
		return lock;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((elementId == null) ? 0 : elementId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (elementId == null) {
			if (other.elementId != null)
				return false;
		} else if (!elementId.equals(other.elementId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Element ["
				+ "elementId=" + elementId 
				+ ", startWaitTime=" + startWaitTime +
				
				"]";
	}

	@Override
	public int compareTo(Object o) {
		if(o == null){
			return -1;
		} else if (getClass() != o.getClass()){
			return -1;
		} else if (Strings.isNullOrEmpty(((Element)o).elementId)){
			return -1;
		} else {
			return elementId.compareTo(((Element)o).elementId);
		}
	}
	
	
}
