package com.svi.bpo.core;

import java.util.concurrent.ConcurrentSkipListMap;

import com.google.common.base.Strings;

/**
 * This class provides a utility to ensure that all elements in the current snapshot of the workflow are unique
 * @author RAVFuentes
 * @since 09 Jan 2017
 */
public class ElementRepository {
	
	private static ConcurrentSkipListMap<String, Element> elements = new ConcurrentSkipListMap<>(String.CASE_INSENSITIVE_ORDER);
	
	/**
	 * Adds the element ID to the elements list
	 * @param elementId The elementId
	 * @return True, if successful
	 */
	public static boolean addElement(String elementId, Element elem){
		if(!Strings.isNullOrEmpty(elementId)){
			return (elements.putIfAbsent(elementId, elem) == null) ? true : false;
		} else {
			return false;
		}
	}
	
	/**
	 * Removes the element ID from the elements list
	 * @param elementId The elementId
	 * @return True, if successful
	 */
	public static Element removeElement(String elementId){
		if(!Strings.isNullOrEmpty(elementId)){
			return elements.remove(elementId);
		}
		return null;
	}
	
	/**
	 * Checks if the element ID is already in the elements list
	 * @param elementId The elementId
	 * @return True, if successful
	 */
	public static boolean contains(String elementId){
		if(!Strings.isNullOrEmpty(elementId)){
			return elements.containsKey(elementId);
		} else {
			return false;
		}
	}
	
	public static void clear(){
		elements.clear();
	}
	
	/**
	 * Returns an element using the specified key (element ID).
	 * <p>
	 * @param elementId The key mapped to the element to be retrieved.
	 * @return The element mapped to the given key.
	 */
	public static Element getElement(String elementId){	
		return elements.get(elementId);		
	}
}
