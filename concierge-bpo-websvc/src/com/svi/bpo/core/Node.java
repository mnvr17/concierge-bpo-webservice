package com.svi.bpo.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Strings;
import com.google.gson.annotations.Expose;
import com.svi.bpo.util.ElementComparator;
import com.svi.object.ServiceObj;
import com.svi.object.TriggerType;

@JsonIgnoreProperties(value = { "elemPriorityQueue","inProcElemList","returnedElemList","workerList","totalGetElement" })
public class Node {
	
// --------------------------------- VARIABLES ------------------------------------//
	@Expose private String nodeId = new String();


	@Expose private AtomicReference<String> nodeName = new AtomicReference<String>();
	@Expose private AtomicReference<String> cluster = new AtomicReference<String>();
	@Expose private AtomicReference<String> status = new AtomicReference<String>();
	@Expose private ConcurrentHashMap<String, ProductionOutputUnit> productionOutputUnits = new ConcurrentHashMap<String, ProductionOutputUnit>();
	@Expose private AtomicReference<String> elemType = new AtomicReference<String>();
	@Expose private AtomicLong aveWaitDurLimit = new AtomicLong(); 
	@Expose private AtomicLong aveProcDurLimit = new AtomicLong(); 
	@Expose private AtomicInteger totalWaitingElemCount = new AtomicInteger();
	@Expose private AtomicInteger totalProcessingElemCount = new AtomicInteger();
	@Expose private AtomicInteger totalCompletedElemCount = new AtomicInteger();
	@Expose private AtomicInteger totalExceptionCount = new AtomicInteger();

	private List<ConcurrentSkipListSet<Element>> elemPriorityQueue = new ArrayList<ConcurrentSkipListSet<Element>>();
	private ConcurrentHashMap<String, Element> inProcElemList = new ConcurrentHashMap<String, Element>();
	private ConcurrentHashMap<String, Element> returnedElemList = new ConcurrentHashMap<String, Element>();
	private ConcurrentHashMap<String, TreeSet<Integer>> workerList = new ConcurrentHashMap<String, TreeSet<Integer>>();
	@Expose private AtomicReference<BigDecimal> aveWaitDur = new AtomicReference<BigDecimal>(new BigDecimal(0));
	@Expose private AtomicReference<BigDecimal> aveProcDur = new AtomicReference<BigDecimal>(new BigDecimal(0));
	@Expose private AtomicLong minWaitDur = new AtomicLong(); 
	@Expose private AtomicLong maxWaitDur = new AtomicLong();
	@Expose private AtomicLong minProcDur  = new AtomicLong();
	@Expose private AtomicLong maxProcDur = new AtomicLong();
	private AtomicInteger totalGetElement = new AtomicInteger();
	
	@Expose private ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun = new ConcurrentHashMap<TriggerType, List<ServiceObj>>();;
	
// --------------------------------- CONSTRUCTORS ------------------------------------//
	
	/**
	 * Creates a node with initialized values for attributes as listed below.
	 * This constructor does not allow NULL keys for the Map.
	 * <p>
	 * @param nodeId The ID of this node.
	 * @param nodeName The name of this node.
	 * @param cluster The cluster of this node.
	 * @param productionOutputUnits The production output and error count mapping for each unit of measurement.
	 * @param elemType The standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * @param aveWaitDurLimit The set limit of average waiting duration of the elements in the node which can be used for notifications and flags.
	 * @param aveProcDurLimit The set limit of average processing duration of the elements in the node which can be used for notifications and flags.
	 */
	public Node(String nodeId, String nodeName, String cluster, String status, Map<String, ProductionOutputUnit> productionOutputUnits, 
			String elemType, long aveWaitDurLimit, long aveProcDurLimit){
		this.nodeId = nodeId;
		this.nodeName.set(nodeName);
		this.cluster.set(cluster);
		this.status.set(status);
		this.productionOutputUnits.putAll(productionOutputUnits);
		this.elemType.set(elemType);
		this.aveWaitDurLimit.set(aveWaitDurLimit);
		this.aveProcDurLimit.set(aveProcDurLimit);
		
		for(int i = 0; i < 100; i++){
			elemPriorityQueue.add(new ConcurrentSkipListSet<Element> (ElementComparator.byPriorityThenByStartWaitTime));
		}
	}
	
	/**
	 * Creates a node with initialized values for attributes as listed below.
	 * This constructor does not allow NULL keys for the Map.
	 * <p>
	 * @param nodeId The ID of this node.
	 * @param nodeName The name of this node.
	 * @param cluster The cluster of this node.
	 * @param productionOutputUnits The production output and error count mapping for each unit of measurement.
	 * @param elemType The standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * @param aveWaitDurLimit The set limit of average waiting duration of the elements in the node which can be used for notifications and flags.
	 * @param aveProcDurLimit The set limit of average processing duration of the elements in the node which can be used for notifications and flags.
	 */
	public Node(String nodeId, String nodeName, String cluster, String status, Map<String, ProductionOutputUnit> productionOutputUnits, 
			String elemType, long aveWaitDurLimit, long aveProcDurLimit, ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun){
		this.nodeId = nodeId;
		this.nodeName.set(nodeName);
		this.cluster.set(cluster);
		this.status.set(status);
		this.productionOutputUnits.putAll(productionOutputUnits);
		this.elemType.set(elemType);
		this.aveWaitDurLimit.set(aveWaitDurLimit);
		this.aveProcDurLimit.set(aveProcDurLimit);
		this.servicesToRun = servicesToRun;
		
		for(int i = 0; i < 100; i++){
			elemPriorityQueue.add(new ConcurrentSkipListSet<Element> (ElementComparator.byPriorityThenByStartWaitTime));
		}
	}
	
	
// --------------------------------- SETTERS AND GETTERS ------------------------------------//
	/**
	 * Returns the ID of the node.
	 * <p>
	 * @return String nodeId
	 */
	public String getNodeId() {
		return nodeId;
	}	

	/**
	 * Returns the name of the node.
	 * <p>
	 * @return String nodeName
	 */
	public String getNodeName() {
		return nodeName.get();
	}

	/**
	 * Sets the name of the node.
	 * <p>
	 *  @param nodeName
	 */
	public void setNodeName(String nodeName) {
		this.nodeName.set(nodeName);
	}

	/**
	 * Returns the cluster of the node.
	 * <p>
	 * @return String cluster
	 */
	public String getCluster() {
		return cluster.get();
	}

	/**
	 * Returns the status of the node (i.e. CLOSE or OPEN).
	 * <p>
	 * @return String status
	 */
	public String getStatus() {
		return status.get();
	}

	/**
	 * Sets the status of the node.
	 * <p>
	 *  @param status
	 */
	public void setStatus(String status) {
		this.status.set(status);
	}	

	/**
	 * Returns the production output and error count mapping for each unit of measurement.
	 * <p>
	 * @return Map productionOutputUnits
	 */
	public HashMap<String, ProductionOutputUnit> getProductionOutputUnits() {
		return new HashMap<String, ProductionOutputUnit>(productionOutputUnits);
	}

	/**
	 * Returns the standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * <p>
	 * @return String elemType
	 */
	public String getElemType() {
		return elemType.get();
	}
	
	/**
	 * Sets the standard unit of measure for the elements (e.g. 1 element = 1 application).
	 * <p>
	 *  @param elemType
	 */
	public void setElemType(String elemType) {
		this.elemType.set(elemType);
	}

	/**
	 * Returns the count of completed elements
	 * <p>
	 * @return int totalCompletedElemCount
	 */
	public int getTotalCompletedElemCount() {
		return totalCompletedElemCount.get();
	}
	
	/**
	 * Returns the limit of average waiting duration of the elements in the node which can be used for notifications and flags.
	 * <p>
	 * @return long aveWaitDurLimit
	 */
	public long getAveWaitDurLimit() {
		return aveWaitDurLimit.get();
	}

	/**
	 * Sets the limit of average waiting duration of the elements in the node which can be used for notifications and flags.
	 * <p>
	 *  @param aveWaitDurLimit
	 */
	public void setAveWaitDurLimit(long aveWaitDurLimit) {
		this.aveWaitDurLimit.set(aveWaitDurLimit);
	}
	
	/**
	 * Returns the limit of average processing duration of the elements in the node which can be used for notifications and flags.
	 * <p>
	 * @return long aveProcDurLimit
	 */
	public long getAveProcDurLimit() {
		return aveProcDurLimit.get();
	}
	
	/**
	 * Sets the limit of average processing duration of the elements in the node which can be used for notifications and flags.
	 * <p>
	 * @param aveProcDurLimit
	 */
	public void setAveProcDurLimit(long aveProcDurLimit) {
		this.aveProcDurLimit.set(aveProcDurLimit);
	}
			
	/**
	 * Returns the list of workers.
	 * <p>
	 * @return List workerList
	 */
	public Map<String, TreeSet<Integer>> getWorkers() {
		Map<String, TreeSet<Integer>> workers = new HashMap<String, TreeSet<Integer>>(workerList);
		return workers;
	}

	/**
	 * Returns the minimum waiting duration of elements.
	 * <p>
	 * @return long minWaitDur
	 */
	public long getMinWaitDur() {
		return minWaitDur.get();
	}

	/**
	 * Sets the minimum waiting duration of elements.
	 * <p>
	 *  @param minWaitDur
	 */
	public void setMinWaitDur(long minWaitDur) {
		this.minWaitDur.set(minWaitDur);
	}
	
	/**
	 * Returns the maximum waiting duration of elements.
	 * <p>
	 * @return long maxWaitDur
	 */
	public long getMaxWaitDur() {
		return maxWaitDur.get();
	}
	
	/**
	 * Sets the maximum waiting duration of elements.
	 * <p>
	 *  @param maxWaitDur
	 */
	public void setMaxWaitDur(long maxWaitDur) {
		this.maxWaitDur.set(maxWaitDur);
	}

	/**
	 * Returns the average waiting duration of elements.
	 * <p>
	 * @return BigDecimal aveWaitDur
	 */
	public BigDecimal getAveWaitDur() {
		return aveWaitDur.get();
	}
	
	/**
	 * Sets the average waiting duration of elements.
	 * <p>
	 *  @param aveWaitDur
	 */
	public void setAveWaitDur(BigDecimal aveWaitDur) {
		this.aveWaitDur.set(aveWaitDur);
	}

	/**
	 * Returns the minimum processing duration of elements.
	 * <p>
	 * @return long minProcDur
	 */
	public long getMinProcDur() {
		return minProcDur.get();
	}

	/**
	 * Sets the minimum processing duration of elements.
	 * <p>
	 *  @param minProcDur
	 */
	public void setMinProcDur(long minProcDur) {
		this.minProcDur.set(minProcDur);
	}

	/**
	 * Returns the maximum processing duration of elements.
	 * <p>
	 * @return long maxProcDur
	 */
	public long getMaxProcDur() {
		return maxProcDur.get();
	}

	/**
	 * Sets the maximum processing duration of elements.
	 * <p>
	 *  @param maxProcDur
	 */
	public void setMaxProcDur(long maxProcDur) {
		this.maxProcDur.set(maxProcDur);
	}

	/**
	 * Returns the average processing duration of elements.
	 * <p>
	 * @return BigDecimal aveProcDur
	 */
	public BigDecimal getAveProcDur() {
		return aveProcDur.get();
	}

	/**
	 * Sets the average processing duration of elements.
	 * <p>
	 *  @param aveProcDur
	 */	
	public void setAveProcDur(BigDecimal aveProcDur) {
		this.aveProcDur.set(aveProcDur);
	}

// --------------------------------- OTHER METHODS ------------------------------------//
	
	/**
	 * Returns the list of specified workers by ID.
	 * <p>
	 * @param workersId The list of worker IDs.
	 * @return  List of specified workers.
	 */
	public Map<String, TreeSet<Integer>> getWorkers(List<String> workersId) {
		// storing all elements in process in a list
		Map<String, TreeSet<Integer>> listOfRequestedWorkers = new HashMap<>();	
		if(workersId.size() != 0){
			// search for all requested workers by worker ID 
			for(String workerId : workersId){
				for(String worker : workerList.keySet()){
					if(worker.equals(workerId)){
						//avoid duplication
						if(!listOfRequestedWorkers.containsKey(worker)){
							listOfRequestedWorkers.put(worker, workerList.get(worker));
						}					
					}
				}
			}		
		}
		return listOfRequestedWorkers;
	}
	
	/**
	 * Check if the worker ID is already on the list.
	 * @param workerId
	 * @return true if the worker ID is on the list. Otherwise, false.
	 */
	public boolean workerExists(String workerId){
		if(workerList.containsKey(workerId)){
			return true;
		}
		return false;
	}
	
	/**
	 * Adds a worker to the worker list.
	 * Does not allow a NULL key and value.
	 * <p>
	 * @param worker The new worker to be added in the list.
	 */
	public boolean assignWorkerToNode(String workerId, TreeSet<Integer> queueIndices){
		if(!Strings.isNullOrEmpty(workerId) && queueIndices != null
				&& !queueIndices.isEmpty()
				&& queueIndices.first() >= 0
				&& queueIndices.last() <= 99){
			if(workerList.containsKey(workerId)){
				workerList.get(workerId).addAll(queueIndices);	
			} else {
				workerList.putIfAbsent(workerId, queueIndices);
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Removes a worker from the worker list.
	 * Does not allow a NULL key.
	 * <p>
	 * @param worker The worker to be removed.
	 * @return boolean 
	 */
	public boolean unassignWorkerFromNode(String key) {
		if(!Strings.isNullOrEmpty(key)){
			if(workerList.remove(key) == null){
				return false;
			}else{
				return true;
			}
		}	
		return false;
	}
	
	/**
	 * Removes a worker from the worker list.
	 * Does not allow a NULL key.
	 * <p>
	 * @param worker The worker to be removed.
	 * @return boolean 
	 */
	public boolean unassignWorkerFromNode(String key, TreeSet<Integer> queueIndices) {
		if(!Strings.isNullOrEmpty(key) && queueIndices != null){
			if(workerList.containsKey(key)){
				workerList.get(key).removeAll(queueIndices);
				if(workerList.get(key).isEmpty()){
					workerList.remove(key);
				}					
				return true;
			 }			
			else{
				return false;
			}
		}	
		return false;
	}
		
	/**
	 * Returns the specified production output unit.
	 * If the key is null, this method returns null.
	 * <p>
	 * @param label The label mapped to the production output unit to be retrieved.
	 * @return
	 */
	public ProductionOutputUnit getProductionOutputUnit(String label){
		if(label == null){
			return null;
		}
		return productionOutputUnits.get(label);
	}
	
	/**
	 * Removes a production output unit.
	 * Does not allow a null key.
	 * <p>
	 * @param label The label mapped to the production output unit to be removed.
	 */
	public void removeProductionOutputUnit(String label){
		if(label != null){
			productionOutputUnits.remove(label);
		}		
	}
	
	/**
	 * Removes all of the mappings from this map (optional operation). 
	 * The map will be empty after this call returns.
	 */
	public void clearProductionOutputUnit(){
		productionOutputUnits.clear();
	}
	
	/**
	 * Adds a production output unit if it does not exist yet.
	 * Does not allow a null key and value.
	 * <p>
	 * @param label The label with which the productionOutputUnit is to be associated.
	 * @param productionOutputUnit The value to be associated with the specified label.
	 */
	public void addProductionOutputUnit(String label, ProductionOutputUnit productionOutputUnit){
		if((label != null) && (productionOutputUnit != null)){			
			productionOutputUnits.putIfAbsent(label, productionOutputUnit);
		}		
	}
	
	/**
	 * Checks if the specified production output unit already exists.
	 * Returns false if the key is null.
	 * <p>
	 * @param label  The label whose presence in this map is to be tested.
	 * @return true if the label exists. Otherwise, false.
	 */
	public boolean productionOutputUnitExists(String label){
		if(label == null){
			return false;
		}
		return productionOutputUnits.containsKey(label);
	}
	
	/**
	 * Adds element to the in-process element list.
	 * Does not allow a null key and value.
	 * <p>
	 * @param element The new element to be added in the list.
	 */
	public void addElemToInProcList(String key, Element element){
		if((element != null) && (key != null)){
			inProcElemList.putIfAbsent(key,element);
		}		
	}
	
	/**
	 * Removes element from the in-process element list.
	 * Returns null if the elementId is null.
	 * <p>
	 * @param elementId The ID of the element to be removed from the list.
	 * @return Element The element removed from the list. 
	 */
	public Element removeFromInProcElemList(String elementId){
		if(elementId == null){
			return null;
		}
		return inProcElemList.remove(elementId);
	}
			
	public int getTotalWaitingElemCount(){
		return this.totalWaitingElemCount.get();
	}
	
	public void setTotalWaitingElemCount(int val) {
		this.totalWaitingElemCount.set(val);
	}
	
	/**
	 * Increases the count of waiting elements.
	 */
	public void incTotalWaitingElemCount(){
		totalWaitingElemCount.incrementAndGet();
	}
	
	/**
	 * Decreases the count of waiting elements.
	 */
	public void decTotalWaitingElemCount(){
		// avoid having negative value for the count
		if(totalWaitingElemCount.get() >0 ){
			totalWaitingElemCount.decrementAndGet();
		}		
	}
	
	public int getTotalProcessingElemCount(){
		return this.totalProcessingElemCount.get();
	}
	
	public void setTotalProcessingElemCount(int val) {
		this.totalProcessingElemCount.set(val);
	}
	
	/**
	 * Increases the count of processing elements.
	 */
	public void incTotalProcessingElemCount(){
		totalProcessingElemCount.incrementAndGet();
	}
	
	/**
	 * Decreases the count of processing elements.
	 */
	public void decTotalProcessingElemCount(){
		totalProcessingElemCount.decrementAndGet();
	}
	
	public int getTotalExceptionCount(){
		return this.totalExceptionCount.get();
	}
	
	public void setTotalExceptionCount(int totalExceptionCount) {
		this.totalExceptionCount.set(totalExceptionCount);
	}
	
	/**
	 * Increases the count of waiting elements.
	 */
	public void incTotalExceptionCount(){
		totalExceptionCount.incrementAndGet();
	}
	
	/**
	 * Increases the count of completed elements.
	 */
	public void incTotalCompletedElemCount(){
		totalCompletedElemCount.incrementAndGet();
	}
	
	/**
	 * set the count of completed elements.
	 */
	public void setTotalCompletedElemCount(int val){
		totalCompletedElemCount.set(val);
	}
	
	/**
	 * Decreases the count of completed elements.
	 */
	public void decTotalCompletedElemCount(){
		totalCompletedElemCount.decrementAndGet();
	}
	
	/**
	 * Resets the count of completed elements to zero.
	 */
	public void resetTotalCompletedElemCount(){
		totalCompletedElemCount.set(0);
	}
	
	/**
	 * Returns a list of all elements in a node.
	 * List is in ascending order by element ID.
	 * <p>
	 * @return A list of all elements in a node (waiting, in-process and returned).
	 */
	public List<Element> getElements(){
		List<Element> elements = new ArrayList<Element>();

		elements.addAll(new ArrayList<Element>(inProcElemList.values()));
//		
//		// adding all returned elements
		elements.addAll(new ArrayList<Element>(returnedElemList.values()));
//		
//		// getting all elements in queue
		for(ConcurrentSkipListSet<Element> waiting : elemPriorityQueue)
			elements.addAll(waiting);
//		
//		// sorting elements
		Collections.sort(elements,ElementComparator.byID);
		
		return elements;
	}
	
	/**
	 * Returns a list of specified  elements (enqueued, in-process and returned) using element ID).
	 * List is in ascending order by element ID.
	 * <p>
	 * @param elementsId The list of element IDs to be retrieved.
	 * @return A list of all specified elements in a node using element ID.
	 */
	public List<Element> getElements(List<String> elementsId){
		List<Element> requestedElements = new ArrayList<Element>();
		if(elementsId != null && elementsId.size() != 0){
			// gets all the elements (ordered)
			List<Element> allElements = new ArrayList<Element>(this.getElements());			
			
			// getting all requested elements
			requestedElements = getElementsById(elementsId,allElements);	
			
			// sorting elements
			Collections.sort(requestedElements,ElementComparator.byID);
		}
		
		return requestedElements;
	}
	
	/**
	 * Returns a list of enqueued and returned elements (ordered by priority and start wait time).
	 * <p>
	 * @return List of enqueued and returned elements.
	 */
	public List<Element> getWaitingElements(){
		// storing all elements in queue
		List<Element> waitingElements = new ArrayList<Element>();
		
		// adding returned elements
		for(ConcurrentSkipListSet<Element> waiting : elemPriorityQueue)
			waitingElements.addAll(waiting);
		
		// sorting the list by priority and then by start wait time
		Collections.sort(waitingElements,ElementComparator.byPriorityThenByStartWaitTime);
		Collections.reverse(waitingElements);
		
		return waitingElements;		
	}
	
	/**
	 * Returns a list of specified enqueued and returned elements by ID  (ordered by priority and start wait time)..
	 * <p>
	 * @param elementsId The list of element IDs to be retrieved.
	 * @return List of waiting elements by ID.
	 */
	public List<Element> getWaitingElements(List<String> elementsId){
		// instantiates the list to be returned
		List<Element> requestedEnqueuedElements = new ArrayList<Element>();
		if(elementsId.size() != 0){
			 // storing all elements in queue and returned (ordered)
			 List<Element> waitingElements = new ArrayList<Element>(this.getWaitingElements());	 
			
			 // getting all requested elements
			 requestedEnqueuedElements = getElementsById(elementsId,waitingElements);
			 
			// sorting the list by priority and then by start wait time
			Collections.sort(requestedEnqueuedElements,ElementComparator.byPriorityThenByStartWaitTime);
			Collections.reverse(requestedEnqueuedElements);
		}
		
		 return requestedEnqueuedElements;
	}
	
	public List<Element> getReturnedElements() {
		List<Element> returnedElements = new ArrayList<Element>(returnedElemList.values());
		Collections.sort(returnedElements,ElementComparator.byStartProcTimeThenByID);
		return returnedElements;
	}
	
	public List<Element> getReturnedElements(List<String> elementsId){	
		List<Element> returnedElements = new ArrayList<Element>();
		if(elementsId != null && elementsId.size() != 0){
			// storing all elements in process (ordered by start processing time then by ID)
			List<Element> inProcessElements = new ArrayList<Element>(this.getReturnedElements());
			
			// getting all requested elements
			returnedElements = getElementsById(elementsId,inProcessElements);
			Collections.sort(returnedElements ,ElementComparator.byStartProcTimeThenByID);
		}
		
		return returnedElements;		
	}
		
	/**
	 * Returns the list of in-process elements (ordered by start processing time and then by element ID).
	 * <p>
	 * @return List inProcElemList
	 */
	public List<Element> getProcessingElements() {
		List<Element> inProcessElements = new ArrayList<Element>(inProcElemList.values());
		Collections.sort(inProcessElements,ElementComparator.byStartProcTimeThenByID);
		return inProcessElements;
	}
		
	/**
	 * Returns a list of specified in-process elements by ID.
	 * <p>
	 * @param elementsId List of element IDs to be retrieved.
	 * @return List of specified in-process elements by ID.
	 */
	public List<Element> getProcessingElements(List<String> elementsId){	
		List<Element> requestedInProcessElements = new ArrayList<Element>();
		if(elementsId != null && elementsId.size() != 0){
			// storing all elements in process (ordered by start processing time then by ID)
			List<Element> inProcessElements = new ArrayList<Element>(this.getProcessingElements());
			
			// getting all requested elements
			requestedInProcessElements = getElementsById(elementsId,inProcessElements);
			Collections.sort(requestedInProcessElements ,ElementComparator.byStartProcTimeThenByID);
		}
		
		return requestedInProcessElements;		
	}
		
	/** 
	 * Enqueues an element to the priority queue.
	 * Does not allow a null element.
	 * @param element Element to be added in the waiting list.
	 * @param isNewElement true if the element is a new element to be added in the process.
	 * @return True, if successful. Otherwise, false.
	 */
	public boolean enqueueElement(Element element, boolean isNewElement){
		if(element != null){
			if(isNewElement){
				element.setStatus("WAITING");
				if(!ElementRepository.contains(element.getElementId())){
					element.setStartWaitTime(System.currentTimeMillis());					
					//element.setStatus("WAITING");
					incTotalWaitingElemCount();
					for (Element elem : elemPriorityQueue.get(element.getQueueIndex())) {
						if(elem.equals(element)){
							return false;
						}
					}
					ElementRepository.addElement(element.getElementId(),element);
				} 
				else {
					return false;
				}
			}
			
			element.clearProductionOutputUnit();
			for(Entry<String,ProductionOutputUnit> unit : productionOutputUnits.entrySet()){
				ProductionOutputUnit temp = new ProductionOutputUnit(unit.getValue().getMeasurementUnit(), unit.getValue().getCost());
				element.addProductionOutputUnit(unit.getKey(), temp);
			}
			
			return elemPriorityQueue.get(element.getQueueIndex()).add(element);
		}
		return false;
	}
	
	/** 
	 * Enqueues an element to the priority queue.
	 * Does not allow a null element.
	 * @param element Element to be added in the waiting list.
	 * @param isNewElement true if the element is a new element to be added in the process.
	 * @return True, if successful. Otherwise, false.
	 */
	public boolean enqueueElement(Element element, boolean isNewElement, boolean isFromBackUp, String status){
		if(element != null){
			if(isNewElement){
				if(!ElementRepository.contains(element.getElementId())){
					
					
					element.clearProductionOutputUnit();
					for(Entry<String,ProductionOutputUnit> unit : productionOutputUnits.entrySet()){
						ProductionOutputUnit temp = new ProductionOutputUnit(unit.getValue().getMeasurementUnit(), unit.getValue().getCost());
						element.addProductionOutputUnit(unit.getKey(), temp);
					}
					
					element.setStatus(status,isFromBackUp);
					if(!isFromBackUp){
						element.setStartWaitTime(System.currentTimeMillis());
						incTotalWaitingElemCount();
					}					
					for (Element elem : elemPriorityQueue.get(element.getQueueIndex())) {
						if(elem.equals(element)){
							return false;
						}
					}
//					System.out.println("ADDING ELEEMNT: " + element.getElementId());
					ElementRepository.addElement(element.getElementId(),element);
				} else {
					return false;
				}
			}
			return elemPriorityQueue.get(element.getQueueIndex()).add(element);
		}
		
		return false;
	}
	/** 
	 * Enqueues an element to the priority queue.
	 * Does not allow a null element.
	 * @param element Element to be added in the waiting list.
	 * @param isNewElement true if the element is a new element to be added in the process.
	 * @return True, if successful. Otherwise, false.
	 */
	public boolean enqueueElement(Element element, boolean isNewElement, boolean restartWaitingTime){
		if(element != null){
			if(isNewElement){				
				if(!ElementRepository.contains(element.getElementId())){
					if(restartWaitingTime){
						element.setStartWaitTime(System.currentTimeMillis());
					}
					
					element.setStatus("WAITING");
					incTotalWaitingElemCount();
					for (Element elem : elemPriorityQueue.get(element.getQueueIndex())) {
						if(elem.equals(element)){
							return false;
						}
					}
					ElementRepository.addElement(element.getElementId(),element);
				} else {
					return false;
				}
			}

			
			return elemPriorityQueue.get(element.getQueueIndex()).add(element);
		}
		
		return false;
	}
		
	/**
	 * Dequeues an element from the priority queue.
	 * If worker is assigned to multiple queues,
	 * it will return an element from the lowest queue first.
	 * <p>
	 * @param workerId The ID of the worker
	 * @return  The element removed from the waiting list.
	 */
	public Element dequeueElement(String workerId){
		TreeSet<Integer> queues = workerList.get(workerId);
		if(queues != null){
			for(int i : queues){
				Element elem = elemPriorityQueue.get(i).pollLast();
				if(elem != null){
					return elem;
				}
			}
//			
//			return elemPriorityQueue
//					.get(workerList.get(workerId).first()).pollLast();
		}
		return null;
	}
	
	/**
	 * Dequeues an element from the priority queue.
	 * If worker is assigned to multiple queues,
	 * it will return an element from the lowest queue first.
	 * <p>
	 * @param workerId The ID of the worker
	 * @param queueIndex The queue of this node where this worker is assigned
	 * @return  The element removed from the waiting list.
	 */
	public Element dequeueElement(String workerId, int queueIndex){
		if(workerList.get(workerId).contains(queueIndex)){
			return elemPriorityQueue
					.get(queueIndex)
					.pollLast();
		}
		return null;
	}
	
	
	public Element dequeueElement(String elementId, String workerId, int queueIndex){
		if(workerList.get(workerId).contains(queueIndex)){
			Element elem = null;
			for(Element e : elemPriorityQueue.get(queueIndex)){
				if(e.getElementId().equals(elementId)){
					elem = e;
					break;
				}
			}
			
			if(elem != null){
				if(elemPriorityQueue.get(queueIndex).remove(elem)){
					return elem;
				}
			}
		}
		
		return null;
	}
			
	/**
	 * Adds element to the list of returned elements.
	 * Does not allow null key and value.
	 * <p>
	 * @param element The element to be returned.
	 */
	public void addToReturnedElemList(String key,Element element){
		if(element != null && key != null){
			returnedElemList.putIfAbsent(key, element);
		}		
	}
	
	
	/**
	 * Removes element from the list of returned elements.
	 * Returns null if the elementId is null.
	 * <p>
	 * @param element The element to be removed from the list of returned elements.
	 */
	public Element removeFromReturnedElemList(String elementId){	
		if(elementId == null){
			return null;
		}
		return	returnedElemList.remove(elementId);		
	}
	
	public Element removeFromWaitingQueue(String elementId, int queueIndex){		
		if(!Strings.isNullOrEmpty(elementId) &&	(queueIndex >=0 && queueIndex <100)){			
			ConcurrentSkipListSet<Element> set = elemPriorityQueue.get(queueIndex);
			Element elem = new Element(elementId);
			for(Element e : set){
				if(e.equals(elem)){
					elem = e;
//					System.out.println("FOUND");
					break;
				}
			}
			
			for(Element e : set){
				if(ElementComparator.byPriorityThenByStartWaitTime.compare(e, elem) == 0){
					elem = e;
				}
			}
			
			if(set.remove(elem)){
				return elem;
			}
		}
		return null;
	}

// --------------------------------- AUXILIARY METHODS ------------------------------------//
// -------------------------- used by some OTHER METHODS above ---------------------------//
	/**
	 * Returns all elements whose IDs are on the specified list of IDs
	 * <p>
	 * @param elementsID List of ID of elements to be retrieved.
	 * @param listOfElements List of all elements where a set of elements is to be retrieved by ID.
	 * @return List<Element>  List of elements whose IDs are on the list.
	 */
	private List<Element> getElementsById(List<String> elementsID, List<Element> listOfElements){
		List<Element> requestedElements = new ArrayList<Element>();
		// search for all requested enqueued elements by element ID 
		for(String requestedElemId : elementsID){
			for(Element element : listOfElements){
				if(element.getElementId().equals(requestedElemId)){
					// avoid duplication
					if(!requestedElements.contains(element)){
						requestedElements.add(element);
					}					
				}
			}
		}			
		return requestedElements;
	}

	public boolean hasElement(String elementId){
		HashSet<Element> set = new HashSet<Element>();
		set.addAll(getElements());
		return set.contains(new Element(elementId));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Node [");
		if (nodeId != null) {
			builder.append("nodeId=");
			builder.append(nodeId);
			builder.append(", ");
		}
		if (nodeName != null) {
			builder.append("nodeName=");
			builder.append(nodeName);
			builder.append(", ");
		}
		if (cluster != null) {
			builder.append("cluster=");
			builder.append(cluster);
			builder.append(", ");
		}
		if (status != null) {
			builder.append("status=");
			builder.append(status);
			builder.append(", ");
		}
		if (productionOutputUnits != null) {
			builder.append("productionOutputUnits=");
			builder.append(productionOutputUnits);
			builder.append(", ");
		}
		if (elemType != null) {
			builder.append("elemType=");
			builder.append(elemType);
			builder.append(", ");
		}
		if (aveWaitDurLimit != null) {
			builder.append("aveWaitDurLimit=");
			builder.append(aveWaitDurLimit);
			builder.append(", ");
		}
		if (aveProcDurLimit != null) {
			builder.append("aveProcDurLimit=");
			builder.append(aveProcDurLimit);
			builder.append(", ");
		}
		if (totalWaitingElemCount != null) {
			builder.append("totalWaitingElemCount=");
			builder.append(totalWaitingElemCount);
			builder.append(", ");
		}
		if (totalProcessingElemCount != null) {
			builder.append("totalProcessingElemCount=");
			builder.append(totalProcessingElemCount);
			builder.append(", ");
		}
		if (totalCompletedElemCount != null) {
			builder.append("totalCompletedElemCount=");
			builder.append(totalCompletedElemCount);
			builder.append(", ");
		}
		if (totalExceptionCount != null) {
			builder.append("totalExceptionCount=");
			builder.append(totalExceptionCount);
			builder.append(", ");
		}
		if (elemPriorityQueue != null) {
			builder.append("elemPriorityQueue=");
			builder.append(elemPriorityQueue);
			builder.append(", ");
		}
		if (inProcElemList != null) {
			builder.append("inProcElemList=");
			builder.append(inProcElemList);
			builder.append(", ");
		}
		if (returnedElemList != null) {
			builder.append("returnedElemList=");
			builder.append(returnedElemList);
			builder.append(", ");
		}
		if (workerList != null) {
			builder.append("workerList=");
			builder.append(workerList);
			builder.append(", ");
		}
		if (aveWaitDur != null) {
			builder.append("aveWaitDur=");
			builder.append(aveWaitDur);
			builder.append(", ");
		}
		if (aveProcDur != null) {
			builder.append("aveProcDur=");
			builder.append(aveProcDur);
			builder.append(", ");
		}
		if (minWaitDur != null) {
			builder.append("minWaitDur=");
			builder.append(minWaitDur);
			builder.append(", ");
		}
		if (maxWaitDur != null) {
			builder.append("maxWaitDur=");
			builder.append(maxWaitDur);
			builder.append(", ");
		}
		if (minProcDur != null) {
			builder.append("minProcDur=");
			builder.append(minProcDur);
			builder.append(", ");
		}
		if (maxProcDur != null) {
			builder.append("maxProcDur=");
			builder.append(maxProcDur);
			builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}

	
	// newly added, for computation of node average waiting time 
	public int getTotalGetElement() {
		return this.totalGetElement.get();
	}

	public void setTotalGetElement(int totalGetElement) {
		this.totalGetElement.set(totalGetElement);;
	}
	
	/**
	 * Increases the count of elements get from the node.
	 */
	public void incTotalGetElement(){
		this.totalGetElement.incrementAndGet();
	}
	
	/**
	 * Resets the count of elements get from the node to zero.
	 */
	public void resetTotalGetElement(){
		this.totalGetElement.set(0);
	}

	public ConcurrentHashMap<TriggerType, List<ServiceObj>> getServicesToRun() {
		return servicesToRun;
	}

	public void setServicesToRun(ConcurrentHashMap<TriggerType, List<ServiceObj>> servicesToRun) {
		this.servicesToRun = servicesToRun;
	}
	
}
