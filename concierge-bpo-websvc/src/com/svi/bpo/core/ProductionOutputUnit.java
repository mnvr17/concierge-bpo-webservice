package com.svi.bpo.core;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.annotations.Expose;

public class ProductionOutputUnit {

	// variables
	@Expose private String measurementUnit = "";
	@Expose private AtomicDouble outputCount = new AtomicDouble();
	@Expose private AtomicDouble errorCount = new AtomicDouble();
	@Expose private double cost = 0;

	// constructors
	public ProductionOutputUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public ProductionOutputUnit(String measurementUnit, double cost) {
		this.measurementUnit = measurementUnit;
		this.cost = cost;
	}
	
	public ProductionOutputUnit(double outputCount, double errorCount) {
		this.outputCount.set(outputCount);;
		this.errorCount.set(errorCount);
	}


	public ProductionOutputUnit(String measurementUnit, double outputCount,
			double errorCount, double cost) {
		this.measurementUnit = measurementUnit;
		this.outputCount.set(outputCount);
		this.errorCount.set(errorCount);
		this.cost = cost;
	}

	// setters and getters
	/**
	 * Returns the output count of the measurement unit.
	 * <p>
	 * 
	 * @return double outputCount
	 */
	public double getOutputCount() {
		return outputCount.get();
	}

	/**
	 * Sets the output count of the measurement unit.
	 * <p>
	 * 
	 * @param outputCount
	 */
	public void setOutputCount(double outputCount) {
		this.outputCount.set(outputCount);
	}
	
	/**
	 * Increases the output count
	 * @param outputCount The output count to be added
	 */
	public void incOutputCount(double outputCount){
		this.outputCount.getAndAdd(outputCount);
	}
	
	/**
	 * Increases the output count
	 * @param outputCount The output count to be added
	 */
	public void decOutputCount(double outputCount){
		this.outputCount.getAndAdd((-1)*outputCount);
	}

	/**
	 * Returns the error count of the measurement unit.
	 * <p>
	 * 
	 * @return double errorCount
	 */
	public double getErrorCount() {
		return errorCount.get();
	}

	/**
	 * Sets the error count of the measurement unit.
	 * <p>
	 * 
	 * @param errorCount
	 */
	public void setErrorCount(double errorCount) {
		System.out.println(errorCount);
		System.out.println(this.errorCount);
		this.errorCount.set(errorCount);
	}
	
	/**
	 * Increases the error count
	 * @param errorCount The error count to be added
	 */
	public void incErrorCount(double errorCount){
		this.errorCount.getAndAdd(errorCount);
	}
	
	/**
	 * Increases the error count
	 * @param errorCount The error count to be added
	 */
	public void decErrorCount(double errorCount){
		this.errorCount.getAndAdd((-1)*errorCount);
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	/**
	 * Returns the measurement unit.
	 * <p>
	 * 
	 * @return String measurementUnit
	 */
	public String getMeasurementUnit() {
		return measurementUnit;
	}

	/**
	 * Returns the cost of each element in the node.
	 * <p>
	 * 
	 * @return double cost
	 */
	public double getCost() {
		return cost;
	}
	

	public void setCost(double cost) {
		this.cost = cost;
	}

	@Override
	public String toString() {
		return "ProductionOutputUnit [measurementUnit=" + measurementUnit + ", outputCount=" + outputCount
				+ ", errorCount=" + errorCount + ", cost=" + cost + "]";
	}
}
