package com.svi.object;

/**
 * The HTTP request method types used for BPO RESTful requests
 */
public enum TriggerType {
	ON_INSERT("ON_INSERT"),
	ON_STATUS_CHANGE("ON_STATUS_CHANGE");
	private String val = "";
	
	TriggerType(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
}
