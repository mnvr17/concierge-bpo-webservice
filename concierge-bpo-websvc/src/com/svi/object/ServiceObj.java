package com.svi.object;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ServiceObj {

	private String servicePath;
	private ServiceType serviceType;
	private ReqType requestType;
	private String parameter;
	
	public ServiceObj(){
		
	}
	
	public ServiceObj(String servicePath, ServiceType serviceType, ReqType requestType, String parameter) {
		this.servicePath = servicePath;
		this.serviceType = serviceType;
		this.requestType = requestType;
		this.parameter = parameter;
	}
	
	public String getServicePath() {
		return servicePath;
	}
	public void setServicePath(String servicePath) {
		this.servicePath = servicePath;
	}
	public ServiceType getServiceType() {
		return serviceType;
	}
	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}
	public ReqType getRequestType() {
		return requestType;
	}
	public void setRequestType(ReqType requestType) {
		this.requestType = requestType;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	@Override
	public String toString() {
		ObjectMapper oMapper = new ObjectMapper();
		return "[servicePath=" + servicePath + ", serviceType=" + serviceType + ", requestType="
				+ requestType + ", parameter=" + parameter + "]";
	}
	
}
