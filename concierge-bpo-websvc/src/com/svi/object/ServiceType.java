package com.svi.object;

import org.ini4j.Profile.Section;

import com.svi.bpo.backup.BackupEnum;

/**
 * The HTTP request method types used for BPO RESTful requests
 */
public enum ServiceType {
	WEBSVC("WEBSVC"),JAR("JAR");

	private String val = "";
	
	ServiceType(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
	
}
