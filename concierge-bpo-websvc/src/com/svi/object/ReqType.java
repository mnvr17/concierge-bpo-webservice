package com.svi.object;

/**
 * The HTTP request method types used for BPO RESTful requests
 */
public enum ReqType {
	GET("GET"),
	POST("POST"),
	PUT("PUT"),
	DELETE("DELETE"),
	PATCH("PATCH"),
	NONE("NONE");
	private String val = "";
	
	ReqType(String val){
		this.val = val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
	public String getVal(){
		return val;
	}
}
